#ifndef TEST_CLASS_H
#define TEST_CLASS_H

#include "dbcgeneratorutils.h"

// VESC_Command_AbsHBrakeCurrent_V8, can id: 2147486728, length: 8
class VESCCommandAbsHBrakeCurrentV8Message
{
public:
	VESCCommandAbsHBrakeCurrentV8Message()
		: _commandHBrakeCurrentV8(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486728; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsHBrakeCurrent_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_HBrakeCurrent_V8
	double commandHBrakeCurrentV8() const { return static_cast<double>(_commandHBrakeCurrentV8)*0.001; }
	// Set Command_HBrakeCurrent_V8
	void commandHBrakeCurrentV8(double val) { _commandHBrakeCurrentV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandHBrakeCurrentV8 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_AbsHBrakeCurrent_V7, can id: 2147486727, length: 8
class VESCCommandAbsHBrakeCurrentV7Message
{
public:
	VESCCommandAbsHBrakeCurrentV7Message()
		: _commandHBrakeCurrentV7(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486727; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsHBrakeCurrent_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_HBrakeCurrent_V7
	double commandHBrakeCurrentV7() const { return static_cast<double>(_commandHBrakeCurrentV7)*0.001; }
	// Set Command_HBrakeCurrent_V7
	void commandHBrakeCurrentV7(double val) { _commandHBrakeCurrentV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandHBrakeCurrentV7 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_AbsHBrakeCurrent_V6, can id: 2147486726, length: 8
class VESCCommandAbsHBrakeCurrentV6Message
{
public:
	VESCCommandAbsHBrakeCurrentV6Message()
		: _commandHBrakeCurrentV6(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486726; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsHBrakeCurrent_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_HBrakeCurrent_V6
	double commandHBrakeCurrentV6() const { return static_cast<double>(_commandHBrakeCurrentV6)*0.001; }
	// Set Command_HBrakeCurrent_V6
	void commandHBrakeCurrentV6(double val) { _commandHBrakeCurrentV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandHBrakeCurrentV6 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_AbsHBrakeCurrent_V5, can id: 2147486725, length: 8
class VESCCommandAbsHBrakeCurrentV5Message
{
public:
	VESCCommandAbsHBrakeCurrentV5Message()
		: _commandHBrakeCurrentV5(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486725; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsHBrakeCurrent_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_HBrakeCurrent_V5
	double commandHBrakeCurrentV5() const { return static_cast<double>(_commandHBrakeCurrentV5)*0.001; }
	// Set Command_HBrakeCurrent_V5
	void commandHBrakeCurrentV5(double val) { _commandHBrakeCurrentV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandHBrakeCurrentV5 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_AbsHBrakeCurrent_V4, can id: 2147486724, length: 8
class VESCCommandAbsHBrakeCurrentV4Message
{
public:
	VESCCommandAbsHBrakeCurrentV4Message()
		: _commandHBrakeCurrentV4(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486724; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsHBrakeCurrent_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_HBrakeCurrent_V4
	double commandHBrakeCurrentV4() const { return static_cast<double>(_commandHBrakeCurrentV4)*0.001; }
	// Set Command_HBrakeCurrent_V4
	void commandHBrakeCurrentV4(double val) { _commandHBrakeCurrentV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandHBrakeCurrentV4 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_AbsHBrakeCurrent_V3, can id: 2147486723, length: 8
class VESCCommandAbsHBrakeCurrentV3Message
{
public:
	VESCCommandAbsHBrakeCurrentV3Message()
		: _commandHBrakeCurrentV3(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486723; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsHBrakeCurrent_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_HBrakeCurrent_V3
	double commandHBrakeCurrentV3() const { return static_cast<double>(_commandHBrakeCurrentV3)*0.001; }
	// Set Command_HBrakeCurrent_V3
	void commandHBrakeCurrentV3(double val) { _commandHBrakeCurrentV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandHBrakeCurrentV3 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_AbsHBrakeCurrent_V2, can id: 2147486722, length: 8
class VESCCommandAbsHBrakeCurrentV2Message
{
public:
	VESCCommandAbsHBrakeCurrentV2Message()
		: _commandHBrakeCurrentV2(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486722; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsHBrakeCurrent_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_HBrakeCurrent_V2
	double commandHBrakeCurrentV2() const { return static_cast<double>(_commandHBrakeCurrentV2)*0.001; }
	// Set Command_HBrakeCurrent_V2
	void commandHBrakeCurrentV2(double val) { _commandHBrakeCurrentV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandHBrakeCurrentV2 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_AbsHBrakeCurrent_V1, can id: 2147486721, length: 8
class VESCCommandAbsHBrakeCurrentV1Message
{
public:
	VESCCommandAbsHBrakeCurrentV1Message()
		: _commandHBrakeCurrentV1(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486721; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsHBrakeCurrent_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_HBrakeCurrent_V1
	double commandHBrakeCurrentV1() const { return static_cast<double>(_commandHBrakeCurrentV1)*0.001; }
	// Set Command_HBrakeCurrent_V1
	void commandHBrakeCurrentV1(double val) { _commandHBrakeCurrentV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandHBrakeCurrentV1 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_RelHBrakeCurrent_V8, can id: 2147486984, length: 8
class VESCCommandRelHBrakeCurrentV8Message
{
public:
	VESCCommandRelHBrakeCurrentV8Message()
		: _commandRelativeHBrakeCurrentV8(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486984; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_RelHBrakeCurrent_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeHBrakeCurrent_V8
	double commandRelativeHBrakeCurrentV8() const { return static_cast<double>(_commandRelativeHBrakeCurrentV8)*0.001; }
	// Set Command_RelativeHBrakeCurrent_V8
	void commandRelativeHBrakeCurrentV8(double val) { _commandRelativeHBrakeCurrentV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeHBrakeCurrentV8 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_RelHBrakeCurrent_V7, can id: 2147486983, length: 8
class VESCCommandRelHBrakeCurrentV7Message
{
public:
	VESCCommandRelHBrakeCurrentV7Message()
		: _commandRelativeHBrakeCurrentV7(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486983; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_RelHBrakeCurrent_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeHBrakeCurrent_V7
	double commandRelativeHBrakeCurrentV7() const { return static_cast<double>(_commandRelativeHBrakeCurrentV7)*0.001; }
	// Set Command_RelativeHBrakeCurrent_V7
	void commandRelativeHBrakeCurrentV7(double val) { _commandRelativeHBrakeCurrentV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeHBrakeCurrentV7 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_RelHBrakeCurrent_V6, can id: 2147486982, length: 8
class VESCCommandRelHBrakeCurrentV6Message
{
public:
	VESCCommandRelHBrakeCurrentV6Message()
		: _commandRelativeHBrakeCurrentV6(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486982; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_RelHBrakeCurrent_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeHBrakeCurrent_V6
	double commandRelativeHBrakeCurrentV6() const { return static_cast<double>(_commandRelativeHBrakeCurrentV6)*0.001; }
	// Set Command_RelativeHBrakeCurrent_V6
	void commandRelativeHBrakeCurrentV6(double val) { _commandRelativeHBrakeCurrentV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeHBrakeCurrentV6 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_RelHBrakeCurrent_V5, can id: 2147486981, length: 8
class VESCCommandRelHBrakeCurrentV5Message
{
public:
	VESCCommandRelHBrakeCurrentV5Message()
		: _commandRelativeHBrakeCurrentV5(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486981; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_RelHBrakeCurrent_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeHBrakeCurrent_V5
	double commandRelativeHBrakeCurrentV5() const { return static_cast<double>(_commandRelativeHBrakeCurrentV5)*0.001; }
	// Set Command_RelativeHBrakeCurrent_V5
	void commandRelativeHBrakeCurrentV5(double val) { _commandRelativeHBrakeCurrentV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeHBrakeCurrentV5 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_RelHBrakeCurrent_V4, can id: 2147486980, length: 8
class VESCCommandRelHBrakeCurrentV4Message
{
public:
	VESCCommandRelHBrakeCurrentV4Message()
		: _commandRelativeHBrakeCurrentV4(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486980; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_RelHBrakeCurrent_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeHBrakeCurrent_V4
	double commandRelativeHBrakeCurrentV4() const { return static_cast<double>(_commandRelativeHBrakeCurrentV4)*0.001; }
	// Set Command_RelativeHBrakeCurrent_V4
	void commandRelativeHBrakeCurrentV4(double val) { _commandRelativeHBrakeCurrentV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeHBrakeCurrentV4 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_RelHBrakeCurrent_V3, can id: 2147486979, length: 8
class VESCCommandRelHBrakeCurrentV3Message
{
public:
	VESCCommandRelHBrakeCurrentV3Message()
		: _commandRelativeHBrakeCurrentV3(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486979; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_RelHBrakeCurrent_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeHBrakeCurrent_V3
	double commandRelativeHBrakeCurrentV3() const { return static_cast<double>(_commandRelativeHBrakeCurrentV3)*0.001; }
	// Set Command_RelativeHBrakeCurrent_V3
	void commandRelativeHBrakeCurrentV3(double val) { _commandRelativeHBrakeCurrentV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeHBrakeCurrentV3 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_RelHBrakeCurrent_V2, can id: 2147486978, length: 8
class VESCCommandRelHBrakeCurrentV2Message
{
public:
	VESCCommandRelHBrakeCurrentV2Message()
		: _commandRelativeHBrakeCurrentV2(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486978; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_RelHBrakeCurrent_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeHBrakeCurrent_V2
	double commandRelativeHBrakeCurrentV2() const { return static_cast<double>(_commandRelativeHBrakeCurrentV2)*0.001; }
	// Set Command_RelativeHBrakeCurrent_V2
	void commandRelativeHBrakeCurrentV2(double val) { _commandRelativeHBrakeCurrentV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeHBrakeCurrentV2 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Command_RelHBrakeCurrent_V1, can id: 2147486977, length: 8
class VESCCommandRelHBrakeCurrentV1Message
{
public:
	VESCCommandRelHBrakeCurrentV1Message()
		: _commandRelativeHBrakeCurrentV1(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 2147486977; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Command_RelHBrakeCurrent_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeHBrakeCurrent_V1
	double commandRelativeHBrakeCurrentV1() const { return static_cast<double>(_commandRelativeHBrakeCurrentV1)*0.001; }
	// Set Command_RelativeHBrakeCurrent_V1
	void commandRelativeHBrakeCurrentV1(double val) { _commandRelativeHBrakeCurrentV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeHBrakeCurrentV1 : 32;
	[[maybe_unused]] const char _reserved[4];
};

// VESC_Set_CurrentLimitPerm_V1, can id: 2147489281, length: 8
class VESCSetCurrentLimitPermV1Message
{
public:
	VESCSetCurrentLimitPermV1Message()
		: _settingCurrentLimitMinV1(0)
		, _settingCurrentLimitMaxV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147489281; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitPerm_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V1
	double settingCurrentLimitMinV1() const { return static_cast<double>(_settingCurrentLimitMinV1)*0.001; }
	// Set Setting_CurrentLimitMin_V1
	void settingCurrentLimitMinV1(double val) { _settingCurrentLimitMinV1 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V1
	double settingCurrentLimitMaxV1() const { return static_cast<double>(_settingCurrentLimitMaxV1)*0.001; }
	// Set Setting_CurrentLimitMax_V1
	void settingCurrentLimitMaxV1(double val) { _settingCurrentLimitMaxV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV1 : 32;
	int32_t _settingCurrentLimitMaxV1 : 32;
};

// VESC_Set_CurrentLimitTemp_V1, can id: 2147489025, length: 8
class VESCSetCurrentLimitTempV1Message
{
public:
	VESCSetCurrentLimitTempV1Message()
		: _settingCurrentLimitMinV1(0)
		, _settingCurrentLimitMaxV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147489025; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitTemp_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V1
	double settingCurrentLimitMinV1() const { return static_cast<double>(_settingCurrentLimitMinV1)*0.001; }
	// Set Setting_CurrentLimitMin_V1
	void settingCurrentLimitMinV1(double val) { _settingCurrentLimitMinV1 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V1
	double settingCurrentLimitMaxV1() const { return static_cast<double>(_settingCurrentLimitMaxV1)*0.001; }
	// Set Setting_CurrentLimitMax_V1
	void settingCurrentLimitMaxV1(double val) { _settingCurrentLimitMaxV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV1 : 32;
	int32_t _settingCurrentLimitMaxV1 : 32;
};

// VESC_Command_RelBrakeCurrent_V1, can id: 2147486465, length: 4
class VESCCommandRelBrakeCurrentV1Message
{
public:
	VESCCommandRelBrakeCurrentV1Message()
		: _commandRelativeBrakeCurrentV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147486465; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelBrakeCurrent_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeBrakeCurrent_V1
	double commandRelativeBrakeCurrentV1() const { return static_cast<double>(_commandRelativeBrakeCurrentV1)*0.001; }
	// Set Command_RelativeBrakeCurrent_V1
	void commandRelativeBrakeCurrentV1(double val) { _commandRelativeBrakeCurrentV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeBrakeCurrentV1 : 32;
};

// VESC_Command_RelCurrent_V1, can id: 2147486209, length: 4
class VESCCommandRelCurrentV1Message
{
public:
	VESCCommandRelCurrentV1Message()
		: _commandRelativeCurrentV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147486209; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelCurrent_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeCurrent_V1
	double commandRelativeCurrentV1() const { return static_cast<double>(_commandRelativeCurrentV1)*0.001; }
	// Set Command_RelativeCurrent_V1
	void commandRelativeCurrentV1(double val) { _commandRelativeCurrentV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeCurrentV1 : 32;
};

// VESC_Command_POS_V1, can id: 2147484673, length: 4
class VESCCommandPOSV1Message
{
public:
	VESCCommandPOSV1Message()
		: _commandPOSV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147484673; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_POS_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_POS_V1
	int32_t commandPOSV1() const { return _commandPOSV1; }
	// Set Command_POS_V1
	void commandPOSV1(int32_t val) { _commandPOSV1 = val; }

private:
	int32_t _commandPOSV1 : 32;
};

// VESC_Command_AbsBrakeCurrent_V1, can id: 2147484161, length: 4
class VESCCommandAbsBrakeCurrentV1Message
{
public:
	VESCCommandAbsBrakeCurrentV1Message()
		: _commandBrakeCurrentV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147484161; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsBrakeCurrent_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_BrakeCurrent_V1
	double commandBrakeCurrentV1() const { return static_cast<double>(_commandBrakeCurrentV1)*0.001; }
	// Set Command_BrakeCurrent_V1
	void commandBrakeCurrentV1(double val) { _commandBrakeCurrentV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandBrakeCurrentV1 : 32;
};

// VESC_Command_AbsCurrent_V1, can id: 2147483905, length: 4
class VESCCommandAbsCurrentV1Message
{
public:
	VESCCommandAbsCurrentV1Message()
		: _commandCurrentV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147483905; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsCurrent_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_Current_V1
	double commandCurrentV1() const { return static_cast<double>(_commandCurrentV1)*0.001; }
	// Set Command_Current_V1
	void commandCurrentV1(double val) { _commandCurrentV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandCurrentV1 : 32;
};

// VESC_Command_RPM_V1, can id: 2147484417, length: 4
class VESCCommandRPMV1Message
{
public:
	VESCCommandRPMV1Message()
		: _commandRPMV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147484417; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RPM_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RPM_V1
	int32_t commandRPMV1() const { return _commandRPMV1; }
	// Set Command_RPM_V1
	void commandRPMV1(int32_t val) { _commandRPMV1 = val; }

private:
	int32_t _commandRPMV1 : 32;
};

// VESC_Status5_V1, can id: 2147490561, length: 8
class VESCStatus5V1Message
{
public:
	VESCStatus5V1Message()
		: _statusInputVoltageV1(0)
		, _statusReservedV1(0)
		, _statusTachometerV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147490561; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status5_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_InputVoltage_V1
	double statusInputVoltageV1() const { return static_cast<double>(_statusInputVoltageV1)*0.1; }
	// Set Status_InputVoltage_V1
	void statusInputVoltageV1(double val) { _statusInputVoltageV1 = static_cast<int16_t>(val/0.1); }

	// Get Status_Reserved_V1
	int16_t statusReservedV1() const { return _statusReservedV1; }
	// Set Status_Reserved_V1
	void statusReservedV1(int16_t val) { _statusReservedV1 = val; }

	// Get Status_Tachometer_V1
	int32_t statusTachometerV1() const { return _statusTachometerV1; }
	// Set Status_Tachometer_V1
	void statusTachometerV1(int32_t val) { _statusTachometerV1 = val; }

private:
	int16_t _statusInputVoltageV1 : 16;
	int16_t _statusReservedV1 : 16;
	int32_t _statusTachometerV1 : 32;
};

// VESC_Status4_V1, can id: 2147487745, length: 8
class VESCStatus4V1Message
{
public:
	VESCStatus4V1Message()
		: _stausMosfetTempV1(0)
		, _stausMotorTempV1(0)
		, _statusTotalInputCurrentV1(0)
		, _statusPIDPosV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147487745; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status4_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Staus_MosfetTemp_V1
	double stausMosfetTempV1() const { return static_cast<double>(_stausMosfetTempV1)*0.1; }
	// Set Staus_MosfetTemp_V1
	void stausMosfetTempV1(double val) { _stausMosfetTempV1 = static_cast<int16_t>(val/0.1); }

	// Get Staus_MotorTemp_V1
	double stausMotorTempV1() const { return static_cast<double>(_stausMotorTempV1)*0.1; }
	// Set Staus_MotorTemp_V1
	void stausMotorTempV1(double val) { _stausMotorTempV1 = static_cast<int16_t>(val/0.1); }

	// Get Status_TotalInputCurrent_V1
	double statusTotalInputCurrentV1() const { return static_cast<double>(_statusTotalInputCurrentV1)*0.1; }
	// Set Status_TotalInputCurrent_V1
	void statusTotalInputCurrentV1(double val) { _statusTotalInputCurrentV1 = static_cast<int16_t>(val/0.1); }

	// Get Status_PIDPos_V1
	double statusPIDPosV1() const { return static_cast<double>(_statusPIDPosV1)*0.02; }
	// Set Status_PIDPos_V1
	void statusPIDPosV1(double val) { _statusPIDPosV1 = static_cast<int16_t>(val/0.02); }

private:
	int16_t _stausMosfetTempV1 : 16;
	int16_t _stausMotorTempV1 : 16;
	int16_t _statusTotalInputCurrentV1 : 16;
	int16_t _statusPIDPosV1 : 16;
};

// VESC_Status3_V1, can id: 2147487489, length: 8
class VESCStatus3V1Message
{
public:
	VESCStatus3V1Message()
		: _statusWattHoursV1(0)
		, _statusWattHoursChargedV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147487489; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status3_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_WattHours_V1
	double statusWattHoursV1() const { return static_cast<double>(_statusWattHoursV1)*1e-05; }
	// Set Status_WattHours_V1
	void statusWattHoursV1(double val) { _statusWattHoursV1 = static_cast<int32_t>(val/1e-05); }

	// Get Status_WattHoursCharged_V1
	double statusWattHoursChargedV1() const { return static_cast<double>(_statusWattHoursChargedV1)*1e-05; }
	// Set Status_WattHoursCharged_V1
	void statusWattHoursChargedV1(double val) { _statusWattHoursChargedV1 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusWattHoursV1 : 32;
	int32_t _statusWattHoursChargedV1 : 32;
};

// VESC_Status2_V1, can id: 2147487233, length: 8
class VESCStatus2V1Message
{
public:
	VESCStatus2V1Message()
		: _statusAmpHoursV1(0)
		, _statusAmpHoursChargedV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147487233; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status2_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_AmpHours_V1
	double statusAmpHoursV1() const { return static_cast<double>(_statusAmpHoursV1)*1e-05; }
	// Set Status_AmpHours_V1
	void statusAmpHoursV1(double val) { _statusAmpHoursV1 = static_cast<int32_t>(val/1e-05); }

	// Get Status_AmpHoursCharged_V1
	double statusAmpHoursChargedV1() const { return static_cast<double>(_statusAmpHoursChargedV1)*1e-05; }
	// Set Status_AmpHoursCharged_V1
	void statusAmpHoursChargedV1(double val) { _statusAmpHoursChargedV1 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusAmpHoursV1 : 32;
	int32_t _statusAmpHoursChargedV1 : 32;
};

// VESC_Status1_V1, can id: 2147485953, length: 8
class VESCStatus1V1Message
{
public:
	VESCStatus1V1Message()
		: _statusRPMV1(0)
		, _statusTotalCurrentV1(0)
		, _statusDutyCycleV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147485953; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status1_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_RPM_V1
	int32_t statusRPMV1() const { return _statusRPMV1; }
	// Set Status_RPM_V1
	void statusRPMV1(int32_t val) { _statusRPMV1 = val; }

	// Get Status_TotalCurrent_V1
	double statusTotalCurrentV1() const { return static_cast<double>(_statusTotalCurrentV1)*0.1; }
	// Set Status_TotalCurrent_V1
	void statusTotalCurrentV1(double val) { _statusTotalCurrentV1 = static_cast<int16_t>(val/0.1); }

	// Get Status_DutyCycle_V1
	double statusDutyCycleV1() const { return static_cast<double>(_statusDutyCycleV1)*0.1; }
	// Set Status_DutyCycle_V1
	void statusDutyCycleV1(double val) { _statusDutyCycleV1 = static_cast<int16_t>(val/0.1); }

private:
	int32_t _statusRPMV1 : 32;
	int16_t _statusTotalCurrentV1 : 16;
	int16_t _statusDutyCycleV1 : 16;
};

// VESC_Command_DutyCycle_V1, can id: 2147483649, length: 4
class VESCCommandDutyCycleV1Message
{
public:
	VESCCommandDutyCycleV1Message()
		: _commandDutyCycleV1(0)
	{}

	static constexpr uint32_t CanId() { return 2147483649; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_DutyCycle_V1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_DutyCycle_V1
	double commandDutyCycleV1() const { return static_cast<double>(_commandDutyCycleV1)*0.001; }
	// Set Command_DutyCycle_V1
	void commandDutyCycleV1(double val) { _commandDutyCycleV1 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandDutyCycleV1 : 32;
};

// VESC_Set_CurrentLimitPerm_V2, can id: 2147489282, length: 8
class VESCSetCurrentLimitPermV2Message
{
public:
	VESCSetCurrentLimitPermV2Message()
		: _settingCurrentLimitMinV2(0)
		, _settingCurrentLimitMaxV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147489282; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitPerm_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V2
	double settingCurrentLimitMinV2() const { return static_cast<double>(_settingCurrentLimitMinV2)*0.001; }
	// Set Setting_CurrentLimitMin_V2
	void settingCurrentLimitMinV2(double val) { _settingCurrentLimitMinV2 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V2
	double settingCurrentLimitMaxV2() const { return static_cast<double>(_settingCurrentLimitMaxV2)*0.001; }
	// Set Setting_CurrentLimitMax_V2
	void settingCurrentLimitMaxV2(double val) { _settingCurrentLimitMaxV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV2 : 32;
	int32_t _settingCurrentLimitMaxV2 : 32;
};

// VESC_Set_CurrentLimitTemp_V2, can id: 2147489026, length: 8
class VESCSetCurrentLimitTempV2Message
{
public:
	VESCSetCurrentLimitTempV2Message()
		: _settingCurrentLimitMinV2(0)
		, _settingCurrentLimitMaxV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147489026; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitTemp_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V2
	double settingCurrentLimitMinV2() const { return static_cast<double>(_settingCurrentLimitMinV2)*0.001; }
	// Set Setting_CurrentLimitMin_V2
	void settingCurrentLimitMinV2(double val) { _settingCurrentLimitMinV2 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V2
	double settingCurrentLimitMaxV2() const { return static_cast<double>(_settingCurrentLimitMaxV2)*0.001; }
	// Set Setting_CurrentLimitMax_V2
	void settingCurrentLimitMaxV2(double val) { _settingCurrentLimitMaxV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV2 : 32;
	int32_t _settingCurrentLimitMaxV2 : 32;
};

// VESC_Command_RelBrakeCurrent_V2, can id: 2147486466, length: 4
class VESCCommandRelBrakeCurrentV2Message
{
public:
	VESCCommandRelBrakeCurrentV2Message()
		: _commandRelativeBrakeCurrentV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147486466; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelBrakeCurrent_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeBrakeCurrent_V2
	double commandRelativeBrakeCurrentV2() const { return static_cast<double>(_commandRelativeBrakeCurrentV2)*0.001; }
	// Set Command_RelativeBrakeCurrent_V2
	void commandRelativeBrakeCurrentV2(double val) { _commandRelativeBrakeCurrentV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeBrakeCurrentV2 : 32;
};

// VESC_Command_RelCurrent_V2, can id: 2147486210, length: 4
class VESCCommandRelCurrentV2Message
{
public:
	VESCCommandRelCurrentV2Message()
		: _commandRelativeCurrentV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147486210; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelCurrent_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeCurrent_V2
	double commandRelativeCurrentV2() const { return static_cast<double>(_commandRelativeCurrentV2)*0.001; }
	// Set Command_RelativeCurrent_V2
	void commandRelativeCurrentV2(double val) { _commandRelativeCurrentV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeCurrentV2 : 32;
};

// VESC_Command_POS_V2, can id: 2147484674, length: 4
class VESCCommandPOSV2Message
{
public:
	VESCCommandPOSV2Message()
		: _commandPOSV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147484674; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_POS_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_POS_V2
	int32_t commandPOSV2() const { return _commandPOSV2; }
	// Set Command_POS_V2
	void commandPOSV2(int32_t val) { _commandPOSV2 = val; }

private:
	int32_t _commandPOSV2 : 32;
};

// VESC_Command_AbsBrakeCurrent_V2, can id: 2147484162, length: 4
class VESCCommandAbsBrakeCurrentV2Message
{
public:
	VESCCommandAbsBrakeCurrentV2Message()
		: _commandBrakeCurrentV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147484162; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsBrakeCurrent_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_BrakeCurrent_V2
	double commandBrakeCurrentV2() const { return static_cast<double>(_commandBrakeCurrentV2)*0.001; }
	// Set Command_BrakeCurrent_V2
	void commandBrakeCurrentV2(double val) { _commandBrakeCurrentV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandBrakeCurrentV2 : 32;
};

// VESC_Command_AbsCurrent_V2, can id: 2147483906, length: 4
class VESCCommandAbsCurrentV2Message
{
public:
	VESCCommandAbsCurrentV2Message()
		: _commandCurrentV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147483906; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsCurrent_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_Current_V2
	double commandCurrentV2() const { return static_cast<double>(_commandCurrentV2)*0.001; }
	// Set Command_Current_V2
	void commandCurrentV2(double val) { _commandCurrentV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandCurrentV2 : 32;
};

// VESC_Command_RPM_V2, can id: 2147484418, length: 4
class VESCCommandRPMV2Message
{
public:
	VESCCommandRPMV2Message()
		: _commandRPMV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147484418; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RPM_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RPM_V2
	int32_t commandRPMV2() const { return _commandRPMV2; }
	// Set Command_RPM_V2
	void commandRPMV2(int32_t val) { _commandRPMV2 = val; }

private:
	int32_t _commandRPMV2 : 32;
};

// VESC_Status5_V2, can id: 2147490562, length: 8
class VESCStatus5V2Message
{
public:
	VESCStatus5V2Message()
		: _statusInputVoltageV2(0)
		, _statusReservedV2(0)
		, _statusTachometerV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147490562; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status5_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_InputVoltage_V2
	double statusInputVoltageV2() const { return static_cast<double>(_statusInputVoltageV2)*0.1; }
	// Set Status_InputVoltage_V2
	void statusInputVoltageV2(double val) { _statusInputVoltageV2 = static_cast<int16_t>(val/0.1); }

	// Get Status_Reserved_V2
	int16_t statusReservedV2() const { return _statusReservedV2; }
	// Set Status_Reserved_V2
	void statusReservedV2(int16_t val) { _statusReservedV2 = val; }

	// Get Status_Tachometer_V2
	int32_t statusTachometerV2() const { return _statusTachometerV2; }
	// Set Status_Tachometer_V2
	void statusTachometerV2(int32_t val) { _statusTachometerV2 = val; }

private:
	int16_t _statusInputVoltageV2 : 16;
	int16_t _statusReservedV2 : 16;
	int32_t _statusTachometerV2 : 32;
};

// VESC_Status4_V2, can id: 2147487746, length: 8
class VESCStatus4V2Message
{
public:
	VESCStatus4V2Message()
		: _stausMosfetTempV2(0)
		, _stausMotorTempV2(0)
		, _statusTotalInputCurrentV2(0)
		, _statusPIDPosV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147487746; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status4_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Staus_MosfetTemp_V2
	double stausMosfetTempV2() const { return static_cast<double>(_stausMosfetTempV2)*0.1; }
	// Set Staus_MosfetTemp_V2
	void stausMosfetTempV2(double val) { _stausMosfetTempV2 = static_cast<int16_t>(val/0.1); }

	// Get Staus_MotorTemp_V2
	double stausMotorTempV2() const { return static_cast<double>(_stausMotorTempV2)*0.1; }
	// Set Staus_MotorTemp_V2
	void stausMotorTempV2(double val) { _stausMotorTempV2 = static_cast<int16_t>(val/0.1); }

	// Get Status_TotalInputCurrent_V2
	double statusTotalInputCurrentV2() const { return static_cast<double>(_statusTotalInputCurrentV2)*0.1; }
	// Set Status_TotalInputCurrent_V2
	void statusTotalInputCurrentV2(double val) { _statusTotalInputCurrentV2 = static_cast<int16_t>(val/0.1); }

	// Get Status_PIDPos_V2
	double statusPIDPosV2() const { return static_cast<double>(_statusPIDPosV2)*0.02; }
	// Set Status_PIDPos_V2
	void statusPIDPosV2(double val) { _statusPIDPosV2 = static_cast<int16_t>(val/0.02); }

private:
	int16_t _stausMosfetTempV2 : 16;
	int16_t _stausMotorTempV2 : 16;
	int16_t _statusTotalInputCurrentV2 : 16;
	int16_t _statusPIDPosV2 : 16;
};

// VESC_Status3_V2, can id: 2147487490, length: 8
class VESCStatus3V2Message
{
public:
	VESCStatus3V2Message()
		: _statusWattHoursV2(0)
		, _statusWattHoursChargedV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147487490; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status3_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_WattHours_V2
	double statusWattHoursV2() const { return static_cast<double>(_statusWattHoursV2)*1e-05; }
	// Set Status_WattHours_V2
	void statusWattHoursV2(double val) { _statusWattHoursV2 = static_cast<int32_t>(val/1e-05); }

	// Get Status_WattHoursCharged_V2
	double statusWattHoursChargedV2() const { return static_cast<double>(_statusWattHoursChargedV2)*1e-05; }
	// Set Status_WattHoursCharged_V2
	void statusWattHoursChargedV2(double val) { _statusWattHoursChargedV2 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusWattHoursV2 : 32;
	int32_t _statusWattHoursChargedV2 : 32;
};

// VESC_Status2_V2, can id: 2147487234, length: 8
class VESCStatus2V2Message
{
public:
	VESCStatus2V2Message()
		: _statusAmpHoursV2(0)
		, _statusAmpHoursChargedV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147487234; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status2_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_AmpHours_V2
	double statusAmpHoursV2() const { return static_cast<double>(_statusAmpHoursV2)*1e-05; }
	// Set Status_AmpHours_V2
	void statusAmpHoursV2(double val) { _statusAmpHoursV2 = static_cast<int32_t>(val/1e-05); }

	// Get Status_AmpHoursCharged_V2
	double statusAmpHoursChargedV2() const { return static_cast<double>(_statusAmpHoursChargedV2)*1e-05; }
	// Set Status_AmpHoursCharged_V2
	void statusAmpHoursChargedV2(double val) { _statusAmpHoursChargedV2 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusAmpHoursV2 : 32;
	int32_t _statusAmpHoursChargedV2 : 32;
};

// VESC_Status1_V2, can id: 2147485954, length: 8
class VESCStatus1V2Message
{
public:
	VESCStatus1V2Message()
		: _statusRPMV2(0)
		, _statusTotalCurrentV2(0)
		, _statusDutyCycleV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147485954; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status1_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_RPM_V2
	int32_t statusRPMV2() const { return _statusRPMV2; }
	// Set Status_RPM_V2
	void statusRPMV2(int32_t val) { _statusRPMV2 = val; }

	// Get Status_TotalCurrent_V2
	double statusTotalCurrentV2() const { return static_cast<double>(_statusTotalCurrentV2)*0.1; }
	// Set Status_TotalCurrent_V2
	void statusTotalCurrentV2(double val) { _statusTotalCurrentV2 = static_cast<int16_t>(val/0.1); }

	// Get Status_DutyCycle_V2
	double statusDutyCycleV2() const { return static_cast<double>(_statusDutyCycleV2)*0.1; }
	// Set Status_DutyCycle_V2
	void statusDutyCycleV2(double val) { _statusDutyCycleV2 = static_cast<int16_t>(val/0.1); }

private:
	int32_t _statusRPMV2 : 32;
	int16_t _statusTotalCurrentV2 : 16;
	int16_t _statusDutyCycleV2 : 16;
};

// VESC_Command_DutyCycle_V2, can id: 2147483650, length: 4
class VESCCommandDutyCycleV2Message
{
public:
	VESCCommandDutyCycleV2Message()
		: _commandDutyCycleV2(0)
	{}

	static constexpr uint32_t CanId() { return 2147483650; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_DutyCycle_V2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_DutyCycle_V2
	double commandDutyCycleV2() const { return static_cast<double>(_commandDutyCycleV2)*0.001; }
	// Set Command_DutyCycle_V2
	void commandDutyCycleV2(double val) { _commandDutyCycleV2 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandDutyCycleV2 : 32;
};

// VESC_Set_CurrentLimitPerm_V3, can id: 2147489283, length: 8
class VESCSetCurrentLimitPermV3Message
{
public:
	VESCSetCurrentLimitPermV3Message()
		: _settingCurrentLimitMinV3(0)
		, _settingCurrentLimitMaxV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147489283; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitPerm_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V3
	double settingCurrentLimitMinV3() const { return static_cast<double>(_settingCurrentLimitMinV3)*0.001; }
	// Set Setting_CurrentLimitMin_V3
	void settingCurrentLimitMinV3(double val) { _settingCurrentLimitMinV3 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V3
	double settingCurrentLimitMaxV3() const { return static_cast<double>(_settingCurrentLimitMaxV3)*0.001; }
	// Set Setting_CurrentLimitMax_V3
	void settingCurrentLimitMaxV3(double val) { _settingCurrentLimitMaxV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV3 : 32;
	int32_t _settingCurrentLimitMaxV3 : 32;
};

// VESC_Set_CurrentLimitTemp_V3, can id: 2147489027, length: 8
class VESCSetCurrentLimitTempV3Message
{
public:
	VESCSetCurrentLimitTempV3Message()
		: _settingCurrentLimitMinV3(0)
		, _settingCurrentLimitMaxV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147489027; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitTemp_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V3
	double settingCurrentLimitMinV3() const { return static_cast<double>(_settingCurrentLimitMinV3)*0.001; }
	// Set Setting_CurrentLimitMin_V3
	void settingCurrentLimitMinV3(double val) { _settingCurrentLimitMinV3 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V3
	double settingCurrentLimitMaxV3() const { return static_cast<double>(_settingCurrentLimitMaxV3)*0.001; }
	// Set Setting_CurrentLimitMax_V3
	void settingCurrentLimitMaxV3(double val) { _settingCurrentLimitMaxV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV3 : 32;
	int32_t _settingCurrentLimitMaxV3 : 32;
};

// VESC_Command_RelBrakeCurrent_V3, can id: 2147486467, length: 4
class VESCCommandRelBrakeCurrentV3Message
{
public:
	VESCCommandRelBrakeCurrentV3Message()
		: _commandRelativeBrakeCurrentV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147486467; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelBrakeCurrent_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeBrakeCurrent_V3
	double commandRelativeBrakeCurrentV3() const { return static_cast<double>(_commandRelativeBrakeCurrentV3)*0.001; }
	// Set Command_RelativeBrakeCurrent_V3
	void commandRelativeBrakeCurrentV3(double val) { _commandRelativeBrakeCurrentV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeBrakeCurrentV3 : 32;
};

// VESC_Command_RelCurrent_V3, can id: 2147486211, length: 4
class VESCCommandRelCurrentV3Message
{
public:
	VESCCommandRelCurrentV3Message()
		: _commandRelativeCurrentV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147486211; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelCurrent_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeCurrent_V3
	double commandRelativeCurrentV3() const { return static_cast<double>(_commandRelativeCurrentV3)*0.001; }
	// Set Command_RelativeCurrent_V3
	void commandRelativeCurrentV3(double val) { _commandRelativeCurrentV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeCurrentV3 : 32;
};

// VESC_Command_POS_V3, can id: 2147484675, length: 4
class VESCCommandPOSV3Message
{
public:
	VESCCommandPOSV3Message()
		: _commandPOSV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147484675; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_POS_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_POS_V3
	int32_t commandPOSV3() const { return _commandPOSV3; }
	// Set Command_POS_V3
	void commandPOSV3(int32_t val) { _commandPOSV3 = val; }

private:
	int32_t _commandPOSV3 : 32;
};

// VESC_Command_AbsBrakeCurrent_V3, can id: 2147484163, length: 4
class VESCCommandAbsBrakeCurrentV3Message
{
public:
	VESCCommandAbsBrakeCurrentV3Message()
		: _commandBrakeCurrentV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147484163; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsBrakeCurrent_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_BrakeCurrent_V3
	double commandBrakeCurrentV3() const { return static_cast<double>(_commandBrakeCurrentV3)*0.001; }
	// Set Command_BrakeCurrent_V3
	void commandBrakeCurrentV3(double val) { _commandBrakeCurrentV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandBrakeCurrentV3 : 32;
};

// VESC_Command_AbsCurrent_V3, can id: 2147483907, length: 4
class VESCCommandAbsCurrentV3Message
{
public:
	VESCCommandAbsCurrentV3Message()
		: _commandCurrentV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147483907; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsCurrent_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_Current_V3
	double commandCurrentV3() const { return static_cast<double>(_commandCurrentV3)*0.001; }
	// Set Command_Current_V3
	void commandCurrentV3(double val) { _commandCurrentV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandCurrentV3 : 32;
};

// VESC_Command_RPM_V3, can id: 2147484419, length: 4
class VESCCommandRPMV3Message
{
public:
	VESCCommandRPMV3Message()
		: _commandRPMV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147484419; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RPM_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RPM_V3
	int32_t commandRPMV3() const { return _commandRPMV3; }
	// Set Command_RPM_V3
	void commandRPMV3(int32_t val) { _commandRPMV3 = val; }

private:
	int32_t _commandRPMV3 : 32;
};

// VESC_Status5_V3, can id: 2147490563, length: 8
class VESCStatus5V3Message
{
public:
	VESCStatus5V3Message()
		: _statusInputVoltageV3(0)
		, _statusReservedV3(0)
		, _statusTachometerV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147490563; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status5_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_InputVoltage_V3
	double statusInputVoltageV3() const { return static_cast<double>(_statusInputVoltageV3)*0.1; }
	// Set Status_InputVoltage_V3
	void statusInputVoltageV3(double val) { _statusInputVoltageV3 = static_cast<int16_t>(val/0.1); }

	// Get Status_Reserved_V3
	int16_t statusReservedV3() const { return _statusReservedV3; }
	// Set Status_Reserved_V3
	void statusReservedV3(int16_t val) { _statusReservedV3 = val; }

	// Get Status_Tachometer_V3
	int32_t statusTachometerV3() const { return _statusTachometerV3; }
	// Set Status_Tachometer_V3
	void statusTachometerV3(int32_t val) { _statusTachometerV3 = val; }

private:
	int16_t _statusInputVoltageV3 : 16;
	int16_t _statusReservedV3 : 16;
	int32_t _statusTachometerV3 : 32;
};

// VESC_Status4_V3, can id: 2147487747, length: 8
class VESCStatus4V3Message
{
public:
	VESCStatus4V3Message()
		: _stausMosfetTempV3(0)
		, _stausMotorTempV3(0)
		, _statusTotalInputCurrentV3(0)
		, _statusPIDPosV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147487747; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status4_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Staus_MosfetTemp_V3
	double stausMosfetTempV3() const { return static_cast<double>(_stausMosfetTempV3)*0.1; }
	// Set Staus_MosfetTemp_V3
	void stausMosfetTempV3(double val) { _stausMosfetTempV3 = static_cast<int16_t>(val/0.1); }

	// Get Staus_MotorTemp_V3
	double stausMotorTempV3() const { return static_cast<double>(_stausMotorTempV3)*0.1; }
	// Set Staus_MotorTemp_V3
	void stausMotorTempV3(double val) { _stausMotorTempV3 = static_cast<int16_t>(val/0.1); }

	// Get Status_TotalInputCurrent_V3
	double statusTotalInputCurrentV3() const { return static_cast<double>(_statusTotalInputCurrentV3)*0.1; }
	// Set Status_TotalInputCurrent_V3
	void statusTotalInputCurrentV3(double val) { _statusTotalInputCurrentV3 = static_cast<int16_t>(val/0.1); }

	// Get Status_PIDPos_V3
	double statusPIDPosV3() const { return static_cast<double>(_statusPIDPosV3)*0.02; }
	// Set Status_PIDPos_V3
	void statusPIDPosV3(double val) { _statusPIDPosV3 = static_cast<int16_t>(val/0.02); }

private:
	int16_t _stausMosfetTempV3 : 16;
	int16_t _stausMotorTempV3 : 16;
	int16_t _statusTotalInputCurrentV3 : 16;
	int16_t _statusPIDPosV3 : 16;
};

// VESC_Status3_V3, can id: 2147487491, length: 8
class VESCStatus3V3Message
{
public:
	VESCStatus3V3Message()
		: _statusWattHoursV3(0)
		, _statusWattHoursChargedV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147487491; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status3_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_WattHours_V3
	double statusWattHoursV3() const { return static_cast<double>(_statusWattHoursV3)*1e-05; }
	// Set Status_WattHours_V3
	void statusWattHoursV3(double val) { _statusWattHoursV3 = static_cast<int32_t>(val/1e-05); }

	// Get Status_WattHoursCharged_V3
	double statusWattHoursChargedV3() const { return static_cast<double>(_statusWattHoursChargedV3)*1e-05; }
	// Set Status_WattHoursCharged_V3
	void statusWattHoursChargedV3(double val) { _statusWattHoursChargedV3 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusWattHoursV3 : 32;
	int32_t _statusWattHoursChargedV3 : 32;
};

// VESC_Status2_V3, can id: 2147487235, length: 8
class VESCStatus2V3Message
{
public:
	VESCStatus2V3Message()
		: _statusAmpHoursV3(0)
		, _statusAmpHoursChargedV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147487235; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status2_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_AmpHours_V3
	double statusAmpHoursV3() const { return static_cast<double>(_statusAmpHoursV3)*1e-05; }
	// Set Status_AmpHours_V3
	void statusAmpHoursV3(double val) { _statusAmpHoursV3 = static_cast<int32_t>(val/1e-05); }

	// Get Status_AmpHoursCharged_V3
	double statusAmpHoursChargedV3() const { return static_cast<double>(_statusAmpHoursChargedV3)*1e-05; }
	// Set Status_AmpHoursCharged_V3
	void statusAmpHoursChargedV3(double val) { _statusAmpHoursChargedV3 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusAmpHoursV3 : 32;
	int32_t _statusAmpHoursChargedV3 : 32;
};

// VESC_Status1_V3, can id: 2147485955, length: 8
class VESCStatus1V3Message
{
public:
	VESCStatus1V3Message()
		: _statusRPMV3(0)
		, _statusTotalCurrentV3(0)
		, _statusDutyCycleV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147485955; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status1_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_RPM_V3
	int32_t statusRPMV3() const { return _statusRPMV3; }
	// Set Status_RPM_V3
	void statusRPMV3(int32_t val) { _statusRPMV3 = val; }

	// Get Status_TotalCurrent_V3
	double statusTotalCurrentV3() const { return static_cast<double>(_statusTotalCurrentV3)*0.1; }
	// Set Status_TotalCurrent_V3
	void statusTotalCurrentV3(double val) { _statusTotalCurrentV3 = static_cast<int16_t>(val/0.1); }

	// Get Status_DutyCycle_V3
	double statusDutyCycleV3() const { return static_cast<double>(_statusDutyCycleV3)*0.1; }
	// Set Status_DutyCycle_V3
	void statusDutyCycleV3(double val) { _statusDutyCycleV3 = static_cast<int16_t>(val/0.1); }

private:
	int32_t _statusRPMV3 : 32;
	int16_t _statusTotalCurrentV3 : 16;
	int16_t _statusDutyCycleV3 : 16;
};

// VESC_Command_DutyCycle_V3, can id: 2147483651, length: 4
class VESCCommandDutyCycleV3Message
{
public:
	VESCCommandDutyCycleV3Message()
		: _commandDutyCycleV3(0)
	{}

	static constexpr uint32_t CanId() { return 2147483651; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_DutyCycle_V3"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_DutyCycle_V3
	double commandDutyCycleV3() const { return static_cast<double>(_commandDutyCycleV3)*0.001; }
	// Set Command_DutyCycle_V3
	void commandDutyCycleV3(double val) { _commandDutyCycleV3 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandDutyCycleV3 : 32;
};

// VESC_Set_CurrentLimitPerm_V4, can id: 2147489284, length: 8
class VESCSetCurrentLimitPermV4Message
{
public:
	VESCSetCurrentLimitPermV4Message()
		: _settingCurrentLimitMinV4(0)
		, _settingCurrentLimitMaxV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147489284; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitPerm_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V4
	double settingCurrentLimitMinV4() const { return static_cast<double>(_settingCurrentLimitMinV4)*0.001; }
	// Set Setting_CurrentLimitMin_V4
	void settingCurrentLimitMinV4(double val) { _settingCurrentLimitMinV4 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V4
	double settingCurrentLimitMaxV4() const { return static_cast<double>(_settingCurrentLimitMaxV4)*0.001; }
	// Set Setting_CurrentLimitMax_V4
	void settingCurrentLimitMaxV4(double val) { _settingCurrentLimitMaxV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV4 : 32;
	int32_t _settingCurrentLimitMaxV4 : 32;
};

// VESC_Set_CurrentLimitTemp_V4, can id: 2147489028, length: 8
class VESCSetCurrentLimitTempV4Message
{
public:
	VESCSetCurrentLimitTempV4Message()
		: _settingCurrentLimitMinV4(0)
		, _settingCurrentLimitMaxV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147489028; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitTemp_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V4
	double settingCurrentLimitMinV4() const { return static_cast<double>(_settingCurrentLimitMinV4)*0.001; }
	// Set Setting_CurrentLimitMin_V4
	void settingCurrentLimitMinV4(double val) { _settingCurrentLimitMinV4 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V4
	double settingCurrentLimitMaxV4() const { return static_cast<double>(_settingCurrentLimitMaxV4)*0.001; }
	// Set Setting_CurrentLimitMax_V4
	void settingCurrentLimitMaxV4(double val) { _settingCurrentLimitMaxV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV4 : 32;
	int32_t _settingCurrentLimitMaxV4 : 32;
};

// VESC_Command_RelBrakeCurrent_V4, can id: 2147486468, length: 4
class VESCCommandRelBrakeCurrentV4Message
{
public:
	VESCCommandRelBrakeCurrentV4Message()
		: _commandRelativeBrakeCurrentV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147486468; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelBrakeCurrent_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeBrakeCurrent_V4
	double commandRelativeBrakeCurrentV4() const { return static_cast<double>(_commandRelativeBrakeCurrentV4)*0.001; }
	// Set Command_RelativeBrakeCurrent_V4
	void commandRelativeBrakeCurrentV4(double val) { _commandRelativeBrakeCurrentV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeBrakeCurrentV4 : 32;
};

// VESC_Command_RelCurrent_V4, can id: 2147486212, length: 4
class VESCCommandRelCurrentV4Message
{
public:
	VESCCommandRelCurrentV4Message()
		: _commandRelativeCurrentV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147486212; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelCurrent_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeCurrent_V4
	double commandRelativeCurrentV4() const { return static_cast<double>(_commandRelativeCurrentV4)*0.001; }
	// Set Command_RelativeCurrent_V4
	void commandRelativeCurrentV4(double val) { _commandRelativeCurrentV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeCurrentV4 : 32;
};

// VESC_Command_POS_V4, can id: 2147484676, length: 4
class VESCCommandPOSV4Message
{
public:
	VESCCommandPOSV4Message()
		: _commandPOSV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147484676; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_POS_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_POS_V4
	int32_t commandPOSV4() const { return _commandPOSV4; }
	// Set Command_POS_V4
	void commandPOSV4(int32_t val) { _commandPOSV4 = val; }

private:
	int32_t _commandPOSV4 : 32;
};

// VESC_Command_AbsBrakeCurrent_V4, can id: 2147484164, length: 4
class VESCCommandAbsBrakeCurrentV4Message
{
public:
	VESCCommandAbsBrakeCurrentV4Message()
		: _commandBrakeCurrentV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147484164; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsBrakeCurrent_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_BrakeCurrent_V4
	double commandBrakeCurrentV4() const { return static_cast<double>(_commandBrakeCurrentV4)*0.001; }
	// Set Command_BrakeCurrent_V4
	void commandBrakeCurrentV4(double val) { _commandBrakeCurrentV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandBrakeCurrentV4 : 32;
};

// VESC_Command_AbsCurrent_V4, can id: 2147483908, length: 4
class VESCCommandAbsCurrentV4Message
{
public:
	VESCCommandAbsCurrentV4Message()
		: _commandCurrentV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147483908; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsCurrent_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_Current_V4
	double commandCurrentV4() const { return static_cast<double>(_commandCurrentV4)*0.001; }
	// Set Command_Current_V4
	void commandCurrentV4(double val) { _commandCurrentV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandCurrentV4 : 32;
};

// VESC_Command_RPM_V4, can id: 2147484420, length: 4
class VESCCommandRPMV4Message
{
public:
	VESCCommandRPMV4Message()
		: _commandRPMV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147484420; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RPM_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RPM_V4
	int32_t commandRPMV4() const { return _commandRPMV4; }
	// Set Command_RPM_V4
	void commandRPMV4(int32_t val) { _commandRPMV4 = val; }

private:
	int32_t _commandRPMV4 : 32;
};

// VESC_Status5_V4, can id: 2147490564, length: 8
class VESCStatus5V4Message
{
public:
	VESCStatus5V4Message()
		: _statusInputVoltageV4(0)
		, _statusReservedV4(0)
		, _statusTachometerV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147490564; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status5_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_InputVoltage_V4
	double statusInputVoltageV4() const { return static_cast<double>(_statusInputVoltageV4)*0.1; }
	// Set Status_InputVoltage_V4
	void statusInputVoltageV4(double val) { _statusInputVoltageV4 = static_cast<int16_t>(val/0.1); }

	// Get Status_Reserved_V4
	int16_t statusReservedV4() const { return _statusReservedV4; }
	// Set Status_Reserved_V4
	void statusReservedV4(int16_t val) { _statusReservedV4 = val; }

	// Get Status_Tachometer_V4
	int32_t statusTachometerV4() const { return _statusTachometerV4; }
	// Set Status_Tachometer_V4
	void statusTachometerV4(int32_t val) { _statusTachometerV4 = val; }

private:
	int16_t _statusInputVoltageV4 : 16;
	int16_t _statusReservedV4 : 16;
	int32_t _statusTachometerV4 : 32;
};

// VESC_Status4_V4, can id: 2147487748, length: 8
class VESCStatus4V4Message
{
public:
	VESCStatus4V4Message()
		: _stausMosfetTempV4(0)
		, _stausMotorTempV4(0)
		, _statusTotalInputCurrentV4(0)
		, _statusPIDPosV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147487748; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status4_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Staus_MosfetTemp_V4
	double stausMosfetTempV4() const { return static_cast<double>(_stausMosfetTempV4)*0.1; }
	// Set Staus_MosfetTemp_V4
	void stausMosfetTempV4(double val) { _stausMosfetTempV4 = static_cast<int16_t>(val/0.1); }

	// Get Staus_MotorTemp_V4
	double stausMotorTempV4() const { return static_cast<double>(_stausMotorTempV4)*0.1; }
	// Set Staus_MotorTemp_V4
	void stausMotorTempV4(double val) { _stausMotorTempV4 = static_cast<int16_t>(val/0.1); }

	// Get Status_TotalInputCurrent_V4
	double statusTotalInputCurrentV4() const { return static_cast<double>(_statusTotalInputCurrentV4)*0.1; }
	// Set Status_TotalInputCurrent_V4
	void statusTotalInputCurrentV4(double val) { _statusTotalInputCurrentV4 = static_cast<int16_t>(val/0.1); }

	// Get Status_PIDPos_V4
	double statusPIDPosV4() const { return static_cast<double>(_statusPIDPosV4)*0.02; }
	// Set Status_PIDPos_V4
	void statusPIDPosV4(double val) { _statusPIDPosV4 = static_cast<int16_t>(val/0.02); }

private:
	int16_t _stausMosfetTempV4 : 16;
	int16_t _stausMotorTempV4 : 16;
	int16_t _statusTotalInputCurrentV4 : 16;
	int16_t _statusPIDPosV4 : 16;
};

// VESC_Status3_V4, can id: 2147487492, length: 8
class VESCStatus3V4Message
{
public:
	VESCStatus3V4Message()
		: _statusWattHoursV4(0)
		, _statusWattHoursChargedV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147487492; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status3_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_WattHours_V4
	double statusWattHoursV4() const { return static_cast<double>(_statusWattHoursV4)*1e-05; }
	// Set Status_WattHours_V4
	void statusWattHoursV4(double val) { _statusWattHoursV4 = static_cast<int32_t>(val/1e-05); }

	// Get Status_WattHoursCharged_V4
	double statusWattHoursChargedV4() const { return static_cast<double>(_statusWattHoursChargedV4)*1e-05; }
	// Set Status_WattHoursCharged_V4
	void statusWattHoursChargedV4(double val) { _statusWattHoursChargedV4 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusWattHoursV4 : 32;
	int32_t _statusWattHoursChargedV4 : 32;
};

// VESC_Status2_V4, can id: 2147487236, length: 8
class VESCStatus2V4Message
{
public:
	VESCStatus2V4Message()
		: _statusAmpHoursV4(0)
		, _statusAmpHoursChargedV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147487236; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status2_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_AmpHours_V4
	double statusAmpHoursV4() const { return static_cast<double>(_statusAmpHoursV4)*1e-05; }
	// Set Status_AmpHours_V4
	void statusAmpHoursV4(double val) { _statusAmpHoursV4 = static_cast<int32_t>(val/1e-05); }

	// Get Status_AmpHoursCharged_V4
	double statusAmpHoursChargedV4() const { return static_cast<double>(_statusAmpHoursChargedV4)*1e-05; }
	// Set Status_AmpHoursCharged_V4
	void statusAmpHoursChargedV4(double val) { _statusAmpHoursChargedV4 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusAmpHoursV4 : 32;
	int32_t _statusAmpHoursChargedV4 : 32;
};

// VESC_Status1_V4, can id: 2147485956, length: 8
class VESCStatus1V4Message
{
public:
	VESCStatus1V4Message()
		: _statusRPMV4(0)
		, _statusTotalCurrentV4(0)
		, _statusDutyCycleV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147485956; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status1_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_RPM_V4
	int32_t statusRPMV4() const { return _statusRPMV4; }
	// Set Status_RPM_V4
	void statusRPMV4(int32_t val) { _statusRPMV4 = val; }

	// Get Status_TotalCurrent_V4
	double statusTotalCurrentV4() const { return static_cast<double>(_statusTotalCurrentV4)*0.1; }
	// Set Status_TotalCurrent_V4
	void statusTotalCurrentV4(double val) { _statusTotalCurrentV4 = static_cast<int16_t>(val/0.1); }

	// Get Status_DutyCycle_V4
	double statusDutyCycleV4() const { return static_cast<double>(_statusDutyCycleV4)*0.1; }
	// Set Status_DutyCycle_V4
	void statusDutyCycleV4(double val) { _statusDutyCycleV4 = static_cast<int16_t>(val/0.1); }

private:
	int32_t _statusRPMV4 : 32;
	int16_t _statusTotalCurrentV4 : 16;
	int16_t _statusDutyCycleV4 : 16;
};

// VESC_Command_DutyCycle_V4, can id: 2147483652, length: 4
class VESCCommandDutyCycleV4Message
{
public:
	VESCCommandDutyCycleV4Message()
		: _commandDutyCycleV4(0)
	{}

	static constexpr uint32_t CanId() { return 2147483652; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_DutyCycle_V4"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_DutyCycle_V4
	double commandDutyCycleV4() const { return static_cast<double>(_commandDutyCycleV4)*0.001; }
	// Set Command_DutyCycle_V4
	void commandDutyCycleV4(double val) { _commandDutyCycleV4 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandDutyCycleV4 : 32;
};

// VESC_Set_CurrentLimitPerm_V5, can id: 2147489285, length: 8
class VESCSetCurrentLimitPermV5Message
{
public:
	VESCSetCurrentLimitPermV5Message()
		: _settingCurrentLimitMinV5(0)
		, _settingCurrentLimitMaxV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147489285; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitPerm_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V5
	double settingCurrentLimitMinV5() const { return static_cast<double>(_settingCurrentLimitMinV5)*0.001; }
	// Set Setting_CurrentLimitMin_V5
	void settingCurrentLimitMinV5(double val) { _settingCurrentLimitMinV5 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V5
	double settingCurrentLimitMaxV5() const { return static_cast<double>(_settingCurrentLimitMaxV5)*0.001; }
	// Set Setting_CurrentLimitMax_V5
	void settingCurrentLimitMaxV5(double val) { _settingCurrentLimitMaxV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV5 : 32;
	int32_t _settingCurrentLimitMaxV5 : 32;
};

// VESC_Set_CurrentLimitTemp_V5, can id: 2147489029, length: 8
class VESCSetCurrentLimitTempV5Message
{
public:
	VESCSetCurrentLimitTempV5Message()
		: _settingCurrentLimitMinV5(0)
		, _settingCurrentLimitMaxV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147489029; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitTemp_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V5
	double settingCurrentLimitMinV5() const { return static_cast<double>(_settingCurrentLimitMinV5)*0.001; }
	// Set Setting_CurrentLimitMin_V5
	void settingCurrentLimitMinV5(double val) { _settingCurrentLimitMinV5 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V5
	double settingCurrentLimitMaxV5() const { return static_cast<double>(_settingCurrentLimitMaxV5)*0.001; }
	// Set Setting_CurrentLimitMax_V5
	void settingCurrentLimitMaxV5(double val) { _settingCurrentLimitMaxV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV5 : 32;
	int32_t _settingCurrentLimitMaxV5 : 32;
};

// VESC_Command_RelBrakeCurrent_V5, can id: 2147486469, length: 4
class VESCCommandRelBrakeCurrentV5Message
{
public:
	VESCCommandRelBrakeCurrentV5Message()
		: _commandRelativeBrakeCurrentV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147486469; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelBrakeCurrent_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeBrakeCurrent_V5
	double commandRelativeBrakeCurrentV5() const { return static_cast<double>(_commandRelativeBrakeCurrentV5)*0.001; }
	// Set Command_RelativeBrakeCurrent_V5
	void commandRelativeBrakeCurrentV5(double val) { _commandRelativeBrakeCurrentV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeBrakeCurrentV5 : 32;
};

// VESC_Command_RelCurrent_V5, can id: 2147486213, length: 4
class VESCCommandRelCurrentV5Message
{
public:
	VESCCommandRelCurrentV5Message()
		: _commandRelativeCurrentV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147486213; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelCurrent_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeCurrent_V5
	double commandRelativeCurrentV5() const { return static_cast<double>(_commandRelativeCurrentV5)*0.001; }
	// Set Command_RelativeCurrent_V5
	void commandRelativeCurrentV5(double val) { _commandRelativeCurrentV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeCurrentV5 : 32;
};

// VESC_Command_POS_V5, can id: 2147484677, length: 4
class VESCCommandPOSV5Message
{
public:
	VESCCommandPOSV5Message()
		: _commandPOSV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147484677; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_POS_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_POS_V5
	int32_t commandPOSV5() const { return _commandPOSV5; }
	// Set Command_POS_V5
	void commandPOSV5(int32_t val) { _commandPOSV5 = val; }

private:
	int32_t _commandPOSV5 : 32;
};

// VESC_Command_AbsBrakeCurrent_V5, can id: 2147484165, length: 4
class VESCCommandAbsBrakeCurrentV5Message
{
public:
	VESCCommandAbsBrakeCurrentV5Message()
		: _commandBrakeCurrentV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147484165; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsBrakeCurrent_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_BrakeCurrent_V5
	double commandBrakeCurrentV5() const { return static_cast<double>(_commandBrakeCurrentV5)*0.001; }
	// Set Command_BrakeCurrent_V5
	void commandBrakeCurrentV5(double val) { _commandBrakeCurrentV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandBrakeCurrentV5 : 32;
};

// VESC_Command_AbsCurrent_V5, can id: 2147483909, length: 4
class VESCCommandAbsCurrentV5Message
{
public:
	VESCCommandAbsCurrentV5Message()
		: _commandCurrentV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147483909; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsCurrent_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_Current_V5
	double commandCurrentV5() const { return static_cast<double>(_commandCurrentV5)*0.001; }
	// Set Command_Current_V5
	void commandCurrentV5(double val) { _commandCurrentV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandCurrentV5 : 32;
};

// VESC_Command_RPM_V5, can id: 2147484421, length: 4
class VESCCommandRPMV5Message
{
public:
	VESCCommandRPMV5Message()
		: _commandRPMV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147484421; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RPM_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RPM_V5
	int32_t commandRPMV5() const { return _commandRPMV5; }
	// Set Command_RPM_V5
	void commandRPMV5(int32_t val) { _commandRPMV5 = val; }

private:
	int32_t _commandRPMV5 : 32;
};

// VESC_Status5_V5, can id: 2147490565, length: 8
class VESCStatus5V5Message
{
public:
	VESCStatus5V5Message()
		: _statusInputVoltageV5(0)
		, _statusReservedV5(0)
		, _statusTachometerV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147490565; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status5_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_InputVoltage_V5
	double statusInputVoltageV5() const { return static_cast<double>(_statusInputVoltageV5)*0.1; }
	// Set Status_InputVoltage_V5
	void statusInputVoltageV5(double val) { _statusInputVoltageV5 = static_cast<int16_t>(val/0.1); }

	// Get Status_Reserved_V5
	int16_t statusReservedV5() const { return _statusReservedV5; }
	// Set Status_Reserved_V5
	void statusReservedV5(int16_t val) { _statusReservedV5 = val; }

	// Get Status_Tachometer_V5
	int32_t statusTachometerV5() const { return _statusTachometerV5; }
	// Set Status_Tachometer_V5
	void statusTachometerV5(int32_t val) { _statusTachometerV5 = val; }

private:
	int16_t _statusInputVoltageV5 : 16;
	int16_t _statusReservedV5 : 16;
	int32_t _statusTachometerV5 : 32;
};

// VESC_Status4_V5, can id: 2147487749, length: 8
class VESCStatus4V5Message
{
public:
	VESCStatus4V5Message()
		: _stausMosfetTempV5(0)
		, _stausMotorTempV5(0)
		, _statusTotalInputCurrentV5(0)
		, _statusPIDPosV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147487749; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status4_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Staus_MosfetTemp_V5
	double stausMosfetTempV5() const { return static_cast<double>(_stausMosfetTempV5)*0.1; }
	// Set Staus_MosfetTemp_V5
	void stausMosfetTempV5(double val) { _stausMosfetTempV5 = static_cast<int16_t>(val/0.1); }

	// Get Staus_MotorTemp_V5
	double stausMotorTempV5() const { return static_cast<double>(_stausMotorTempV5)*0.1; }
	// Set Staus_MotorTemp_V5
	void stausMotorTempV5(double val) { _stausMotorTempV5 = static_cast<int16_t>(val/0.1); }

	// Get Status_TotalInputCurrent_V5
	double statusTotalInputCurrentV5() const { return static_cast<double>(_statusTotalInputCurrentV5)*0.1; }
	// Set Status_TotalInputCurrent_V5
	void statusTotalInputCurrentV5(double val) { _statusTotalInputCurrentV5 = static_cast<int16_t>(val/0.1); }

	// Get Status_PIDPos_V5
	double statusPIDPosV5() const { return static_cast<double>(_statusPIDPosV5)*0.02; }
	// Set Status_PIDPos_V5
	void statusPIDPosV5(double val) { _statusPIDPosV5 = static_cast<int16_t>(val/0.02); }

private:
	int16_t _stausMosfetTempV5 : 16;
	int16_t _stausMotorTempV5 : 16;
	int16_t _statusTotalInputCurrentV5 : 16;
	int16_t _statusPIDPosV5 : 16;
};

// VESC_Status3_V5, can id: 2147487493, length: 8
class VESCStatus3V5Message
{
public:
	VESCStatus3V5Message()
		: _statusWattHoursV5(0)
		, _statusWattHoursChargedV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147487493; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status3_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_WattHours_V5
	double statusWattHoursV5() const { return static_cast<double>(_statusWattHoursV5)*1e-05; }
	// Set Status_WattHours_V5
	void statusWattHoursV5(double val) { _statusWattHoursV5 = static_cast<int32_t>(val/1e-05); }

	// Get Status_WattHoursCharged_V5
	double statusWattHoursChargedV5() const { return static_cast<double>(_statusWattHoursChargedV5)*1e-05; }
	// Set Status_WattHoursCharged_V5
	void statusWattHoursChargedV5(double val) { _statusWattHoursChargedV5 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusWattHoursV5 : 32;
	int32_t _statusWattHoursChargedV5 : 32;
};

// VESC_Status2_V5, can id: 2147487237, length: 8
class VESCStatus2V5Message
{
public:
	VESCStatus2V5Message()
		: _statusAmpHoursV5(0)
		, _statusAmpHoursChargedV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147487237; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status2_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_AmpHours_V5
	double statusAmpHoursV5() const { return static_cast<double>(_statusAmpHoursV5)*1e-05; }
	// Set Status_AmpHours_V5
	void statusAmpHoursV5(double val) { _statusAmpHoursV5 = static_cast<int32_t>(val/1e-05); }

	// Get Status_AmpHoursCharged_V5
	double statusAmpHoursChargedV5() const { return static_cast<double>(_statusAmpHoursChargedV5)*1e-05; }
	// Set Status_AmpHoursCharged_V5
	void statusAmpHoursChargedV5(double val) { _statusAmpHoursChargedV5 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusAmpHoursV5 : 32;
	int32_t _statusAmpHoursChargedV5 : 32;
};

// VESC_Status1_V5, can id: 2147485957, length: 8
class VESCStatus1V5Message
{
public:
	VESCStatus1V5Message()
		: _statusRPMV5(0)
		, _statusTotalCurrentV5(0)
		, _statusDutyCycleV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147485957; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status1_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_RPM_V5
	int32_t statusRPMV5() const { return _statusRPMV5; }
	// Set Status_RPM_V5
	void statusRPMV5(int32_t val) { _statusRPMV5 = val; }

	// Get Status_TotalCurrent_V5
	double statusTotalCurrentV5() const { return static_cast<double>(_statusTotalCurrentV5)*0.1; }
	// Set Status_TotalCurrent_V5
	void statusTotalCurrentV5(double val) { _statusTotalCurrentV5 = static_cast<int16_t>(val/0.1); }

	// Get Status_DutyCycle_V5
	double statusDutyCycleV5() const { return static_cast<double>(_statusDutyCycleV5)*0.1; }
	// Set Status_DutyCycle_V5
	void statusDutyCycleV5(double val) { _statusDutyCycleV5 = static_cast<int16_t>(val/0.1); }

private:
	int32_t _statusRPMV5 : 32;
	int16_t _statusTotalCurrentV5 : 16;
	int16_t _statusDutyCycleV5 : 16;
};

// VESC_Command_DutyCycle_V5, can id: 2147483653, length: 4
class VESCCommandDutyCycleV5Message
{
public:
	VESCCommandDutyCycleV5Message()
		: _commandDutyCycleV5(0)
	{}

	static constexpr uint32_t CanId() { return 2147483653; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_DutyCycle_V5"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_DutyCycle_V5
	double commandDutyCycleV5() const { return static_cast<double>(_commandDutyCycleV5)*0.001; }
	// Set Command_DutyCycle_V5
	void commandDutyCycleV5(double val) { _commandDutyCycleV5 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandDutyCycleV5 : 32;
};

// VESC_Set_CurrentLimitPerm_V6, can id: 2147489286, length: 8
class VESCSetCurrentLimitPermV6Message
{
public:
	VESCSetCurrentLimitPermV6Message()
		: _settingCurrentLimitMinV6(0)
		, _settingCurrentLimitMaxV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147489286; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitPerm_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V6
	double settingCurrentLimitMinV6() const { return static_cast<double>(_settingCurrentLimitMinV6)*0.001; }
	// Set Setting_CurrentLimitMin_V6
	void settingCurrentLimitMinV6(double val) { _settingCurrentLimitMinV6 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V6
	double settingCurrentLimitMaxV6() const { return static_cast<double>(_settingCurrentLimitMaxV6)*0.001; }
	// Set Setting_CurrentLimitMax_V6
	void settingCurrentLimitMaxV6(double val) { _settingCurrentLimitMaxV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV6 : 32;
	int32_t _settingCurrentLimitMaxV6 : 32;
};

// VESC_Set_CurrentLimitTemp_V6, can id: 2147489030, length: 8
class VESCSetCurrentLimitTempV6Message
{
public:
	VESCSetCurrentLimitTempV6Message()
		: _settingCurrentLimitMinV6(0)
		, _settingCurrentLimitMaxV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147489030; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitTemp_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V6
	double settingCurrentLimitMinV6() const { return static_cast<double>(_settingCurrentLimitMinV6)*0.001; }
	// Set Setting_CurrentLimitMin_V6
	void settingCurrentLimitMinV6(double val) { _settingCurrentLimitMinV6 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V6
	double settingCurrentLimitMaxV6() const { return static_cast<double>(_settingCurrentLimitMaxV6)*0.001; }
	// Set Setting_CurrentLimitMax_V6
	void settingCurrentLimitMaxV6(double val) { _settingCurrentLimitMaxV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV6 : 32;
	int32_t _settingCurrentLimitMaxV6 : 32;
};

// VESC_Command_RelBrakeCurrent_V6, can id: 2147486470, length: 4
class VESCCommandRelBrakeCurrentV6Message
{
public:
	VESCCommandRelBrakeCurrentV6Message()
		: _commandRelativeBrakeCurrentV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147486470; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelBrakeCurrent_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeBrakeCurrent_V6
	double commandRelativeBrakeCurrentV6() const { return static_cast<double>(_commandRelativeBrakeCurrentV6)*0.001; }
	// Set Command_RelativeBrakeCurrent_V6
	void commandRelativeBrakeCurrentV6(double val) { _commandRelativeBrakeCurrentV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeBrakeCurrentV6 : 32;
};

// VESC_Command_RelCurrent_V6, can id: 2147486214, length: 4
class VESCCommandRelCurrentV6Message
{
public:
	VESCCommandRelCurrentV6Message()
		: _commandRelativeCurrentV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147486214; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelCurrent_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeCurrent_V6
	double commandRelativeCurrentV6() const { return static_cast<double>(_commandRelativeCurrentV6)*0.001; }
	// Set Command_RelativeCurrent_V6
	void commandRelativeCurrentV6(double val) { _commandRelativeCurrentV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeCurrentV6 : 32;
};

// VESC_Command_POS_V6, can id: 2147484678, length: 4
class VESCCommandPOSV6Message
{
public:
	VESCCommandPOSV6Message()
		: _commandPOSV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147484678; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_POS_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_POS_V6
	int32_t commandPOSV6() const { return _commandPOSV6; }
	// Set Command_POS_V6
	void commandPOSV6(int32_t val) { _commandPOSV6 = val; }

private:
	int32_t _commandPOSV6 : 32;
};

// VESC_Command_AbsBrakeCurrent_V6, can id: 2147484166, length: 4
class VESCCommandAbsBrakeCurrentV6Message
{
public:
	VESCCommandAbsBrakeCurrentV6Message()
		: _commandBrakeCurrentV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147484166; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsBrakeCurrent_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_BrakeCurrent_V6
	double commandBrakeCurrentV6() const { return static_cast<double>(_commandBrakeCurrentV6)*0.001; }
	// Set Command_BrakeCurrent_V6
	void commandBrakeCurrentV6(double val) { _commandBrakeCurrentV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandBrakeCurrentV6 : 32;
};

// VESC_Command_AbsCurrent_V6, can id: 2147483910, length: 4
class VESCCommandAbsCurrentV6Message
{
public:
	VESCCommandAbsCurrentV6Message()
		: _commandCurrentV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147483910; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsCurrent_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_Current_V6
	double commandCurrentV6() const { return static_cast<double>(_commandCurrentV6)*0.001; }
	// Set Command_Current_V6
	void commandCurrentV6(double val) { _commandCurrentV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandCurrentV6 : 32;
};

// VESC_Command_RPM_V6, can id: 2147484422, length: 4
class VESCCommandRPMV6Message
{
public:
	VESCCommandRPMV6Message()
		: _commandRPMV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147484422; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RPM_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RPM_V6
	int32_t commandRPMV6() const { return _commandRPMV6; }
	// Set Command_RPM_V6
	void commandRPMV6(int32_t val) { _commandRPMV6 = val; }

private:
	int32_t _commandRPMV6 : 32;
};

// VESC_Status5_V6, can id: 2147490566, length: 8
class VESCStatus5V6Message
{
public:
	VESCStatus5V6Message()
		: _statusInputVoltageV6(0)
		, _statusReservedV6(0)
		, _statusTachometerV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147490566; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status5_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_InputVoltage_V6
	double statusInputVoltageV6() const { return static_cast<double>(_statusInputVoltageV6)*0.1; }
	// Set Status_InputVoltage_V6
	void statusInputVoltageV6(double val) { _statusInputVoltageV6 = static_cast<int16_t>(val/0.1); }

	// Get Status_Reserved_V6
	int16_t statusReservedV6() const { return _statusReservedV6; }
	// Set Status_Reserved_V6
	void statusReservedV6(int16_t val) { _statusReservedV6 = val; }

	// Get Status_Tachometer_V6
	int32_t statusTachometerV6() const { return _statusTachometerV6; }
	// Set Status_Tachometer_V6
	void statusTachometerV6(int32_t val) { _statusTachometerV6 = val; }

private:
	int16_t _statusInputVoltageV6 : 16;
	int16_t _statusReservedV6 : 16;
	int32_t _statusTachometerV6 : 32;
};

// VESC_Status4_V6, can id: 2147487750, length: 8
class VESCStatus4V6Message
{
public:
	VESCStatus4V6Message()
		: _stausMosfetTempV6(0)
		, _stausMotorTempV6(0)
		, _statusTotalInputCurrentV6(0)
		, _statusPIDPosV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147487750; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status4_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Staus_MosfetTemp_V6
	double stausMosfetTempV6() const { return static_cast<double>(_stausMosfetTempV6)*0.1; }
	// Set Staus_MosfetTemp_V6
	void stausMosfetTempV6(double val) { _stausMosfetTempV6 = static_cast<int16_t>(val/0.1); }

	// Get Staus_MotorTemp_V6
	double stausMotorTempV6() const { return static_cast<double>(_stausMotorTempV6)*0.1; }
	// Set Staus_MotorTemp_V6
	void stausMotorTempV6(double val) { _stausMotorTempV6 = static_cast<int16_t>(val/0.1); }

	// Get Status_TotalInputCurrent_V6
	double statusTotalInputCurrentV6() const { return static_cast<double>(_statusTotalInputCurrentV6)*0.1; }
	// Set Status_TotalInputCurrent_V6
	void statusTotalInputCurrentV6(double val) { _statusTotalInputCurrentV6 = static_cast<int16_t>(val/0.1); }

	// Get Status_PIDPos_V6
	double statusPIDPosV6() const { return static_cast<double>(_statusPIDPosV6)*0.02; }
	// Set Status_PIDPos_V6
	void statusPIDPosV6(double val) { _statusPIDPosV6 = static_cast<int16_t>(val/0.02); }

private:
	int16_t _stausMosfetTempV6 : 16;
	int16_t _stausMotorTempV6 : 16;
	int16_t _statusTotalInputCurrentV6 : 16;
	int16_t _statusPIDPosV6 : 16;
};

// VESC_Status3_V6, can id: 2147487494, length: 8
class VESCStatus3V6Message
{
public:
	VESCStatus3V6Message()
		: _statusWattHoursV6(0)
		, _statusWattHoursChargedV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147487494; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status3_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_WattHours_V6
	double statusWattHoursV6() const { return static_cast<double>(_statusWattHoursV6)*1e-05; }
	// Set Status_WattHours_V6
	void statusWattHoursV6(double val) { _statusWattHoursV6 = static_cast<int32_t>(val/1e-05); }

	// Get Status_WattHoursCharged_V6
	double statusWattHoursChargedV6() const { return static_cast<double>(_statusWattHoursChargedV6)*1e-05; }
	// Set Status_WattHoursCharged_V6
	void statusWattHoursChargedV6(double val) { _statusWattHoursChargedV6 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusWattHoursV6 : 32;
	int32_t _statusWattHoursChargedV6 : 32;
};

// VESC_Status2_V6, can id: 2147487238, length: 8
class VESCStatus2V6Message
{
public:
	VESCStatus2V6Message()
		: _statusAmpHoursV6(0)
		, _statusAmpHoursChargedV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147487238; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status2_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_AmpHours_V6
	double statusAmpHoursV6() const { return static_cast<double>(_statusAmpHoursV6)*1e-05; }
	// Set Status_AmpHours_V6
	void statusAmpHoursV6(double val) { _statusAmpHoursV6 = static_cast<int32_t>(val/1e-05); }

	// Get Status_AmpHoursCharged_V6
	double statusAmpHoursChargedV6() const { return static_cast<double>(_statusAmpHoursChargedV6)*1e-05; }
	// Set Status_AmpHoursCharged_V6
	void statusAmpHoursChargedV6(double val) { _statusAmpHoursChargedV6 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusAmpHoursV6 : 32;
	int32_t _statusAmpHoursChargedV6 : 32;
};

// VESC_Status1_V6, can id: 2147485958, length: 8
class VESCStatus1V6Message
{
public:
	VESCStatus1V6Message()
		: _statusRPMV6(0)
		, _statusTotalCurrentV6(0)
		, _statusDutyCycleV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147485958; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status1_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_RPM_V6
	int32_t statusRPMV6() const { return _statusRPMV6; }
	// Set Status_RPM_V6
	void statusRPMV6(int32_t val) { _statusRPMV6 = val; }

	// Get Status_TotalCurrent_V6
	double statusTotalCurrentV6() const { return static_cast<double>(_statusTotalCurrentV6)*0.1; }
	// Set Status_TotalCurrent_V6
	void statusTotalCurrentV6(double val) { _statusTotalCurrentV6 = static_cast<int16_t>(val/0.1); }

	// Get Status_DutyCycle_V6
	double statusDutyCycleV6() const { return static_cast<double>(_statusDutyCycleV6)*0.1; }
	// Set Status_DutyCycle_V6
	void statusDutyCycleV6(double val) { _statusDutyCycleV6 = static_cast<int16_t>(val/0.1); }

private:
	int32_t _statusRPMV6 : 32;
	int16_t _statusTotalCurrentV6 : 16;
	int16_t _statusDutyCycleV6 : 16;
};

// VESC_Command_DutyCycle_V6, can id: 2147483654, length: 4
class VESCCommandDutyCycleV6Message
{
public:
	VESCCommandDutyCycleV6Message()
		: _commandDutyCycleV6(0)
	{}

	static constexpr uint32_t CanId() { return 2147483654; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_DutyCycle_V6"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_DutyCycle_V6
	double commandDutyCycleV6() const { return static_cast<double>(_commandDutyCycleV6)*0.001; }
	// Set Command_DutyCycle_V6
	void commandDutyCycleV6(double val) { _commandDutyCycleV6 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandDutyCycleV6 : 32;
};

// VESC_Set_CurrentLimitPerm_V7, can id: 2147489287, length: 8
class VESCSetCurrentLimitPermV7Message
{
public:
	VESCSetCurrentLimitPermV7Message()
		: _settingCurrentLimitMinV7(0)
		, _settingCurrentLimitMaxV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147489287; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitPerm_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V7
	double settingCurrentLimitMinV7() const { return static_cast<double>(_settingCurrentLimitMinV7)*0.001; }
	// Set Setting_CurrentLimitMin_V7
	void settingCurrentLimitMinV7(double val) { _settingCurrentLimitMinV7 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V7
	double settingCurrentLimitMaxV7() const { return static_cast<double>(_settingCurrentLimitMaxV7)*0.001; }
	// Set Setting_CurrentLimitMax_V7
	void settingCurrentLimitMaxV7(double val) { _settingCurrentLimitMaxV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV7 : 32;
	int32_t _settingCurrentLimitMaxV7 : 32;
};

// VESC_Set_CurrentLimitTemp_V7, can id: 2147489031, length: 8
class VESCSetCurrentLimitTempV7Message
{
public:
	VESCSetCurrentLimitTempV7Message()
		: _settingCurrentLimitMinV7(0)
		, _settingCurrentLimitMaxV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147489031; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitTemp_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V7
	double settingCurrentLimitMinV7() const { return static_cast<double>(_settingCurrentLimitMinV7)*0.001; }
	// Set Setting_CurrentLimitMin_V7
	void settingCurrentLimitMinV7(double val) { _settingCurrentLimitMinV7 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V7
	double settingCurrentLimitMaxV7() const { return static_cast<double>(_settingCurrentLimitMaxV7)*0.001; }
	// Set Setting_CurrentLimitMax_V7
	void settingCurrentLimitMaxV7(double val) { _settingCurrentLimitMaxV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV7 : 32;
	int32_t _settingCurrentLimitMaxV7 : 32;
};

// VESC_Command_RelBrakeCurrent_V7, can id: 2147486471, length: 4
class VESCCommandRelBrakeCurrentV7Message
{
public:
	VESCCommandRelBrakeCurrentV7Message()
		: _commandRelativeBrakeCurrentV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147486471; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelBrakeCurrent_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeBrakeCurrent_V7
	double commandRelativeBrakeCurrentV7() const { return static_cast<double>(_commandRelativeBrakeCurrentV7)*0.001; }
	// Set Command_RelativeBrakeCurrent_V7
	void commandRelativeBrakeCurrentV7(double val) { _commandRelativeBrakeCurrentV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeBrakeCurrentV7 : 32;
};

// VESC_Command_RelCurrent_V7, can id: 2147486215, length: 4
class VESCCommandRelCurrentV7Message
{
public:
	VESCCommandRelCurrentV7Message()
		: _commandRelativeCurrentV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147486215; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelCurrent_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeCurrent_V7
	double commandRelativeCurrentV7() const { return static_cast<double>(_commandRelativeCurrentV7)*0.001; }
	// Set Command_RelativeCurrent_V7
	void commandRelativeCurrentV7(double val) { _commandRelativeCurrentV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeCurrentV7 : 32;
};

// VESC_Command_POS_V7, can id: 2147484679, length: 4
class VESCCommandPOSV7Message
{
public:
	VESCCommandPOSV7Message()
		: _commandPOSV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147484679; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_POS_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_POS_V7
	int32_t commandPOSV7() const { return _commandPOSV7; }
	// Set Command_POS_V7
	void commandPOSV7(int32_t val) { _commandPOSV7 = val; }

private:
	int32_t _commandPOSV7 : 32;
};

// VESC_Command_AbsBrakeCurrent_V7, can id: 2147484167, length: 4
class VESCCommandAbsBrakeCurrentV7Message
{
public:
	VESCCommandAbsBrakeCurrentV7Message()
		: _commandBrakeCurrentV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147484167; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsBrakeCurrent_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_BrakeCurrent_V7
	double commandBrakeCurrentV7() const { return static_cast<double>(_commandBrakeCurrentV7)*0.001; }
	// Set Command_BrakeCurrent_V7
	void commandBrakeCurrentV7(double val) { _commandBrakeCurrentV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandBrakeCurrentV7 : 32;
};

// VESC_Command_AbsCurrent_V7, can id: 2147483911, length: 4
class VESCCommandAbsCurrentV7Message
{
public:
	VESCCommandAbsCurrentV7Message()
		: _commandCurrentV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147483911; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsCurrent_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_Current_V7
	double commandCurrentV7() const { return static_cast<double>(_commandCurrentV7)*0.001; }
	// Set Command_Current_V7
	void commandCurrentV7(double val) { _commandCurrentV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandCurrentV7 : 32;
};

// VESC_Command_RPM_V7, can id: 2147484423, length: 4
class VESCCommandRPMV7Message
{
public:
	VESCCommandRPMV7Message()
		: _commandRPMV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147484423; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RPM_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RPM_V7
	int32_t commandRPMV7() const { return _commandRPMV7; }
	// Set Command_RPM_V7
	void commandRPMV7(int32_t val) { _commandRPMV7 = val; }

private:
	int32_t _commandRPMV7 : 32;
};

// VESC_Status5_V7, can id: 2147490567, length: 8
class VESCStatus5V7Message
{
public:
	VESCStatus5V7Message()
		: _statusInputVoltageV7(0)
		, _statusReservedV7(0)
		, _statusTachometerV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147490567; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status5_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_InputVoltage_V7
	double statusInputVoltageV7() const { return static_cast<double>(_statusInputVoltageV7)*0.1; }
	// Set Status_InputVoltage_V7
	void statusInputVoltageV7(double val) { _statusInputVoltageV7 = static_cast<int16_t>(val/0.1); }

	// Get Status_Reserved_V7
	int16_t statusReservedV7() const { return _statusReservedV7; }
	// Set Status_Reserved_V7
	void statusReservedV7(int16_t val) { _statusReservedV7 = val; }

	// Get Status_Tachometer_V7
	int32_t statusTachometerV7() const { return _statusTachometerV7; }
	// Set Status_Tachometer_V7
	void statusTachometerV7(int32_t val) { _statusTachometerV7 = val; }

private:
	int16_t _statusInputVoltageV7 : 16;
	int16_t _statusReservedV7 : 16;
	int32_t _statusTachometerV7 : 32;
};

// VESC_Status4_V7, can id: 2147487751, length: 8
class VESCStatus4V7Message
{
public:
	VESCStatus4V7Message()
		: _stausMosfetTempV7(0)
		, _stausMotorTempV7(0)
		, _statusTotalInputCurrentV7(0)
		, _statusPIDPosV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147487751; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status4_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Staus_MosfetTemp_V7
	double stausMosfetTempV7() const { return static_cast<double>(_stausMosfetTempV7)*0.1; }
	// Set Staus_MosfetTemp_V7
	void stausMosfetTempV7(double val) { _stausMosfetTempV7 = static_cast<int16_t>(val/0.1); }

	// Get Staus_MotorTemp_V7
	double stausMotorTempV7() const { return static_cast<double>(_stausMotorTempV7)*0.1; }
	// Set Staus_MotorTemp_V7
	void stausMotorTempV7(double val) { _stausMotorTempV7 = static_cast<int16_t>(val/0.1); }

	// Get Status_TotalInputCurrent_V7
	double statusTotalInputCurrentV7() const { return static_cast<double>(_statusTotalInputCurrentV7)*0.1; }
	// Set Status_TotalInputCurrent_V7
	void statusTotalInputCurrentV7(double val) { _statusTotalInputCurrentV7 = static_cast<int16_t>(val/0.1); }

	// Get Status_PIDPos_V7
	double statusPIDPosV7() const { return static_cast<double>(_statusPIDPosV7)*0.02; }
	// Set Status_PIDPos_V7
	void statusPIDPosV7(double val) { _statusPIDPosV7 = static_cast<int16_t>(val/0.02); }

private:
	int16_t _stausMosfetTempV7 : 16;
	int16_t _stausMotorTempV7 : 16;
	int16_t _statusTotalInputCurrentV7 : 16;
	int16_t _statusPIDPosV7 : 16;
};

// VESC_Status3_V7, can id: 2147487495, length: 8
class VESCStatus3V7Message
{
public:
	VESCStatus3V7Message()
		: _statusWattHoursV7(0)
		, _statusWattHoursChargedV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147487495; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status3_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_WattHours_V7
	double statusWattHoursV7() const { return static_cast<double>(_statusWattHoursV7)*1e-05; }
	// Set Status_WattHours_V7
	void statusWattHoursV7(double val) { _statusWattHoursV7 = static_cast<int32_t>(val/1e-05); }

	// Get Status_WattHoursCharged_V7
	double statusWattHoursChargedV7() const { return static_cast<double>(_statusWattHoursChargedV7)*1e-05; }
	// Set Status_WattHoursCharged_V7
	void statusWattHoursChargedV7(double val) { _statusWattHoursChargedV7 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusWattHoursV7 : 32;
	int32_t _statusWattHoursChargedV7 : 32;
};

// VESC_Status2_V7, can id: 2147487239, length: 8
class VESCStatus2V7Message
{
public:
	VESCStatus2V7Message()
		: _statusAmpHoursV7(0)
		, _statusAmpHoursChargedV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147487239; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status2_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_AmpHours_V7
	double statusAmpHoursV7() const { return static_cast<double>(_statusAmpHoursV7)*1e-05; }
	// Set Status_AmpHours_V7
	void statusAmpHoursV7(double val) { _statusAmpHoursV7 = static_cast<int32_t>(val/1e-05); }

	// Get Status_AmpHoursCharged_V7
	double statusAmpHoursChargedV7() const { return static_cast<double>(_statusAmpHoursChargedV7)*1e-05; }
	// Set Status_AmpHoursCharged_V7
	void statusAmpHoursChargedV7(double val) { _statusAmpHoursChargedV7 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusAmpHoursV7 : 32;
	int32_t _statusAmpHoursChargedV7 : 32;
};

// VESC_Status1_V7, can id: 2147485959, length: 8
class VESCStatus1V7Message
{
public:
	VESCStatus1V7Message()
		: _statusRPMV7(0)
		, _statusTotalCurrentV7(0)
		, _statusDutyCycleV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147485959; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status1_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_RPM_V7
	int32_t statusRPMV7() const { return _statusRPMV7; }
	// Set Status_RPM_V7
	void statusRPMV7(int32_t val) { _statusRPMV7 = val; }

	// Get Status_TotalCurrent_V7
	double statusTotalCurrentV7() const { return static_cast<double>(_statusTotalCurrentV7)*0.1; }
	// Set Status_TotalCurrent_V7
	void statusTotalCurrentV7(double val) { _statusTotalCurrentV7 = static_cast<int16_t>(val/0.1); }

	// Get Status_DutyCycle_V7
	double statusDutyCycleV7() const { return static_cast<double>(_statusDutyCycleV7)*0.1; }
	// Set Status_DutyCycle_V7
	void statusDutyCycleV7(double val) { _statusDutyCycleV7 = static_cast<int16_t>(val/0.1); }

private:
	int32_t _statusRPMV7 : 32;
	int16_t _statusTotalCurrentV7 : 16;
	int16_t _statusDutyCycleV7 : 16;
};

// VESC_Command_DutyCycle_V7, can id: 2147483655, length: 4
class VESCCommandDutyCycleV7Message
{
public:
	VESCCommandDutyCycleV7Message()
		: _commandDutyCycleV7(0)
	{}

	static constexpr uint32_t CanId() { return 2147483655; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_DutyCycle_V7"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_DutyCycle_V7
	double commandDutyCycleV7() const { return static_cast<double>(_commandDutyCycleV7)*0.001; }
	// Set Command_DutyCycle_V7
	void commandDutyCycleV7(double val) { _commandDutyCycleV7 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandDutyCycleV7 : 32;
};

// VESC_Set_CurrentLimitPerm_V8, can id: 2147489288, length: 8
class VESCSetCurrentLimitPermV8Message
{
public:
	VESCSetCurrentLimitPermV8Message()
		: _settingCurrentLimitMinV8(0)
		, _settingCurrentLimitMaxV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147489288; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitPerm_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V8
	double settingCurrentLimitMinV8() const { return static_cast<double>(_settingCurrentLimitMinV8)*0.001; }
	// Set Setting_CurrentLimitMin_V8
	void settingCurrentLimitMinV8(double val) { _settingCurrentLimitMinV8 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V8
	double settingCurrentLimitMaxV8() const { return static_cast<double>(_settingCurrentLimitMaxV8)*0.001; }
	// Set Setting_CurrentLimitMax_V8
	void settingCurrentLimitMaxV8(double val) { _settingCurrentLimitMaxV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV8 : 32;
	int32_t _settingCurrentLimitMaxV8 : 32;
};

// VESC_Set_CurrentLimitTemp_V8, can id: 2147489032, length: 8
class VESCSetCurrentLimitTempV8Message
{
public:
	VESCSetCurrentLimitTempV8Message()
		: _settingCurrentLimitMinV8(0)
		, _settingCurrentLimitMaxV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147489032; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Set_CurrentLimitTemp_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Setting_CurrentLimitMin_V8
	double settingCurrentLimitMinV8() const { return static_cast<double>(_settingCurrentLimitMinV8)*0.001; }
	// Set Setting_CurrentLimitMin_V8
	void settingCurrentLimitMinV8(double val) { _settingCurrentLimitMinV8 = static_cast<int32_t>(val/0.001); }

	// Get Setting_CurrentLimitMax_V8
	double settingCurrentLimitMaxV8() const { return static_cast<double>(_settingCurrentLimitMaxV8)*0.001; }
	// Set Setting_CurrentLimitMax_V8
	void settingCurrentLimitMaxV8(double val) { _settingCurrentLimitMaxV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _settingCurrentLimitMinV8 : 32;
	int32_t _settingCurrentLimitMaxV8 : 32;
};

// VESC_Command_RelBrakeCurrent_V8, can id: 2147486472, length: 4
class VESCCommandRelBrakeCurrentV8Message
{
public:
	VESCCommandRelBrakeCurrentV8Message()
		: _commandRelativeBrakeCurrentV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147486472; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelBrakeCurrent_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeBrakeCurrent_V8
	double commandRelativeBrakeCurrentV8() const { return static_cast<double>(_commandRelativeBrakeCurrentV8)*0.001; }
	// Set Command_RelativeBrakeCurrent_V8
	void commandRelativeBrakeCurrentV8(double val) { _commandRelativeBrakeCurrentV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeBrakeCurrentV8 : 32;
};

// VESC_Command_RelCurrent_V8, can id: 2147486216, length: 4
class VESCCommandRelCurrentV8Message
{
public:
	VESCCommandRelCurrentV8Message()
		: _commandRelativeCurrentV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147486216; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RelCurrent_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RelativeCurrent_V8
	double commandRelativeCurrentV8() const { return static_cast<double>(_commandRelativeCurrentV8)*0.001; }
	// Set Command_RelativeCurrent_V8
	void commandRelativeCurrentV8(double val) { _commandRelativeCurrentV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandRelativeCurrentV8 : 32;
};

// VESC_Command_POS_V8, can id: 2147484680, length: 4
class VESCCommandPOSV8Message
{
public:
	VESCCommandPOSV8Message()
		: _commandPOSV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147484680; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_POS_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_POS_V8
	int32_t commandPOSV8() const { return _commandPOSV8; }
	// Set Command_POS_V8
	void commandPOSV8(int32_t val) { _commandPOSV8 = val; }

private:
	int32_t _commandPOSV8 : 32;
};

// VESC_Command_AbsBrakeCurrent_V8, can id: 2147484168, length: 4
class VESCCommandAbsBrakeCurrentV8Message
{
public:
	VESCCommandAbsBrakeCurrentV8Message()
		: _commandBrakeCurrentV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147484168; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsBrakeCurrent_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_BrakeCurrent_V8
	double commandBrakeCurrentV8() const { return static_cast<double>(_commandBrakeCurrentV8)*0.001; }
	// Set Command_BrakeCurrent_V8
	void commandBrakeCurrentV8(double val) { _commandBrakeCurrentV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandBrakeCurrentV8 : 32;
};

// VESC_Command_AbsCurrent_V8, can id: 2147483912, length: 4
class VESCCommandAbsCurrentV8Message
{
public:
	VESCCommandAbsCurrentV8Message()
		: _commandCurrentV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147483912; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_AbsCurrent_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_Current_V8
	double commandCurrentV8() const { return static_cast<double>(_commandCurrentV8)*0.001; }
	// Set Command_Current_V8
	void commandCurrentV8(double val) { _commandCurrentV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandCurrentV8 : 32;
};

// VESC_Command_RPM_V8, can id: 2147484424, length: 4
class VESCCommandRPMV8Message
{
public:
	VESCCommandRPMV8Message()
		: _commandRPMV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147484424; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_RPM_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_RPM_V8
	int32_t commandRPMV8() const { return _commandRPMV8; }
	// Set Command_RPM_V8
	void commandRPMV8(int32_t val) { _commandRPMV8 = val; }

private:
	int32_t _commandRPMV8 : 32;
};

// VESC_Status5_V8, can id: 2147490568, length: 8
class VESCStatus5V8Message
{
public:
	VESCStatus5V8Message()
		: _statusInputVoltageV8(0)
		, _statusReservedV8(0)
		, _statusTachometerV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147490568; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status5_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_InputVoltage_V8
	double statusInputVoltageV8() const { return static_cast<double>(_statusInputVoltageV8)*0.1; }
	// Set Status_InputVoltage_V8
	void statusInputVoltageV8(double val) { _statusInputVoltageV8 = static_cast<int16_t>(val/0.1); }

	// Get Status_Reserved_V8
	int16_t statusReservedV8() const { return _statusReservedV8; }
	// Set Status_Reserved_V8
	void statusReservedV8(int16_t val) { _statusReservedV8 = val; }

	// Get Status_Tachometer_V8
	int32_t statusTachometerV8() const { return _statusTachometerV8; }
	// Set Status_Tachometer_V8
	void statusTachometerV8(int32_t val) { _statusTachometerV8 = val; }

private:
	int16_t _statusInputVoltageV8 : 16;
	int16_t _statusReservedV8 : 16;
	int32_t _statusTachometerV8 : 32;
};

// VESC_Status4_V8, can id: 2147487752, length: 8
class VESCStatus4V8Message
{
public:
	VESCStatus4V8Message()
		: _stausMosfetTempV8(0)
		, _stausMotorTempV8(0)
		, _statusTotalInputCurrentV8(0)
		, _statusPIDPosV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147487752; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status4_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Staus_MosfetTemp_V8
	double stausMosfetTempV8() const { return static_cast<double>(_stausMosfetTempV8)*0.1; }
	// Set Staus_MosfetTemp_V8
	void stausMosfetTempV8(double val) { _stausMosfetTempV8 = static_cast<int16_t>(val/0.1); }

	// Get Staus_MotorTemp_V8
	double stausMotorTempV8() const { return static_cast<double>(_stausMotorTempV8)*0.1; }
	// Set Staus_MotorTemp_V8
	void stausMotorTempV8(double val) { _stausMotorTempV8 = static_cast<int16_t>(val/0.1); }

	// Get Status_TotalInputCurrent_V8
	double statusTotalInputCurrentV8() const { return static_cast<double>(_statusTotalInputCurrentV8)*0.1; }
	// Set Status_TotalInputCurrent_V8
	void statusTotalInputCurrentV8(double val) { _statusTotalInputCurrentV8 = static_cast<int16_t>(val/0.1); }

	// Get Status_PIDPos_V8
	double statusPIDPosV8() const { return static_cast<double>(_statusPIDPosV8)*0.02; }
	// Set Status_PIDPos_V8
	void statusPIDPosV8(double val) { _statusPIDPosV8 = static_cast<int16_t>(val/0.02); }

private:
	int16_t _stausMosfetTempV8 : 16;
	int16_t _stausMotorTempV8 : 16;
	int16_t _statusTotalInputCurrentV8 : 16;
	int16_t _statusPIDPosV8 : 16;
};

// VESC_Status3_V8, can id: 2147487496, length: 8
class VESCStatus3V8Message
{
public:
	VESCStatus3V8Message()
		: _statusWattHoursV8(0)
		, _statusWattHoursChargedV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147487496; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status3_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_WattHours_V8
	double statusWattHoursV8() const { return static_cast<double>(_statusWattHoursV8)*1e-05; }
	// Set Status_WattHours_V8
	void statusWattHoursV8(double val) { _statusWattHoursV8 = static_cast<int32_t>(val/1e-05); }

	// Get Status_WattHoursCharged_V8
	double statusWattHoursChargedV8() const { return static_cast<double>(_statusWattHoursChargedV8)*1e-05; }
	// Set Status_WattHoursCharged_V8
	void statusWattHoursChargedV8(double val) { _statusWattHoursChargedV8 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusWattHoursV8 : 32;
	int32_t _statusWattHoursChargedV8 : 32;
};

// VESC_Status2_V8, can id: 2147487240, length: 8
class VESCStatus2V8Message
{
public:
	VESCStatus2V8Message()
		: _statusAmpHoursV8(0)
		, _statusAmpHoursChargedV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147487240; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status2_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_AmpHours_V8
	double statusAmpHoursV8() const { return static_cast<double>(_statusAmpHoursV8)*1e-05; }
	// Set Status_AmpHours_V8
	void statusAmpHoursV8(double val) { _statusAmpHoursV8 = static_cast<int32_t>(val/1e-05); }

	// Get Status_AmpHoursCharged_V8
	double statusAmpHoursChargedV8() const { return static_cast<double>(_statusAmpHoursChargedV8)*1e-05; }
	// Set Status_AmpHoursCharged_V8
	void statusAmpHoursChargedV8(double val) { _statusAmpHoursChargedV8 = static_cast<int32_t>(val/1e-05); }

private:
	int32_t _statusAmpHoursV8 : 32;
	int32_t _statusAmpHoursChargedV8 : 32;
};

// VESC_Status1_V8, can id: 2147485960, length: 8
class VESCStatus1V8Message
{
public:
	VESCStatus1V8Message()
		: _statusRPMV8(0)
		, _statusTotalCurrentV8(0)
		, _statusDutyCycleV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147485960; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VESC_Status1_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Status_RPM_V8
	int32_t statusRPMV8() const { return _statusRPMV8; }
	// Set Status_RPM_V8
	void statusRPMV8(int32_t val) { _statusRPMV8 = val; }

	// Get Status_TotalCurrent_V8
	double statusTotalCurrentV8() const { return static_cast<double>(_statusTotalCurrentV8)*0.1; }
	// Set Status_TotalCurrent_V8
	void statusTotalCurrentV8(double val) { _statusTotalCurrentV8 = static_cast<int16_t>(val/0.1); }

	// Get Status_DutyCycle_V8
	double statusDutyCycleV8() const { return static_cast<double>(_statusDutyCycleV8)*0.1; }
	// Set Status_DutyCycle_V8
	void statusDutyCycleV8(double val) { _statusDutyCycleV8 = static_cast<int16_t>(val/0.1); }

private:
	int32_t _statusRPMV8 : 32;
	int16_t _statusTotalCurrentV8 : 16;
	int16_t _statusDutyCycleV8 : 16;
};

// VESC_Command_DutyCycle_V8, can id: 2147483656, length: 4
class VESCCommandDutyCycleV8Message
{
public:
	VESCCommandDutyCycleV8Message()
		: _commandDutyCycleV8(0)
	{}

	static constexpr uint32_t CanId() { return 2147483656; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "VESC_Command_DutyCycle_V8"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Command_DutyCycle_V8
	double commandDutyCycleV8() const { return static_cast<double>(_commandDutyCycleV8)*0.001; }
	// Set Command_DutyCycle_V8
	void commandDutyCycleV8(double val) { _commandDutyCycleV8 = static_cast<int32_t>(val/0.001); }

private:
	int32_t _commandDutyCycleV8 : 32;
};

enum class ETestClass : uint32_t
{
	EVESCCommandAbsHBrakeCurrentV8 = 2147486728,
	EVESCCommandAbsHBrakeCurrentV7 = 2147486727,
	EVESCCommandAbsHBrakeCurrentV6 = 2147486726,
	EVESCCommandAbsHBrakeCurrentV5 = 2147486725,
	EVESCCommandAbsHBrakeCurrentV4 = 2147486724,
	EVESCCommandAbsHBrakeCurrentV3 = 2147486723,
	EVESCCommandAbsHBrakeCurrentV2 = 2147486722,
	EVESCCommandAbsHBrakeCurrentV1 = 2147486721,
	EVESCCommandRelHBrakeCurrentV8 = 2147486984,
	EVESCCommandRelHBrakeCurrentV7 = 2147486983,
	EVESCCommandRelHBrakeCurrentV6 = 2147486982,
	EVESCCommandRelHBrakeCurrentV5 = 2147486981,
	EVESCCommandRelHBrakeCurrentV4 = 2147486980,
	EVESCCommandRelHBrakeCurrentV3 = 2147486979,
	EVESCCommandRelHBrakeCurrentV2 = 2147486978,
	EVESCCommandRelHBrakeCurrentV1 = 2147486977,
	EVESCSetCurrentLimitPermV1 = 2147489281,
	EVESCSetCurrentLimitTempV1 = 2147489025,
	EVESCCommandRelBrakeCurrentV1 = 2147486465,
	EVESCCommandRelCurrentV1 = 2147486209,
	EVESCCommandPOSV1 = 2147484673,
	EVESCCommandAbsBrakeCurrentV1 = 2147484161,
	EVESCCommandAbsCurrentV1 = 2147483905,
	EVESCCommandRPMV1 = 2147484417,
	EVESCStatus5V1 = 2147490561,
	EVESCStatus4V1 = 2147487745,
	EVESCStatus3V1 = 2147487489,
	EVESCStatus2V1 = 2147487233,
	EVESCStatus1V1 = 2147485953,
	EVESCCommandDutyCycleV1 = 2147483649,
	EVESCSetCurrentLimitPermV2 = 2147489282,
	EVESCSetCurrentLimitTempV2 = 2147489026,
	EVESCCommandRelBrakeCurrentV2 = 2147486466,
	EVESCCommandRelCurrentV2 = 2147486210,
	EVESCCommandPOSV2 = 2147484674,
	EVESCCommandAbsBrakeCurrentV2 = 2147484162,
	EVESCCommandAbsCurrentV2 = 2147483906,
	EVESCCommandRPMV2 = 2147484418,
	EVESCStatus5V2 = 2147490562,
	EVESCStatus4V2 = 2147487746,
	EVESCStatus3V2 = 2147487490,
	EVESCStatus2V2 = 2147487234,
	EVESCStatus1V2 = 2147485954,
	EVESCCommandDutyCycleV2 = 2147483650,
	EVESCSetCurrentLimitPermV3 = 2147489283,
	EVESCSetCurrentLimitTempV3 = 2147489027,
	EVESCCommandRelBrakeCurrentV3 = 2147486467,
	EVESCCommandRelCurrentV3 = 2147486211,
	EVESCCommandPOSV3 = 2147484675,
	EVESCCommandAbsBrakeCurrentV3 = 2147484163,
	EVESCCommandAbsCurrentV3 = 2147483907,
	EVESCCommandRPMV3 = 2147484419,
	EVESCStatus5V3 = 2147490563,
	EVESCStatus4V3 = 2147487747,
	EVESCStatus3V3 = 2147487491,
	EVESCStatus2V3 = 2147487235,
	EVESCStatus1V3 = 2147485955,
	EVESCCommandDutyCycleV3 = 2147483651,
	EVESCSetCurrentLimitPermV4 = 2147489284,
	EVESCSetCurrentLimitTempV4 = 2147489028,
	EVESCCommandRelBrakeCurrentV4 = 2147486468,
	EVESCCommandRelCurrentV4 = 2147486212,
	EVESCCommandPOSV4 = 2147484676,
	EVESCCommandAbsBrakeCurrentV4 = 2147484164,
	EVESCCommandAbsCurrentV4 = 2147483908,
	EVESCCommandRPMV4 = 2147484420,
	EVESCStatus5V4 = 2147490564,
	EVESCStatus4V4 = 2147487748,
	EVESCStatus3V4 = 2147487492,
	EVESCStatus2V4 = 2147487236,
	EVESCStatus1V4 = 2147485956,
	EVESCCommandDutyCycleV4 = 2147483652,
	EVESCSetCurrentLimitPermV5 = 2147489285,
	EVESCSetCurrentLimitTempV5 = 2147489029,
	EVESCCommandRelBrakeCurrentV5 = 2147486469,
	EVESCCommandRelCurrentV5 = 2147486213,
	EVESCCommandPOSV5 = 2147484677,
	EVESCCommandAbsBrakeCurrentV5 = 2147484165,
	EVESCCommandAbsCurrentV5 = 2147483909,
	EVESCCommandRPMV5 = 2147484421,
	EVESCStatus5V5 = 2147490565,
	EVESCStatus4V5 = 2147487749,
	EVESCStatus3V5 = 2147487493,
	EVESCStatus2V5 = 2147487237,
	EVESCStatus1V5 = 2147485957,
	EVESCCommandDutyCycleV5 = 2147483653,
	EVESCSetCurrentLimitPermV6 = 2147489286,
	EVESCSetCurrentLimitTempV6 = 2147489030,
	EVESCCommandRelBrakeCurrentV6 = 2147486470,
	EVESCCommandRelCurrentV6 = 2147486214,
	EVESCCommandPOSV6 = 2147484678,
	EVESCCommandAbsBrakeCurrentV6 = 2147484166,
	EVESCCommandAbsCurrentV6 = 2147483910,
	EVESCCommandRPMV6 = 2147484422,
	EVESCStatus5V6 = 2147490566,
	EVESCStatus4V6 = 2147487750,
	EVESCStatus3V6 = 2147487494,
	EVESCStatus2V6 = 2147487238,
	EVESCStatus1V6 = 2147485958,
	EVESCCommandDutyCycleV6 = 2147483654,
	EVESCSetCurrentLimitPermV7 = 2147489287,
	EVESCSetCurrentLimitTempV7 = 2147489031,
	EVESCCommandRelBrakeCurrentV7 = 2147486471,
	EVESCCommandRelCurrentV7 = 2147486215,
	EVESCCommandPOSV7 = 2147484679,
	EVESCCommandAbsBrakeCurrentV7 = 2147484167,
	EVESCCommandAbsCurrentV7 = 2147483911,
	EVESCCommandRPMV7 = 2147484423,
	EVESCStatus5V7 = 2147490567,
	EVESCStatus4V7 = 2147487751,
	EVESCStatus3V7 = 2147487495,
	EVESCStatus2V7 = 2147487239,
	EVESCStatus1V7 = 2147485959,
	EVESCCommandDutyCycleV7 = 2147483655,
	EVESCSetCurrentLimitPermV8 = 2147489288,
	EVESCSetCurrentLimitTempV8 = 2147489032,
	EVESCCommandRelBrakeCurrentV8 = 2147486472,
	EVESCCommandRelCurrentV8 = 2147486216,
	EVESCCommandPOSV8 = 2147484680,
	EVESCCommandAbsBrakeCurrentV8 = 2147484168,
	EVESCCommandAbsCurrentV8 = 2147483912,
	EVESCCommandRPMV8 = 2147484424,
	EVESCStatus5V8 = 2147490568,
	EVESCStatus4V8 = 2147487752,
	EVESCStatus3V8 = 2147487496,
	EVESCStatus2V8 = 2147487240,
	EVESCStatus1V8 = 2147485960,
	EVESCCommandDutyCycleV8 = 2147483656,
};

#endif // TEST_CLASS_H
