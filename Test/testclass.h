#ifndef TEST_CLASS_H
#define TEST_CLASS_H

#include "dbcgeneratorutils.h"

// Heartbeat, can id: 1, length: 8
class HeartbeatMessage
{
public:
	HeartbeatMessage()
		: _axisError(0)
		, _axisState(0)
		, _motorFlags(0)
		, _encoderFlags(0)
		, _controllerFlags(0)
	{}

	static constexpr uint32_t CanId() { return 1; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Heartbeat"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Axis_Error
	uint32_t axisError() const { return _axisError; }
	// Set Axis_Error
	void axisError(uint32_t val) { _axisError = val; }

	// Get Axis_State
	uint8_t axisState() const { return _axisState; }
	// Set Axis_State
	void axisState(uint8_t val) { _axisState = val; }

	// Get Motor_Flags
	uint8_t motorFlags() const { return _motorFlags; }
	// Set Motor_Flags
	void motorFlags(uint8_t val) { _motorFlags = val; }

	// Get Encoder_Flags
	uint8_t encoderFlags() const { return _encoderFlags; }
	// Set Encoder_Flags
	void encoderFlags(uint8_t val) { _encoderFlags = val; }

	// Get Controller_Flags
	uint8_t controllerFlags() const { return _controllerFlags; }
	// Set Controller_Flags
	void controllerFlags(uint8_t val) { _controllerFlags = val; }

private:
	uint32_t _axisError;
	uint8_t _axisState;
	uint8_t _motorFlags;
	uint8_t _encoderFlags;
	uint8_t _controllerFlags;
};

// Estop, can id: 2, length: 0
class EstopMessage
{
public:
	EstopMessage()
	{}

	static constexpr uint32_t CanId() { return 2; }
	static constexpr uint32_t Length() { return 0; }
	static constexpr const char* MessageName() { return "Estop"; }

private:
};

// Get_Motor_Error, can id: 3, length: 8
class GetMotorErrorMessage
{
public:
	GetMotorErrorMessage()
		: _motorError(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 3; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Get_Motor_Error"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Motor_Error
	uint32_t motorError() const { return _motorError; }
	// Set Motor_Error
	void motorError(uint32_t val) { _motorError = val; }

private:
	uint32_t _motorError;
	[[maybe_unused]] const char _reserved[4];
};

// Get_Encoder_Error, can id: 4, length: 8
class GetEncoderErrorMessage
{
public:
	GetEncoderErrorMessage()
		: _encoderError(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 4; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Get_Encoder_Error"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Encoder_Error
	uint32_t encoderError() const { return _encoderError; }
	// Set Encoder_Error
	void encoderError(uint32_t val) { _encoderError = val; }

private:
	uint32_t _encoderError;
	[[maybe_unused]] const char _reserved[4];
};

// Get_Sensorless_Error, can id: 5, length: 8
class GetSensorlessErrorMessage
{
public:
	GetSensorlessErrorMessage()
		: _sensorlessError(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 5; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Get_Sensorless_Error"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Sensorless_Error
	uint32_t sensorlessError() const { return _sensorlessError; }
	// Set Sensorless_Error
	void sensorlessError(uint32_t val) { _sensorlessError = val; }

private:
	uint32_t _sensorlessError;
	[[maybe_unused]] const char _reserved[4];
};

// Set_Axis_Node_ID, can id: 6, length: 8
class SetAxisNodeIDMessage
{
public:
	SetAxisNodeIDMessage()
		: _axisNodeID(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 6; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Axis_Node_ID"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Axis_Node_ID
	uint32_t axisNodeID() const { return _axisNodeID; }
	// Set Axis_Node_ID
	void axisNodeID(uint32_t val) { _axisNodeID = val; }

private:
	uint32_t _axisNodeID;
	[[maybe_unused]] const char _reserved[4];
};

// Set_Axis_State, can id: 7, length: 8
class SetAxisStateMessage
{
public:
	SetAxisStateMessage()
		: _axisRequestedState(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 7; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Axis_State"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Axis_Requested_State
	uint32_t axisRequestedState() const { return _axisRequestedState; }
	// Set Axis_Requested_State
	void axisRequestedState(uint32_t val) { _axisRequestedState = val; }

private:
	uint32_t _axisRequestedState;
	[[maybe_unused]] const char _reserved[4];
};

// Get_Encoder_Estimates, can id: 9, length: 8
class GetEncoderEstimatesMessage
{
public:
	GetEncoderEstimatesMessage()
		: _posEstimate(0.0f)
		, _velEstimate(0.0f)
	{}

	static constexpr uint32_t CanId() { return 9; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Get_Encoder_Estimates"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Pos_Estimate
	float posEstimate() const { return _posEstimate; }
	// Set Pos_Estimate
	void posEstimate(float val) { _posEstimate = val; }

	// Get Vel_Estimate
	float velEstimate() const { return _velEstimate; }
	// Set Vel_Estimate
	void velEstimate(float val) { _velEstimate = val; }

private:
	float _posEstimate;
	float _velEstimate;
};

// Get_Encoder_Count, can id: 10, length: 8
class GetEncoderCountMessage
{
public:
	GetEncoderCountMessage()
		: _shadowCount(0)
		, _countInCPR(0)
	{}

	static constexpr uint32_t CanId() { return 10; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Get_Encoder_Count"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Shadow_Count
	uint32_t shadowCount() const { return _shadowCount; }
	// Set Shadow_Count
	void shadowCount(uint32_t val) { _shadowCount = val; }

	// Get Count_in_CPR
	uint32_t countInCPR() const { return _countInCPR; }
	// Set Count_in_CPR
	void countInCPR(uint32_t val) { _countInCPR = val; }

private:
	uint32_t _shadowCount;
	uint32_t _countInCPR;
};

// Set_Controller_Mode, can id: 11, length: 8
class SetControllerModeMessage
{
public:
	SetControllerModeMessage()
		: _controlMode(0)
		, _inputMode(0)
	{}

	static constexpr uint32_t CanId() { return 11; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Controller_Mode"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Control_Mode
	uint32_t controlMode() const { return _controlMode; }
	// Set Control_Mode
	void controlMode(uint32_t val) { _controlMode = val; }

	// Get Input_Mode
	uint32_t inputMode() const { return _inputMode - 5; }
	// Set Input_Mode
	void inputMode(uint32_t val) { _inputMode = val + 5; }

private:
	uint32_t _controlMode;
	uint32_t _inputMode;
};

// Set_Input_Pos, can id: 12, length: 8
class SetInputPosMessage
{
public:
	SetInputPosMessage()
		: _inputPos(0.0f)
		, _velFF(0)
		, _torqueFF(0)
	{}

	static constexpr uint32_t CanId() { return 12; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Input_Pos"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Input_Pos
	float inputPos() const { return _inputPos; }
	// Set Input_Pos
	void inputPos(float val) { _inputPos = val; }

	// Get Vel_FF
	double velFF() const { return static_cast<double>(_velFF)*0.001 + 12.2; }
	// Set Vel_FF
	void velFF(double val) { _velFF = static_cast<int16_t>((val - 12.2)/0.001); }

	// Get Torque_FF
	double torqueFF() const { return static_cast<double>(_torqueFF)*0.001; }
	// Set Torque_FF
	void torqueFF(double val) { _torqueFF = static_cast<int16_t>(val/0.001); }

private:
	float _inputPos;
	int16_t _velFF;
	int16_t _torqueFF;
};

// Set_Input_Vel, can id: 13, length: 8
class SetInputVelMessage
{
public:
	SetInputVelMessage()
		: _inputVel(0.0f)
		, _inputTorqueFF(0.0f)
	{}

	static constexpr uint32_t CanId() { return 13; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Input_Vel"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Input_Vel
	float inputVel() const { return _inputVel; }
	// Set Input_Vel
	void inputVel(float val) { _inputVel = val; }

	// Get Input_Torque_FF
	float inputTorqueFF() const { return _inputTorqueFF; }
	// Set Input_Torque_FF
	void inputTorqueFF(float val) { _inputTorqueFF = val; }

private:
	float _inputVel;
	float _inputTorqueFF;
};

// Set_Input_Torque, can id: 14, length: 8
class SetInputTorqueMessage
{
public:
	SetInputTorqueMessage()
		: _inputTorque(0.0f)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 14; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Input_Torque"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Input_Torque
	float inputTorque() const { return _inputTorque; }
	// Set Input_Torque
	void inputTorque(float val) { _inputTorque = val; }

private:
	float _inputTorque;
	[[maybe_unused]] const char _reserved[4];
};

// Set_Limits, can id: 15, length: 8
class SetLimitsMessage
{
public:
	SetLimitsMessage()
		: _velocityLimit(0.0f)
		, _currentLimit(0.0f)
	{}

	static constexpr uint32_t CanId() { return 15; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Limits"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Velocity_Limit
	float velocityLimit() const { return _velocityLimit; }
	// Set Velocity_Limit
	void velocityLimit(float val) { _velocityLimit = val; }

	// Get Current_Limit
	float currentLimit() const { return _currentLimit; }
	// Set Current_Limit
	void currentLimit(float val) { _currentLimit = val; }

private:
	float _velocityLimit;
	float _currentLimit;
};

// Start_Anticogging, can id: 16, length: 0
class StartAnticoggingMessage
{
public:
	StartAnticoggingMessage()
	{}

	static constexpr uint32_t CanId() { return 16; }
	static constexpr uint32_t Length() { return 0; }
	static constexpr const char* MessageName() { return "Start_Anticogging"; }

private:
};

// Set_Traj_Vel_Limit, can id: 17, length: 8
class SetTrajVelLimitMessage
{
public:
	SetTrajVelLimitMessage()
		: _trajVelLimit(0.0f)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 17; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Traj_Vel_Limit"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Traj_Vel_Limit
	float trajVelLimit() const { return _trajVelLimit; }
	// Set Traj_Vel_Limit
	void trajVelLimit(float val) { _trajVelLimit = val; }

private:
	float _trajVelLimit;
	[[maybe_unused]] const char _reserved[4];
};

// Set_Traj_Accel_Limits, can id: 18, length: 8
class SetTrajAccelLimitsMessage
{
public:
	SetTrajAccelLimitsMessage()
		: _trajAccelLimit(0.0f)
		, _trajDecelLimit(0.0f)
	{}

	static constexpr uint32_t CanId() { return 18; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Traj_Accel_Limits"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Traj_Accel_Limit
	float trajAccelLimit() const { return _trajAccelLimit; }
	// Set Traj_Accel_Limit
	void trajAccelLimit(float val) { _trajAccelLimit = val; }

	// Get Traj_Decel_Limit
	float trajDecelLimit() const { return _trajDecelLimit; }
	// Set Traj_Decel_Limit
	void trajDecelLimit(float val) { _trajDecelLimit = val; }

private:
	float _trajAccelLimit;
	float _trajDecelLimit;
};

// Set_Traj_Inertia, can id: 19, length: 8
class SetTrajInertiaMessage
{
public:
	SetTrajInertiaMessage()
		: _trajInertia(0.0f)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 19; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Traj_Inertia"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Traj_Inertia
	float trajInertia() const { return _trajInertia; }
	// Set Traj_Inertia
	void trajInertia(float val) { _trajInertia = val; }

private:
	float _trajInertia;
	[[maybe_unused]] const char _reserved[4];
};

// Get_Iq, can id: 20, length: 8
class GetIqMessage
{
public:
	GetIqMessage()
		: _iqSetpoint(0.0f)
		, _iqMeasured(0.0f)
	{}

	static constexpr uint32_t CanId() { return 20; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Get_Iq"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Iq_Setpoint
	float iqSetpoint() const { return _iqSetpoint; }
	// Set Iq_Setpoint
	void iqSetpoint(float val) { _iqSetpoint = val; }

	// Get Iq_Measured
	float iqMeasured() const { return _iqMeasured; }
	// Set Iq_Measured
	void iqMeasured(float val) { _iqMeasured = val; }

private:
	float _iqSetpoint;
	float _iqMeasured;
};

// Get_Sensorless_Estimates, can id: 21, length: 8
class GetSensorlessEstimatesMessage
{
public:
	GetSensorlessEstimatesMessage()
		: _sensorlessPosEstimate(0.0f)
		, _sensorlessVelEstimate(0.0f)
	{}

	static constexpr uint32_t CanId() { return 21; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Get_Sensorless_Estimates"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Sensorless_Pos_Estimate
	float sensorlessPosEstimate() const { return _sensorlessPosEstimate; }
	// Set Sensorless_Pos_Estimate
	void sensorlessPosEstimate(float val) { _sensorlessPosEstimate = val; }

	// Get Sensorless_Vel_Estimate
	float sensorlessVelEstimate() const { return _sensorlessVelEstimate; }
	// Set Sensorless_Vel_Estimate
	void sensorlessVelEstimate(float val) { _sensorlessVelEstimate = val; }

private:
	float _sensorlessPosEstimate;
	float _sensorlessVelEstimate;
};

// Reboot, can id: 22, length: 0
class RebootMessage
{
public:
	RebootMessage()
	{}

	static constexpr uint32_t CanId() { return 22; }
	static constexpr uint32_t Length() { return 0; }
	static constexpr const char* MessageName() { return "Reboot"; }

private:
};

// Get_Vbus_Voltage, can id: 23, length: 8
class GetVbusVoltageMessage
{
public:
	GetVbusVoltageMessage()
		: _vbusVoltage(0.0f)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 23; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Get_Vbus_Voltage"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Vbus_Voltage
	float vbusVoltage() const { return _vbusVoltage; }
	// Set Vbus_Voltage
	void vbusVoltage(float val) { _vbusVoltage = val; }

private:
	float _vbusVoltage;
	[[maybe_unused]] const char _reserved[4];
};

// Clear_Errors, can id: 24, length: 0
class ClearErrorsMessage
{
public:
	ClearErrorsMessage()
	{}

	static constexpr uint32_t CanId() { return 24; }
	static constexpr uint32_t Length() { return 0; }
	static constexpr const char* MessageName() { return "Clear_Errors"; }

private:
};

// Set_Linear_Count, can id: 25, length: 8
class SetLinearCountMessage
{
public:
	SetLinearCountMessage()
		: _position(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 25; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Linear_Count"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Position
	int32_t position() const { return _position; }
	// Set Position
	void position(int32_t val) { _position = val; }

private:
	int32_t _position;
	[[maybe_unused]] const char _reserved[4];
};

// Set_Pos_Gain, can id: 26, length: 8
class SetPosGainMessage
{
public:
	SetPosGainMessage()
		: _posGain(0.0f)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 26; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Pos_Gain"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Pos_Gain
	float posGain() const { return _posGain; }
	// Set Pos_Gain
	void posGain(float val) { _posGain = val; }

private:
	float _posGain;
	[[maybe_unused]] const char _reserved[4];
};

// Set_Vel_gains, can id: 27, length: 8
class SetVelGainsMessage
{
public:
	SetVelGainsMessage()
		: _velGain(0.0f)
		, _velIntegratorGain(0.0f)
	{}

	static constexpr uint32_t CanId() { return 27; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "Set_Vel_gains"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get Vel_Gain
	float velGain() const { return _velGain; }
	// Set Vel_Gain
	void velGain(float val) { _velGain = val; }

	// Get Vel_Integrator_Gain
	float velIntegratorGain() const { return _velIntegratorGain; }
	// Set Vel_Integrator_Gain
	void velIntegratorGain(float val) { _velIntegratorGain = val; }

private:
	float _velGain;
	float _velIntegratorGain;
};

enum class ETestClass
{
	EHeartbeat = 1,
	EEstop = 2,
	EGetMotorError = 3,
	EGetEncoderError = 4,
	EGetSensorlessError = 5,
	ESetAxisNodeID = 6,
	ESetAxisState = 7,
	EGetEncoderEstimates = 9,
	EGetEncoderCount = 10,
	ESetControllerMode = 11,
	ESetInputPos = 12,
	ESetInputVel = 13,
	ESetInputTorque = 14,
	ESetLimits = 15,
	EStartAnticogging = 16,
	ESetTrajVelLimit = 17,
	ESetTrajAccelLimits = 18,
	ESetTrajInertia = 19,
	EGetIq = 20,
	EGetSensorlessEstimates = 21,
	EReboot = 22,
	EGetVbusVoltage = 23,
	EClearErrors = 24,
	ESetLinearCount = 25,
	ESetPosGain = 26,
	ESetVelGains = 27,
};

#endif // TEST_CLASS_H
