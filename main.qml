import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts

import "qml"
import BackendLib

Window {
    id: mainWindow
    width: 1280
    height: width*9/16
    minimumWidth: 850
    minimumHeight: 200
    visible: true
    title: qsTr("Hello World")

    property var borderColor: Qt.color("grey")
    property int borderWidth: 2

    Backend {
        property int lastErrorLine: -1
        id: backend
        onFileLoaded: function(text) { dbcTextArea.text = text }
        onFileParsed: function (text) {
            console.log("Characters: " + text.length)
            hTextArea.text = text
            resetErrorLine()

            if (errorLine !== -1)
            {
                dbcTextArea.setMarginColor(errorLine, "#cc0000")
            }
        }

        function resetErrorLine() {
            if (lastErrorLine != -1)
            {
                dbcTextArea.setMarginColor(lastErrorLine, "")
            }

            lastErrorLine = -1
        }
    }

    FileDialog {
        id: loadFd
        nameFilters: ["Can DBC files (*.dbc)"]
        onAccepted: {
            backend.loadFile(currentFile)
            backend.resetErrorLine()
        }
    }

    FileDialog {
        id: saveFd
        nameFilters: ["C++ header file (*.h)"]
        fileMode: FileDialog.SaveFile
        selectedFile: backend.lowerName + qsTr(".h")
        onAccepted: {
            backend.selectSaveFile(selectedFile)
            backend.saveFile(hTextArea.text)
        }
    }

    RowLayout {
        anchors.fill: parent
        ColumnLayout {
            Layout.preferredWidth: parent.width / 2
            Layout.maximumWidth: parent.width / 2

            RowLayout {
                Layout.maximumHeight: 50
                Layout.alignment: Qt.AlignHCenter
                Button {
                    Layout.fillHeight: true
                    Layout.preferredWidth: 100
                    text: qsTr("Save")
                }
                Button {
                    Layout.fillHeight: true
                    Layout.preferredWidth: 100
                    text: qsTr("Save As")
                }
                Button {
                    Layout.fillHeight: true
                    Layout.preferredWidth: 100
                    text: qsTr("Load")
                    onClicked: loadFd.open()
                }
                Button {
                    Layout.fillHeight: true
                    Layout.preferredWidth: 100
                    text: qsTr("Parse")
                    onClicked: backend.parse(dbcTextArea.text)
                }
            }

            NumberedTextArea {
                Layout.fillHeight: true
                Layout.fillWidth: true
                id: dbcTextArea

            }

            /*
            Rectangle {
                Layout.fillHeight: true
                Layout.fillWidth: true
                border.color: borderColor
                border.width: borderWidth

                ScrollView {
                    id: scrollView
                    width: parent.width - 2*borderWidth
                    height: parent.height - 2*borderWidth
                    anchors.centerIn: parent

                    RowLayout {
                        implicitWidth: scrollView.width
                        ListView {
                            id: lineNumbers
                            Layout.fillHeight: true
                            Layout.preferredWidth: 40
                            delegate: RowLayout {
                                property bool selected: (dbcTextArea.selectedLineNumber == index)
                                property bool error: (backend.errorLine == index)

                                property var selectedColor: Qt.color("orange")
                                property var errorColor: Qt.color("red")

                                id: root
                                height: dbcTextArea.lineHeight
                                width: 40
                                Item {
                                    Layout.fillHeight: true
                                    Layout.fillWidth: true

                                    Label {
                                        anchors.right: parent.right
                                        text: index + 1
                                        color: error ? errorColor : (selected ? selectedColor : borderColor)
                                    }

                                }
                                Rectangle {
                                    Layout.fillHeight: true
                                    Layout.preferredWidth: 2
                                    border.width: 1
                                    border.color: error ? errorColor : (selected ? selectedColor : borderColor)
                                }
                            }
                            model: ListModel { id: lineNumbersModel }
                        }
                        TextArea {
                            property double lineHeight: (lineCount == 0) ? 0 : height / lineCount
                            property int selectedLineNumber: -1

                            id: dbcTextArea
                            Layout.fillWidth: true
                            text: qsTr("Test\nThree\nLines\nof\ncode\n\n\n\n\n\n\n\n\nmany\nlines\nlater")
                            tabStopDistance: 25
                            onLineCountChanged: {
                                while (lineNumbersModel.count < lineCount)
                                    lineNumbersModel.append({})

                                while (lineNumbersModel.count > lineCount)
                                    lineNumbersModel.remove(lineNumbersModel.count - 1)
                            }
                            onCursorPositionChanged: selectedLineNumber = backend.currentLineNumber(textDocument, cursorPosition)
                            onActiveFocusChanged: {
                                if (!activeFocus)
                                    selectedLineNumber = -1
                            }
                        }
                    }
                }
            }
            */
        }
        ColumnLayout {
            RowLayout {
                Layout.maximumHeight: 50
                Layout.alignment: Qt.AlignHCenter
                Button {
                    Layout.fillHeight: true
                    Layout.preferredWidth: 100
                    text: qsTr("Save")
                    onClicked: saveFd.open()
                }
                Button {
                    Layout.fillHeight: true
                    Layout.preferredWidth: 100
                    text: qsTr("Save As")
                }
                Button {
                    Layout.fillHeight: true
                    Layout.preferredWidth: 100
                    text: qsTr("Load")
                }
            }

            NumberedTextArea {
                id: hTextArea
                Layout.fillHeight: true
                Layout.fillWidth: true
            }

            /*
            Rectangle {
                Layout.fillHeight: true
                Layout.fillWidth: true
                border.color: borderColor
                border.width: borderWidth

                ScrollView {
                    height: parent.height - 2*borderWidth
                    width: parent.width - 2*borderWidth
                    anchors.centerIn: parent

                    TextArea {
                        id: hTextArea
                        tabStopDistance: 25

                    }
                }
            }
            */
        }
    }
}
