#ifndef TESTCLASS_H
#define TESTCLASS_H

#include "dbcgeneratorutils.h"

// GAS_PEDAL_2, can id: 304, length: 8
class GasPedal2Message
{
public:
	GasPedal2Message()
		: _engineTorqueEstimate(0)
		, _engineTorqueRequest(0)
		, _carGas(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 304; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "GAS_PEDAL_2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get ENGINE_TORQUE_ESTIMATE
	int16_t engineTorqueEstimate() const { return _engineTorqueEstimate; }
	// Set ENGINE_TORQUE_ESTIMATE
	void engineTorqueEstimate(int16_t val) { _engineTorqueEstimate = val; }

	// Get ENGINE_TORQUE_REQUEST
	int16_t engineTorqueRequest() const { return _engineTorqueRequest; }
	// Set ENGINE_TORQUE_REQUEST
	void engineTorqueRequest(int16_t val) { _engineTorqueRequest = val; }

	// Get CAR_GAS
	uint8_t carGas() const { return _carGas; }
	// Set CAR_GAS
	void carGas(uint8_t val) { _carGas = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int16_t _engineTorqueEstimate;
	int16_t _engineTorqueRequest;
	uint8_t _carGas;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[3];
};

// GAS_PEDAL, can id: 316, length: 8
class GasPedalMessage
{
public:
	GasPedalMessage()
		: _carGas(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 316; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "GAS_PEDAL"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get CAR_GAS
	uint8_t carGas() const { return _carGas; }
	// Set CAR_GAS
	void carGas(uint8_t val) { _carGas = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _carGas;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[7];
};

// ENGINE_DATA, can id: 344, length: 8
class EngineDataMessage
{
public:
	EngineDataMessage()
		: _xmissionSpeed(0)
		, _engineRpm(0)
		, _xmissionSpeed2(0)
		, _odometer(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 344; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ENGINE_DATA"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get XMISSION_SPEED
	double xmissionSpeed() const { return static_cast<double>(_xmissionSpeed)*0.01; }
	// Set XMISSION_SPEED
	void xmissionSpeed(double val) { _xmissionSpeed = static_cast<uint16_t>(val/0.01); }

	// Get ENGINE_RPM
	uint16_t engineRpm() const { return _engineRpm; }
	// Set ENGINE_RPM
	void engineRpm(uint16_t val) { _engineRpm = val; }

	// Get XMISSION_SPEED2
	double xmissionSpeed2() const { return static_cast<double>(_xmissionSpeed2)*0.01; }
	// Set XMISSION_SPEED2
	void xmissionSpeed2(double val) { _xmissionSpeed2 = static_cast<uint16_t>(val/0.01); }

	// Get ODOMETER
	double odometer() const { return static_cast<double>(_odometer)*10; }
	// Set ODOMETER
	void odometer(double val) { _odometer = static_cast<uint8_t>(val/10); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint16_t _xmissionSpeed;
	uint16_t _engineRpm;
	uint16_t _xmissionSpeed2;
	uint8_t _odometer;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[1];
};

// POWERTRAIN_DATA, can id: 380, length: 8
class PowertrainDataMessage
{
public:
	PowertrainDataMessage()
		: _pedalGas(0)
		, _engineRpm(0)
		, _brakeSwitch(0)
		, _boh17c(0)
		, _accStatus(0)
		, _gasPressed(0)
		, _boh217c(0)
		, _boh317c(0)
		, _brakePressed(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 380; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "POWERTRAIN_DATA"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get PEDAL_GAS
	uint8_t pedalGas() const { return _pedalGas; }
	// Set PEDAL_GAS
	void pedalGas(uint8_t val) { _pedalGas = val; }

	// Get ENGINE_RPM
	uint16_t engineRpm() const { return _engineRpm; }
	// Set ENGINE_RPM
	void engineRpm(uint16_t val) { _engineRpm = val; }

	// Get BRAKE_SWITCH
	Uknown brakeSwitch() const { return _brakeSwitch; }
	// Set BRAKE_SWITCH
	void brakeSwitch(Uknown val) { _brakeSwitch = val; }

	// Get BOH_17C
	Uknown boh17c() const { return _boh17c; }
	// Set BOH_17C
	void boh17c(Uknown val) { _boh17c = val; }

	// Get ACC_STATUS
	Uknown accStatus() const { return _accStatus; }
	// Set ACC_STATUS
	void accStatus(Uknown val) { _accStatus = val; }

	// Get GAS_PRESSED
	Uknown gasPressed() const { return _gasPressed; }
	// Set GAS_PRESSED
	void gasPressed(Uknown val) { _gasPressed = val; }

	// Get BOH2_17C
	uint8_t boh217c() const { return _boh217c; }
	// Set BOH2_17C
	void boh217c(uint8_t val) { _boh217c = val; }

	// Get BOH3_17C
	Uknown boh317c() const { return _boh317c; }
	// Set BOH3_17C
	void boh317c(Uknown val) { _boh317c = val; }

	// Get BRAKE_PRESSED
	Uknown brakePressed() const { return _brakePressed; }
	// Set BRAKE_PRESSED
	void brakePressed(Uknown val) { _brakePressed = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _pedalGas;
	uint16_t _engineRpm;
	Uknown _brakeSwitch;
	Uknown _boh17c;
	Uknown _accStatus;
	Uknown _gasPressed;
	uint8_t _boh217c;
	Uknown _boh317c;
	Uknown _brakePressed;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[2];
};

// VSA_STATUS, can id: 420, length: 8
class VsaStatusMessage
{
public:
	VsaStatusMessage()
		: _userBrake(0)
		, _computerBraking(0)
		, _espDisabled(0)
		, _brakeHoldEnabled(0)
		, _brakeHoldActive(0)
		, _brakeHoldRelated(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 420; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VSA_STATUS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get USER_BRAKE
	double userBrake() const { return static_cast<double>(_userBrake)*0.015625 - 1.60938; }
	// Set USER_BRAKE
	void userBrake(double val) { _userBrake = static_cast<uint16_t>((val + 1.60938)/0.015625); }

	// Get COMPUTER_BRAKING
	Uknown computerBraking() const { return _computerBraking; }
	// Set COMPUTER_BRAKING
	void computerBraking(Uknown val) { _computerBraking = val; }

	// Get ESP_DISABLED
	Uknown espDisabled() const { return _espDisabled; }
	// Set ESP_DISABLED
	void espDisabled(Uknown val) { _espDisabled = val; }

	// Get BRAKE_HOLD_ENABLED
	Uknown brakeHoldEnabled() const { return _brakeHoldEnabled; }
	// Set BRAKE_HOLD_ENABLED
	void brakeHoldEnabled(Uknown val) { _brakeHoldEnabled = val; }

	// Get BRAKE_HOLD_ACTIVE
	Uknown brakeHoldActive() const { return _brakeHoldActive; }
	// Set BRAKE_HOLD_ACTIVE
	void brakeHoldActive(Uknown val) { _brakeHoldActive = val; }

	// Get BRAKE_HOLD_RELATED
	Uknown brakeHoldRelated() const { return _brakeHoldRelated; }
	// Set BRAKE_HOLD_RELATED
	void brakeHoldRelated(Uknown val) { _brakeHoldRelated = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint16_t _userBrake;
	Uknown _computerBraking;
	Uknown _espDisabled;
	Uknown _brakeHoldEnabled;
	Uknown _brakeHoldActive;
	Uknown _brakeHoldRelated;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[5];
};

// STEER_MOTOR_TORQUE, can id: 427, length: 3
class SteerMotorTorqueMessage
{
public:
	SteerMotorTorqueMessage()
		: _motorTorque(0)
		, _configValid(0)
		, _checksum(0)
		, _counter(0)
		, _outputDisabled(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 427; }
	static constexpr uint32_t Length() { return 3; }
	static constexpr const char* MessageName() { return "STEER_MOTOR_TORQUE"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get MOTOR_TORQUE
	uint8_t motorTorque() const { return _motorTorque; }
	// Set MOTOR_TORQUE
	void motorTorque(uint8_t val) { _motorTorque = val; }

	// Get CONFIG_VALID
	Uknown configValid() const { return _configValid; }
	// Set CONFIG_VALID
	void configValid(Uknown val) { _configValid = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

	// Get OUTPUT_DISABLED
	Uknown outputDisabled() const { return _outputDisabled; }
	// Set OUTPUT_DISABLED
	void outputDisabled(Uknown val) { _outputDisabled = val; }

private:
	uint8_t _motorTorque;
	Uknown _configValid;
	Uknown _checksum;
	Uknown _counter;
	Uknown _outputDisabled;
	[[maybe_unused]] const char _reserved[1];
};

// WHEEL_SPEEDS, can id: 464, length: 8
class WheelSpeedsMessage
{
public:
	WheelSpeedsMessage()
		: _wheelSpeedFl(0)
		, _wheelSpeedFr(0)
		, _wheelSpeedRl(0)
		, _wheelSpeedRr(0)
		, _checksum(0)
	{}

	static constexpr uint32_t CanId() { return 464; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "WHEEL_SPEEDS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get WHEEL_SPEED_FL
	double wheelSpeedFl() const { return static_cast<double>(_wheelSpeedFl)*0.01; }
	// Set WHEEL_SPEED_FL
	void wheelSpeedFl(double val) { _wheelSpeedFl = static_cast<int8_t>(val/0.01); }

	// Get WHEEL_SPEED_FR
	double wheelSpeedFr() const { return static_cast<double>(_wheelSpeedFr)*0.01; }
	// Set WHEEL_SPEED_FR
	void wheelSpeedFr(double val) { _wheelSpeedFr = static_cast<int8_t>(val/0.01); }

	// Get WHEEL_SPEED_RL
	double wheelSpeedRl() const { return static_cast<double>(_wheelSpeedRl)*0.01; }
	// Set WHEEL_SPEED_RL
	void wheelSpeedRl(double val) { _wheelSpeedRl = static_cast<int8_t>(val/0.01); }

	// Get WHEEL_SPEED_RR
	double wheelSpeedRr() const { return static_cast<double>(_wheelSpeedRr)*0.01; }
	// Set WHEEL_SPEED_RR
	void wheelSpeedRr(double val) { _wheelSpeedRr = static_cast<int8_t>(val/0.01); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

private:
	int8_t _wheelSpeedFl;
	int8_t _wheelSpeedFr;
	int8_t _wheelSpeedRl;
	int8_t _wheelSpeedRr;
	Uknown _checksum;
};

// VEHICLE_DYNAMICS, can id: 490, length: 8
class VehicleDynamicsMessage
{
public:
	VehicleDynamicsMessage()
		: _latAccel(0)
		, _longAccel(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 490; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "VEHICLE_DYNAMICS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get LAT_ACCEL
	double latAccel() const { return static_cast<double>(_latAccel)*0.0015; }
	// Set LAT_ACCEL
	void latAccel(double val) { _latAccel = static_cast<int16_t>(val/0.0015); }

	// Get LONG_ACCEL
	double longAccel() const { return static_cast<double>(_longAccel)*0.0015; }
	// Set LONG_ACCEL
	void longAccel(double val) { _longAccel = static_cast<int16_t>(val/0.0015); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int16_t _latAccel;
	int16_t _longAccel;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[4];
};

// SEATBELT_STATUS, can id: 773, length: 7
class SeatbeltStatusMessage
{
public:
	SeatbeltStatusMessage()
		: _seatbeltDriverLamp(0)
		, _seatbeltPassUnlatched(0)
		, _seatbeltPassLatched(0)
		, _seatbeltDriverUnlatched(0)
		, _seatbeltDriverLatched(0)
		, _passAirbagOff(0)
		, _passAirbagOn(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 773; }
	static constexpr uint32_t Length() { return 7; }
	static constexpr const char* MessageName() { return "SEATBELT_STATUS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get SEATBELT_DRIVER_LAMP
	Uknown seatbeltDriverLamp() const { return _seatbeltDriverLamp; }
	// Set SEATBELT_DRIVER_LAMP
	void seatbeltDriverLamp(Uknown val) { _seatbeltDriverLamp = val; }

	// Get SEATBELT_PASS_UNLATCHED
	Uknown seatbeltPassUnlatched() const { return _seatbeltPassUnlatched; }
	// Set SEATBELT_PASS_UNLATCHED
	void seatbeltPassUnlatched(Uknown val) { _seatbeltPassUnlatched = val; }

	// Get SEATBELT_PASS_LATCHED
	Uknown seatbeltPassLatched() const { return _seatbeltPassLatched; }
	// Set SEATBELT_PASS_LATCHED
	void seatbeltPassLatched(Uknown val) { _seatbeltPassLatched = val; }

	// Get SEATBELT_DRIVER_UNLATCHED
	Uknown seatbeltDriverUnlatched() const { return _seatbeltDriverUnlatched; }
	// Set SEATBELT_DRIVER_UNLATCHED
	void seatbeltDriverUnlatched(Uknown val) { _seatbeltDriverUnlatched = val; }

	// Get SEATBELT_DRIVER_LATCHED
	Uknown seatbeltDriverLatched() const { return _seatbeltDriverLatched; }
	// Set SEATBELT_DRIVER_LATCHED
	void seatbeltDriverLatched(Uknown val) { _seatbeltDriverLatched = val; }

	// Get PASS_AIRBAG_OFF
	Uknown passAirbagOff() const { return _passAirbagOff; }
	// Set PASS_AIRBAG_OFF
	void passAirbagOff(Uknown val) { _passAirbagOff = val; }

	// Get PASS_AIRBAG_ON
	Uknown passAirbagOn() const { return _passAirbagOn; }
	// Set PASS_AIRBAG_ON
	void passAirbagOn(Uknown val) { _passAirbagOn = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _seatbeltDriverLamp;
	Uknown _seatbeltPassUnlatched;
	Uknown _seatbeltPassLatched;
	Uknown _seatbeltDriverUnlatched;
	Uknown _seatbeltDriverLatched;
	Uknown _passAirbagOff;
	Uknown _passAirbagOn;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[6];
};

// CAR_SPEED, can id: 777, length: 8
class CarSpeedMessage
{
public:
	CarSpeedMessage()
		: _carSpeed(0)
		, _roughCarSpeed(0)
		, _roughCarSpeed2(0)
		, _roughCarSpeed3(0)
		, _lockStatus(0)
		, _checksum(0)
		, _counter(0)
		, _imperialUnit(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 777; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "CAR_SPEED"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get CAR_SPEED
	double carSpeed() const { return static_cast<double>(_carSpeed)*0.01; }
	// Set CAR_SPEED
	void carSpeed(double val) { _carSpeed = static_cast<uint16_t>(val/0.01); }

	// Get ROUGH_CAR_SPEED
	uint8_t roughCarSpeed() const { return _roughCarSpeed; }
	// Set ROUGH_CAR_SPEED
	void roughCarSpeed(uint8_t val) { _roughCarSpeed = val; }

	// Get ROUGH_CAR_SPEED_2
	uint8_t roughCarSpeed2() const { return _roughCarSpeed2; }
	// Set ROUGH_CAR_SPEED_2
	void roughCarSpeed2(uint8_t val) { _roughCarSpeed2 = val; }

	// Get ROUGH_CAR_SPEED_3
	double roughCarSpeed3() const { return static_cast<double>(_roughCarSpeed3)*0.01; }
	// Set ROUGH_CAR_SPEED_3
	void roughCarSpeed3(double val) { _roughCarSpeed3 = static_cast<uint16_t>(val/0.01); }

	// Get LOCK_STATUS
	Uknown lockStatus() const { return _lockStatus; }
	// Set LOCK_STATUS
	void lockStatus(Uknown val) { _lockStatus = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

	// Get IMPERIAL_UNIT
	Uknown imperialUnit() const { return _imperialUnit; }
	// Set IMPERIAL_UNIT
	void imperialUnit(Uknown val) { _imperialUnit = val; }

private:
	uint16_t _carSpeed;
	uint8_t _roughCarSpeed;
	uint8_t _roughCarSpeed2;
	uint16_t _roughCarSpeed3;
	Uknown _lockStatus;
	Uknown _checksum;
	Uknown _counter;
	Uknown _imperialUnit;
	[[maybe_unused]] const char _reserved[1];
};

// ACC_HUD, can id: 780, length: 8
class AccHudMessage
{
public:
	AccHudMessage()
		: _pcmSpeed(0)
		, _pcmGas(0)
		, _cruiseSpeed(0)
		, _enableMiniCar(0)
		, _radarObstructed(0)
		, _fcmProblem(0)
		, _fcmOff(0)
		, _fcmOff2(0)
		, _accProblem(0)
		, _boh(0)
		, _dtcMode(0)
		, _cruiseControlLabel(0)
		, _boh5(0)
		, _boh4(0)
		, _boh3(0)
		, _hudLead(0)
		, _hudDistance(0)
		, _setMeX01(0)
		, _chime(0)
		, _accOn(0)
		, _imperialUnit(0)
		, _setMeX012(0)
		, _checksum(0)
		, _counter(0)
		, _icons(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 780; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ACC_HUD"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get PCM_SPEED
	double pcmSpeed() const { return static_cast<double>(_pcmSpeed)*0.01; }
	// Set PCM_SPEED
	void pcmSpeed(double val) { _pcmSpeed = static_cast<uint16_t>(val/0.01); }

	// Get PCM_GAS
	uint8_t pcmGas() const { return _pcmGas; }
	// Set PCM_GAS
	void pcmGas(uint8_t val) { _pcmGas = val; }

	// Get CRUISE_SPEED
	uint8_t cruiseSpeed() const { return _cruiseSpeed; }
	// Set CRUISE_SPEED
	void cruiseSpeed(uint8_t val) { _cruiseSpeed = val; }

	// Get ENABLE_MINI_CAR
	Uknown enableMiniCar() const { return _enableMiniCar; }
	// Set ENABLE_MINI_CAR
	void enableMiniCar(Uknown val) { _enableMiniCar = val; }

	// Get RADAR_OBSTRUCTED
	Uknown radarObstructed() const { return _radarObstructed; }
	// Set RADAR_OBSTRUCTED
	void radarObstructed(Uknown val) { _radarObstructed = val; }

	// Get FCM_PROBLEM
	Uknown fcmProblem() const { return _fcmProblem; }
	// Set FCM_PROBLEM
	void fcmProblem(Uknown val) { _fcmProblem = val; }

	// Get FCM_OFF
	Uknown fcmOff() const { return _fcmOff; }
	// Set FCM_OFF
	void fcmOff(Uknown val) { _fcmOff = val; }

	// Get FCM_OFF_2
	Uknown fcmOff2() const { return _fcmOff2; }
	// Set FCM_OFF_2
	void fcmOff2(Uknown val) { _fcmOff2 = val; }

	// Get ACC_PROBLEM
	Uknown accProblem() const { return _accProblem; }
	// Set ACC_PROBLEM
	void accProblem(Uknown val) { _accProblem = val; }

	// Get BOH
	Uknown boh() const { return _boh; }
	// Set BOH
	void boh(Uknown val) { _boh = val; }

	// Get DTC_MODE
	Uknown dtcMode() const { return _dtcMode; }
	// Set DTC_MODE
	void dtcMode(Uknown val) { _dtcMode = val; }

	// Get CRUISE_CONTROL_LABEL
	Uknown cruiseControlLabel() const { return _cruiseControlLabel; }
	// Set CRUISE_CONTROL_LABEL
	void cruiseControlLabel(Uknown val) { _cruiseControlLabel = val; }

	// Get BOH_5
	Uknown boh5() const { return _boh5; }
	// Set BOH_5
	void boh5(Uknown val) { _boh5 = val; }

	// Get BOH_4
	Uknown boh4() const { return _boh4; }
	// Set BOH_4
	void boh4(Uknown val) { _boh4 = val; }

	// Get BOH_3
	Uknown boh3() const { return _boh3; }
	// Set BOH_3
	void boh3(Uknown val) { _boh3 = val; }

	// Get HUD_LEAD
	Uknown hudLead() const { return _hudLead; }
	// Set HUD_LEAD
	void hudLead(Uknown val) { _hudLead = val; }

	// Get HUD_DISTANCE
	Uknown hudDistance() const { return _hudDistance; }
	// Set HUD_DISTANCE
	void hudDistance(Uknown val) { _hudDistance = val; }

	// Get SET_ME_X01
	Uknown setMeX01() const { return _setMeX01; }
	// Set SET_ME_X01
	void setMeX01(Uknown val) { _setMeX01 = val; }

	// Get CHIME
	Uknown chime() const { return _chime; }
	// Set CHIME
	void chime(Uknown val) { _chime = val; }

	// Get ACC_ON
	Uknown accOn() const { return _accOn; }
	// Set ACC_ON
	void accOn(Uknown val) { _accOn = val; }

	// Get IMPERIAL_UNIT
	Uknown imperialUnit() const { return _imperialUnit; }
	// Set IMPERIAL_UNIT
	void imperialUnit(Uknown val) { _imperialUnit = val; }

	// Get SET_ME_X01_2
	Uknown setMeX012() const { return _setMeX012; }
	// Set SET_ME_X01_2
	void setMeX012(Uknown val) { _setMeX012 = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

	// Get ICONS
	Uknown icons() const { return _icons; }
	// Set ICONS
	void icons(Uknown val) { _icons = val; }

private:
	uint16_t _pcmSpeed;
	uint8_t _pcmGas;
	uint8_t _cruiseSpeed;
	Uknown _enableMiniCar;
	Uknown _radarObstructed;
	Uknown _fcmProblem;
	Uknown _fcmOff;
	Uknown _fcmOff2;
	Uknown _accProblem;
	Uknown _boh;
	Uknown _dtcMode;
	Uknown _cruiseControlLabel;
	Uknown _boh5;
	Uknown _boh4;
	Uknown _boh3;
	Uknown _hudLead;
	Uknown _hudDistance;
	Uknown _setMeX01;
	Uknown _chime;
	Uknown _accOn;
	Uknown _imperialUnit;
	Uknown _setMeX012;
	Uknown _checksum;
	Uknown _counter;
	Uknown _icons;
	[[maybe_unused]] const char _reserved[1];
};

// CRUISE, can id: 804, length: 8
class CruiseMessage
{
public:
	CruiseMessage()
		: _hudSpeedKph(0)
		, _hudSpeedMph(0)
		, _tripFuelConsumed(0)
		, _cruiseSpeedPcm(0)
		, _boh2(0)
		, _boh3(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 804; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "CRUISE"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get HUD_SPEED_KPH
	uint8_t hudSpeedKph() const { return _hudSpeedKph; }
	// Set HUD_SPEED_KPH
	void hudSpeedKph(uint8_t val) { _hudSpeedKph = val; }

	// Get HUD_SPEED_MPH
	uint8_t hudSpeedMph() const { return _hudSpeedMph; }
	// Set HUD_SPEED_MPH
	void hudSpeedMph(uint8_t val) { _hudSpeedMph = val; }

	// Get TRIP_FUEL_CONSUMED
	uint16_t tripFuelConsumed() const { return _tripFuelConsumed; }
	// Set TRIP_FUEL_CONSUMED
	void tripFuelConsumed(uint16_t val) { _tripFuelConsumed = val; }

	// Get CRUISE_SPEED_PCM
	uint8_t cruiseSpeedPcm() const { return _cruiseSpeedPcm; }
	// Set CRUISE_SPEED_PCM
	void cruiseSpeedPcm(uint8_t val) { _cruiseSpeedPcm = val; }

	// Get BOH2
	int8_t boh2() const { return _boh2; }
	// Set BOH2
	void boh2(int8_t val) { _boh2 = val; }

	// Get BOH3
	uint8_t boh3() const { return _boh3; }
	// Set BOH3
	void boh3(uint8_t val) { _boh3 = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _hudSpeedKph;
	uint8_t _hudSpeedMph;
	uint16_t _tripFuelConsumed;
	uint8_t _cruiseSpeedPcm;
	int8_t _boh2;
	uint8_t _boh3;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[1];
};

// STALK_STATUS, can id: 884, length: 8
class StalkStatusMessage
{
public:
	StalkStatusMessage()
		: _dashboardAlert(0)
		, _highBeamFlash(0)
		, _autoHeadlights(0)
		, _highBeamHold(0)
		, _wiperSwitch(0)
		, _headlightsOn(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 884; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "STALK_STATUS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get DASHBOARD_ALERT
	uint8_t dashboardAlert() const { return _dashboardAlert; }
	// Set DASHBOARD_ALERT
	void dashboardAlert(uint8_t val) { _dashboardAlert = val; }

	// Get HIGH_BEAM_FLASH
	Uknown highBeamFlash() const { return _highBeamFlash; }
	// Set HIGH_BEAM_FLASH
	void highBeamFlash(Uknown val) { _highBeamFlash = val; }

	// Get AUTO_HEADLIGHTS
	Uknown autoHeadlights() const { return _autoHeadlights; }
	// Set AUTO_HEADLIGHTS
	void autoHeadlights(Uknown val) { _autoHeadlights = val; }

	// Get HIGH_BEAM_HOLD
	Uknown highBeamHold() const { return _highBeamHold; }
	// Set HIGH_BEAM_HOLD
	void highBeamHold(Uknown val) { _highBeamHold = val; }

	// Get WIPER_SWITCH
	Uknown wiperSwitch() const { return _wiperSwitch; }
	// Set WIPER_SWITCH
	void wiperSwitch(Uknown val) { _wiperSwitch = val; }

	// Get HEADLIGHTS_ON
	Uknown headlightsOn() const { return _headlightsOn; }
	// Set HEADLIGHTS_ON
	void headlightsOn(Uknown val) { _headlightsOn = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _dashboardAlert;
	Uknown _highBeamFlash;
	Uknown _autoHeadlights;
	Uknown _highBeamHold;
	Uknown _wiperSwitch;
	Uknown _headlightsOn;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[6];
};

// STALK_STATUS_2, can id: 891, length: 8
class StalkStatus2Message
{
public:
	StalkStatus2Message()
		: _wipers(0)
		, _highBeams(0)
		, _lowBeams(0)
		, _parkLights(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 891; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "STALK_STATUS_2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get WIPERS
	Uknown wipers() const { return _wipers; }
	// Set WIPERS
	void wipers(Uknown val) { _wipers = val; }

	// Get HIGH_BEAMS
	Uknown highBeams() const { return _highBeams; }
	// Set HIGH_BEAMS
	void highBeams(Uknown val) { _highBeams = val; }

	// Get LOW_BEAMS
	Uknown lowBeams() const { return _lowBeams; }
	// Set LOW_BEAMS
	void lowBeams(Uknown val) { _lowBeams = val; }

	// Get PARK_LIGHTS
	Uknown parkLights() const { return _parkLights; }
	// Set PARK_LIGHTS
	void parkLights(Uknown val) { _parkLights = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _wipers;
	Uknown _highBeams;
	Uknown _lowBeams;
	Uknown _parkLights;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[7];
};

// DOORS_STATUS, can id: 1029, length: 8
class DoorsStatusMessage
{
public:
	DoorsStatusMessage()
		: _doorOpenFl(0)
		, _doorOpenFr(0)
		, _doorOpenRl(0)
		, _doorOpenRr(0)
		, _trunkOpen(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 1029; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "DOORS_STATUS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get DOOR_OPEN_FL
	Uknown doorOpenFl() const { return _doorOpenFl; }
	// Set DOOR_OPEN_FL
	void doorOpenFl(Uknown val) { _doorOpenFl = val; }

	// Get DOOR_OPEN_FR
	Uknown doorOpenFr() const { return _doorOpenFr; }
	// Set DOOR_OPEN_FR
	void doorOpenFr(Uknown val) { _doorOpenFr = val; }

	// Get DOOR_OPEN_RL
	Uknown doorOpenRl() const { return _doorOpenRl; }
	// Set DOOR_OPEN_RL
	void doorOpenRl(Uknown val) { _doorOpenRl = val; }

	// Get DOOR_OPEN_RR
	Uknown doorOpenRr() const { return _doorOpenRr; }
	// Set DOOR_OPEN_RR
	void doorOpenRr(Uknown val) { _doorOpenRr = val; }

	// Get TRUNK_OPEN
	Uknown trunkOpen() const { return _trunkOpen; }
	// Set TRUNK_OPEN
	void trunkOpen(Uknown val) { _trunkOpen = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _doorOpenFl;
	Uknown _doorOpenFr;
	Uknown _doorOpenRl;
	Uknown _doorOpenRr;
	Uknown _trunkOpen;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[7];
};

// KINEMATICS, can id: 148, length: 8
class KinematicsMessage
{
public:
	KinematicsMessage()
		: _latAccel(0)
		, _longAccel(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 148; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "KINEMATICS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get LAT_ACCEL
	double latAccel() const { return static_cast<double>(_latAccel)*-0.035 + 17.92; }
	// Set LAT_ACCEL
	void latAccel(double val) { _latAccel = static_cast<uint8_t>((val - 17.92)/-0.035); }

	// Get LONG_ACCEL
	double longAccel() const { return static_cast<double>(_longAccel)*-0.035 + 17.92; }
	// Set LONG_ACCEL
	void longAccel(double val) { _longAccel = static_cast<uint8_t>((val - 17.92)/-0.035); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _latAccel;
	uint8_t _longAccel;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[5];
};

// STEERING_CONTROL, can id: 228, length: 5
class SteeringControlMessage
{
public:
	SteeringControlMessage()
		: _steerTorque(0)
		, _setMeX00(0)
		, _steerTorqueRequest(0)
		, _setMeX002(0)
		, _checksum(0)
		, _counter(0)
		, _steerDownToZero(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 228; }
	static constexpr uint32_t Length() { return 5; }
	static constexpr const char* MessageName() { return "STEERING_CONTROL"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get STEER_TORQUE
	int16_t steerTorque() const { return _steerTorque; }
	// Set STEER_TORQUE
	void steerTorque(int16_t val) { _steerTorque = val; }

	// Get SET_ME_X00
	uint8_t setMeX00() const { return _setMeX00; }
	// Set SET_ME_X00
	void setMeX00(uint8_t val) { _setMeX00 = val; }

	// Get STEER_TORQUE_REQUEST
	Uknown steerTorqueRequest() const { return _steerTorqueRequest; }
	// Set STEER_TORQUE_REQUEST
	void steerTorqueRequest(Uknown val) { _steerTorqueRequest = val; }

	// Get SET_ME_X00_2
	uint8_t setMeX002() const { return _setMeX002; }
	// Set SET_ME_X00_2
	void setMeX002(uint8_t val) { _setMeX002 = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

	// Get STEER_DOWN_TO_ZERO
	Uknown steerDownToZero() const { return _steerDownToZero; }
	// Set STEER_DOWN_TO_ZERO
	void steerDownToZero(Uknown val) { _steerDownToZero = val; }

private:
	int16_t _steerTorque;
	uint8_t _setMeX00;
	Uknown _steerTorqueRequest;
	uint8_t _setMeX002;
	Uknown _checksum;
	Uknown _counter;
	Uknown _steerDownToZero;
	[[maybe_unused]] const char _reserved[1];
};

// BOSCH_SUPPLEMENTAL_1, can id: 229, length: 8
class BoschSupplemental1Message
{
public:
	BoschSupplemental1Message()
		: _setMeX04(0)
		, _setMeX00(0)
		, _setMeX80(0)
		, _setMeX10(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 229; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "BOSCH_SUPPLEMENTAL_1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get SET_ME_X04
	uint8_t setMeX04() const { return _setMeX04; }
	// Set SET_ME_X04
	void setMeX04(uint8_t val) { _setMeX04 = val; }

	// Get SET_ME_X00
	uint8_t setMeX00() const { return _setMeX00; }
	// Set SET_ME_X00
	void setMeX00(uint8_t val) { _setMeX00 = val; }

	// Get SET_ME_X80
	uint8_t setMeX80() const { return _setMeX80; }
	// Set SET_ME_X80
	void setMeX80(uint8_t val) { _setMeX80 = val; }

	// Get SET_ME_X10
	uint8_t setMeX10() const { return _setMeX10; }
	// Set SET_ME_X10
	void setMeX10(uint8_t val) { _setMeX10 = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _setMeX04;
	uint8_t _setMeX00;
	uint8_t _setMeX80;
	uint8_t _setMeX10;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[4];
};

// BRAKE_HOLD, can id: 232, length: 7
class BrakeHoldMessage
{
public:
	BrakeHoldMessage()
		: _xmissionSpeed(0)
		, _computerBrakeRequest(0)
		, _computerBrake(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 232; }
	static constexpr uint32_t Length() { return 7; }
	static constexpr const char* MessageName() { return "BRAKE_HOLD"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get XMISSION_SPEED
	uint16_t xmissionSpeed() const { return _xmissionSpeed; }
	// Set XMISSION_SPEED
	void xmissionSpeed(uint16_t val) { _xmissionSpeed = val; }

	// Get COMPUTER_BRAKE_REQUEST
	Uknown computerBrakeRequest() const { return _computerBrakeRequest; }
	// Set COMPUTER_BRAKE_REQUEST
	void computerBrakeRequest(Uknown val) { _computerBrakeRequest = val; }

	// Get COMPUTER_BRAKE
	uint16_t computerBrake() const { return _computerBrake; }
	// Set COMPUTER_BRAKE
	void computerBrake(uint16_t val) { _computerBrake = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint16_t _xmissionSpeed;
	Uknown _computerBrakeRequest;
	uint16_t _computerBrake;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[3];
};

// STEER_STATUS, can id: 399, length: 7
class SteerStatusMessage
{
public:
	SteerStatusMessage()
		: _steerTorqueSensor(0)
		, _steerAngleRate(0)
		, _steerControlActive(0)
		, _steerStatus(0)
		, _steerConfigIndex(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 399; }
	static constexpr uint32_t Length() { return 7; }
	static constexpr const char* MessageName() { return "STEER_STATUS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get STEER_TORQUE_SENSOR
	double steerTorqueSensor() const { return static_cast<double>(_steerTorqueSensor)*-1; }
	// Set STEER_TORQUE_SENSOR
	void steerTorqueSensor(double val) { _steerTorqueSensor = static_cast<int16_t>(val/-1); }

	// Get STEER_ANGLE_RATE
	double steerAngleRate() const { return static_cast<double>(_steerAngleRate)*-0.1; }
	// Set STEER_ANGLE_RATE
	void steerAngleRate(double val) { _steerAngleRate = static_cast<int16_t>(val/-0.1); }

	// Get STEER_CONTROL_ACTIVE
	Uknown steerControlActive() const { return _steerControlActive; }
	// Set STEER_CONTROL_ACTIVE
	void steerControlActive(Uknown val) { _steerControlActive = val; }

	// Get STEER_STATUS
	Uknown steerStatus() const { return _steerStatus; }
	// Set STEER_STATUS
	void steerStatus(Uknown val) { _steerStatus = val; }

	// Get STEER_CONFIG_INDEX
	Uknown steerConfigIndex() const { return _steerConfigIndex; }
	// Set STEER_CONFIG_INDEX
	void steerConfigIndex(Uknown val) { _steerConfigIndex = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int16_t _steerTorqueSensor;
	int16_t _steerAngleRate;
	Uknown _steerControlActive;
	Uknown _steerStatus;
	Uknown _steerConfigIndex;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[2];
};

// EPB_STATUS, can id: 450, length: 8
class EpbStatusMessage
{
public:
	EpbStatusMessage()
		: _epbActive(0)
		, _epbState(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 450; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "EPB_STATUS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get EPB_ACTIVE
	Uknown epbActive() const { return _epbActive; }
	// Set EPB_ACTIVE
	void epbActive(Uknown val) { _epbActive = val; }

	// Get EPB_STATE
	Uknown epbState() const { return _epbState; }
	// Set EPB_STATE
	void epbState(Uknown val) { _epbState = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _epbActive;
	Uknown _epbState;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[7];
};

// XXX_16, can id: 545, length: 6
class Xxx16Message
{
public:
	Xxx16Message()
		: _econOn(0)
		, _driveMode(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 545; }
	static constexpr uint32_t Length() { return 6; }
	static constexpr const char* MessageName() { return "XXX_16"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get ECON_ON
	Uknown econOn() const { return _econOn; }
	// Set ECON_ON
	void econOn(Uknown val) { _econOn = val; }

	// Get DRIVE_MODE
	Uknown driveMode() const { return _driveMode; }
	// Set DRIVE_MODE
	void driveMode(Uknown val) { _driveMode = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _econOn;
	Uknown _driveMode;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[5];
};

// LEFT_LANE_LINE_1, can id: 576, length: 8
class LeftLaneLine1Message
{
public:
	LeftLaneLine1Message()
		: _lineAngle(0)
		, _frameIndex(0)
		, _lineOffset(0)
		, _lineDistanceVisible(0)
		, _lineProbability(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 576; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "LEFT_LANE_LINE_1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get LINE_ANGLE
	double lineAngle() const { return static_cast<double>(_lineAngle)*0.0005 - 1.024; }
	// Set LINE_ANGLE
	void lineAngle(double val) { _lineAngle = static_cast<int8_t>((val + 1.024)/0.0005); }

	// Get FRAME_INDEX
	Uknown frameIndex() const { return _frameIndex; }
	// Set FRAME_INDEX
	void frameIndex(Uknown val) { _frameIndex = val; }

	// Get LINE_OFFSET
	double lineOffset() const { return static_cast<double>(_lineOffset)*0.004 - 8.192; }
	// Set LINE_OFFSET
	void lineOffset(double val) { _lineOffset = static_cast<int8_t>((val + 8.192)/0.004); }

	// Get LINE_DISTANCE_VISIBLE
	uint8_t lineDistanceVisible() const { return _lineDistanceVisible; }
	// Set LINE_DISTANCE_VISIBLE
	void lineDistanceVisible(uint8_t val) { _lineDistanceVisible = val; }

	// Get LINE_PROBABILITY
	double lineProbability() const { return static_cast<double>(_lineProbability)*0.015625; }
	// Set LINE_PROBABILITY
	void lineProbability(double val) { _lineProbability = static_cast<uint8_t>(val/0.015625); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int8_t _lineAngle;
	Uknown _frameIndex;
	int8_t _lineOffset;
	uint8_t _lineDistanceVisible;
	uint8_t _lineProbability;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[2];
};

// LEFT_LANE_LINE_2, can id: 577, length: 8
class LeftLaneLine2Message
{
public:
	LeftLaneLine2Message()
		: _frameIndex(0)
		, _lineSolid(0)
		, _lineDashed(0)
		, _lineCurvature(0)
		, _lineParameter(0)
		, _lineFarEdgePosition(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 577; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "LEFT_LANE_LINE_2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get FRAME_INDEX
	Uknown frameIndex() const { return _frameIndex; }
	// Set FRAME_INDEX
	void frameIndex(Uknown val) { _frameIndex = val; }

	// Get LINE_SOLID
	Uknown lineSolid() const { return _lineSolid; }
	// Set LINE_SOLID
	void lineSolid(Uknown val) { _lineSolid = val; }

	// Get LINE_DASHED
	Uknown lineDashed() const { return _lineDashed; }
	// Set LINE_DASHED
	void lineDashed(Uknown val) { _lineDashed = val; }

	// Get LINE_CURVATURE
	double lineCurvature() const { return static_cast<double>(_lineCurvature)*1e-05 - 0.02048; }
	// Set LINE_CURVATURE
	void lineCurvature(double val) { _lineCurvature = static_cast<int8_t>((val + 0.02048)/1e-05); }

	// Get LINE_PARAMETER
	int8_t lineParameter() const { return _lineParameter; }
	// Set LINE_PARAMETER
	void lineParameter(int8_t val) { _lineParameter = val; }

	// Get LINE_FAR_EDGE_POSITION
	uint8_t lineFarEdgePosition() const { return _lineFarEdgePosition - 128; }
	// Set LINE_FAR_EDGE_POSITION
	void lineFarEdgePosition(uint8_t val) { _lineFarEdgePosition = val + 128; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _frameIndex;
	Uknown _lineSolid;
	Uknown _lineDashed;
	int8_t _lineCurvature;
	int8_t _lineParameter;
	uint8_t _lineFarEdgePosition;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[3];
};

// RIGHT_LANE_LINE_1, can id: 579, length: 8
class RightLaneLine1Message
{
public:
	RightLaneLine1Message()
		: _lineAngle(0)
		, _frameIndex(0)
		, _lineOffset(0)
		, _lineDistanceVisible(0)
		, _lineProbability(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 579; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "RIGHT_LANE_LINE_1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get LINE_ANGLE
	double lineAngle() const { return static_cast<double>(_lineAngle)*0.0005 - 1.024; }
	// Set LINE_ANGLE
	void lineAngle(double val) { _lineAngle = static_cast<int8_t>((val + 1.024)/0.0005); }

	// Get FRAME_INDEX
	Uknown frameIndex() const { return _frameIndex; }
	// Set FRAME_INDEX
	void frameIndex(Uknown val) { _frameIndex = val; }

	// Get LINE_OFFSET
	double lineOffset() const { return static_cast<double>(_lineOffset)*0.004 - 8.192; }
	// Set LINE_OFFSET
	void lineOffset(double val) { _lineOffset = static_cast<int8_t>((val + 8.192)/0.004); }

	// Get LINE_DISTANCE_VISIBLE
	uint8_t lineDistanceVisible() const { return _lineDistanceVisible; }
	// Set LINE_DISTANCE_VISIBLE
	void lineDistanceVisible(uint8_t val) { _lineDistanceVisible = val; }

	// Get LINE_PROBABILITY
	double lineProbability() const { return static_cast<double>(_lineProbability)*0.015625; }
	// Set LINE_PROBABILITY
	void lineProbability(double val) { _lineProbability = static_cast<uint8_t>(val/0.015625); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int8_t _lineAngle;
	Uknown _frameIndex;
	int8_t _lineOffset;
	uint8_t _lineDistanceVisible;
	uint8_t _lineProbability;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[2];
};

// RIGHT_LANE_LINE_2, can id: 580, length: 8
class RightLaneLine2Message
{
public:
	RightLaneLine2Message()
		: _frameIndex(0)
		, _lineSolid(0)
		, _lineDashed(0)
		, _lineCurvature(0)
		, _lineParameter(0)
		, _lineFarEdgePosition(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 580; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "RIGHT_LANE_LINE_2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get FRAME_INDEX
	Uknown frameIndex() const { return _frameIndex; }
	// Set FRAME_INDEX
	void frameIndex(Uknown val) { _frameIndex = val; }

	// Get LINE_SOLID
	Uknown lineSolid() const { return _lineSolid; }
	// Set LINE_SOLID
	void lineSolid(Uknown val) { _lineSolid = val; }

	// Get LINE_DASHED
	Uknown lineDashed() const { return _lineDashed; }
	// Set LINE_DASHED
	void lineDashed(Uknown val) { _lineDashed = val; }

	// Get LINE_CURVATURE
	double lineCurvature() const { return static_cast<double>(_lineCurvature)*1e-05 - 0.02048; }
	// Set LINE_CURVATURE
	void lineCurvature(double val) { _lineCurvature = static_cast<int8_t>((val + 0.02048)/1e-05); }

	// Get LINE_PARAMETER
	int8_t lineParameter() const { return _lineParameter; }
	// Set LINE_PARAMETER
	void lineParameter(int8_t val) { _lineParameter = val; }

	// Get LINE_FAR_EDGE_POSITION
	uint8_t lineFarEdgePosition() const { return _lineFarEdgePosition - 128; }
	// Set LINE_FAR_EDGE_POSITION
	void lineFarEdgePosition(uint8_t val) { _lineFarEdgePosition = val + 128; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _frameIndex;
	Uknown _lineSolid;
	Uknown _lineDashed;
	int8_t _lineCurvature;
	int8_t _lineParameter;
	uint8_t _lineFarEdgePosition;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[3];
};

// ADJACENT_LEFT_LANE_LINE_1, can id: 582, length: 8
class AdjacentLeftLaneLine1Message
{
public:
	AdjacentLeftLaneLine1Message()
		: _lineAngle(0)
		, _frameIndex(0)
		, _lineOffset(0)
		, _lineDistanceVisible(0)
		, _lineProbability(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 582; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ADJACENT_LEFT_LANE_LINE_1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get LINE_ANGLE
	double lineAngle() const { return static_cast<double>(_lineAngle)*0.0005 - 1.024; }
	// Set LINE_ANGLE
	void lineAngle(double val) { _lineAngle = static_cast<int8_t>((val + 1.024)/0.0005); }

	// Get FRAME_INDEX
	Uknown frameIndex() const { return _frameIndex; }
	// Set FRAME_INDEX
	void frameIndex(Uknown val) { _frameIndex = val; }

	// Get LINE_OFFSET
	double lineOffset() const { return static_cast<double>(_lineOffset)*0.004 - 8.192; }
	// Set LINE_OFFSET
	void lineOffset(double val) { _lineOffset = static_cast<int8_t>((val + 8.192)/0.004); }

	// Get LINE_DISTANCE_VISIBLE
	uint8_t lineDistanceVisible() const { return _lineDistanceVisible; }
	// Set LINE_DISTANCE_VISIBLE
	void lineDistanceVisible(uint8_t val) { _lineDistanceVisible = val; }

	// Get LINE_PROBABILITY
	double lineProbability() const { return static_cast<double>(_lineProbability)*0.015625; }
	// Set LINE_PROBABILITY
	void lineProbability(double val) { _lineProbability = static_cast<uint8_t>(val/0.015625); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int8_t _lineAngle;
	Uknown _frameIndex;
	int8_t _lineOffset;
	uint8_t _lineDistanceVisible;
	uint8_t _lineProbability;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[2];
};

// ADJACENT_LEFT_LANE_LINE_2, can id: 583, length: 8
class AdjacentLeftLaneLine2Message
{
public:
	AdjacentLeftLaneLine2Message()
		: _frameIndex(0)
		, _lineSolid(0)
		, _lineDashed(0)
		, _lineCurvature(0)
		, _lineParameter(0)
		, _lineFarEdgePosition(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 583; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ADJACENT_LEFT_LANE_LINE_2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get FRAME_INDEX
	Uknown frameIndex() const { return _frameIndex; }
	// Set FRAME_INDEX
	void frameIndex(Uknown val) { _frameIndex = val; }

	// Get LINE_SOLID
	Uknown lineSolid() const { return _lineSolid; }
	// Set LINE_SOLID
	void lineSolid(Uknown val) { _lineSolid = val; }

	// Get LINE_DASHED
	Uknown lineDashed() const { return _lineDashed; }
	// Set LINE_DASHED
	void lineDashed(Uknown val) { _lineDashed = val; }

	// Get LINE_CURVATURE
	double lineCurvature() const { return static_cast<double>(_lineCurvature)*1e-05 - 0.02048; }
	// Set LINE_CURVATURE
	void lineCurvature(double val) { _lineCurvature = static_cast<int8_t>((val + 0.02048)/1e-05); }

	// Get LINE_PARAMETER
	int8_t lineParameter() const { return _lineParameter; }
	// Set LINE_PARAMETER
	void lineParameter(int8_t val) { _lineParameter = val; }

	// Get LINE_FAR_EDGE_POSITION
	uint8_t lineFarEdgePosition() const { return _lineFarEdgePosition - 128; }
	// Set LINE_FAR_EDGE_POSITION
	void lineFarEdgePosition(uint8_t val) { _lineFarEdgePosition = val + 128; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _frameIndex;
	Uknown _lineSolid;
	Uknown _lineDashed;
	int8_t _lineCurvature;
	int8_t _lineParameter;
	uint8_t _lineFarEdgePosition;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[3];
};

// ADJACENT_RIGHT_LANE_LINE_1, can id: 585, length: 8
class AdjacentRightLaneLine1Message
{
public:
	AdjacentRightLaneLine1Message()
		: _lineAngle(0)
		, _frameIndex(0)
		, _lineOffset(0)
		, _lineDistanceVisible(0)
		, _lineProbability(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 585; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ADJACENT_RIGHT_LANE_LINE_1"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get LINE_ANGLE
	double lineAngle() const { return static_cast<double>(_lineAngle)*0.0005 - 1.024; }
	// Set LINE_ANGLE
	void lineAngle(double val) { _lineAngle = static_cast<int8_t>((val + 1.024)/0.0005); }

	// Get FRAME_INDEX
	Uknown frameIndex() const { return _frameIndex; }
	// Set FRAME_INDEX
	void frameIndex(Uknown val) { _frameIndex = val; }

	// Get LINE_OFFSET
	double lineOffset() const { return static_cast<double>(_lineOffset)*0.004 - 8.192; }
	// Set LINE_OFFSET
	void lineOffset(double val) { _lineOffset = static_cast<int8_t>((val + 8.192)/0.004); }

	// Get LINE_DISTANCE_VISIBLE
	uint8_t lineDistanceVisible() const { return _lineDistanceVisible; }
	// Set LINE_DISTANCE_VISIBLE
	void lineDistanceVisible(uint8_t val) { _lineDistanceVisible = val; }

	// Get LINE_PROBABILITY
	double lineProbability() const { return static_cast<double>(_lineProbability)*0.015625; }
	// Set LINE_PROBABILITY
	void lineProbability(double val) { _lineProbability = static_cast<uint8_t>(val/0.015625); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int8_t _lineAngle;
	Uknown _frameIndex;
	int8_t _lineOffset;
	uint8_t _lineDistanceVisible;
	uint8_t _lineProbability;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[2];
};

// ADJACENT_RIGHT_LANE_LINE_2, can id: 586, length: 8
class AdjacentRightLaneLine2Message
{
public:
	AdjacentRightLaneLine2Message()
		: _frameIndex(0)
		, _lineSolid(0)
		, _lineDashed(0)
		, _lineCurvature(0)
		, _lineParameter(0)
		, _lineFarEdgePosition(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 586; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ADJACENT_RIGHT_LANE_LINE_2"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get FRAME_INDEX
	Uknown frameIndex() const { return _frameIndex; }
	// Set FRAME_INDEX
	void frameIndex(Uknown val) { _frameIndex = val; }

	// Get LINE_SOLID
	Uknown lineSolid() const { return _lineSolid; }
	// Set LINE_SOLID
	void lineSolid(Uknown val) { _lineSolid = val; }

	// Get LINE_DASHED
	Uknown lineDashed() const { return _lineDashed; }
	// Set LINE_DASHED
	void lineDashed(Uknown val) { _lineDashed = val; }

	// Get LINE_CURVATURE
	double lineCurvature() const { return static_cast<double>(_lineCurvature)*1e-05 - 0.02048; }
	// Set LINE_CURVATURE
	void lineCurvature(double val) { _lineCurvature = static_cast<int8_t>((val + 0.02048)/1e-05); }

	// Get LINE_PARAMETER
	int8_t lineParameter() const { return _lineParameter; }
	// Set LINE_PARAMETER
	void lineParameter(int8_t val) { _lineParameter = val; }

	// Get LINE_FAR_EDGE_POSITION
	uint8_t lineFarEdgePosition() const { return _lineFarEdgePosition - 128; }
	// Set LINE_FAR_EDGE_POSITION
	void lineFarEdgePosition(uint8_t val) { _lineFarEdgePosition = val + 128; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _frameIndex;
	Uknown _lineSolid;
	Uknown _lineDashed;
	int8_t _lineCurvature;
	int8_t _lineParameter;
	uint8_t _lineFarEdgePosition;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[3];
};

// ROUGH_WHEEL_SPEED, can id: 597, length: 8
class RoughWheelSpeedMessage
{
public:
	RoughWheelSpeedMessage()
		: _wheelSpeedFl(0)
		, _wheelSpeedFr(0)
		, _wheelSpeedRl(0)
		, _wheelSpeedRr(0)
		, _setToX55(0)
		, _setToX552(0)
		, _longCounter(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 597; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ROUGH_WHEEL_SPEED"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get WHEEL_SPEED_FL
	uint8_t wheelSpeedFl() const { return _wheelSpeedFl; }
	// Set WHEEL_SPEED_FL
	void wheelSpeedFl(uint8_t val) { _wheelSpeedFl = val; }

	// Get WHEEL_SPEED_FR
	uint8_t wheelSpeedFr() const { return _wheelSpeedFr; }
	// Set WHEEL_SPEED_FR
	void wheelSpeedFr(uint8_t val) { _wheelSpeedFr = val; }

	// Get WHEEL_SPEED_RL
	uint8_t wheelSpeedRl() const { return _wheelSpeedRl; }
	// Set WHEEL_SPEED_RL
	void wheelSpeedRl(uint8_t val) { _wheelSpeedRl = val; }

	// Get WHEEL_SPEED_RR
	uint8_t wheelSpeedRr() const { return _wheelSpeedRr; }
	// Set WHEEL_SPEED_RR
	void wheelSpeedRr(uint8_t val) { _wheelSpeedRr = val; }

	// Get SET_TO_X55
	uint8_t setToX55() const { return _setToX55; }
	// Set SET_TO_X55
	void setToX55(uint8_t val) { _setToX55 = val; }

	// Get SET_TO_X55_2
	uint8_t setToX552() const { return _setToX552; }
	// Set SET_TO_X55_2
	void setToX552(uint8_t val) { _setToX552 = val; }

	// Get LONG_COUNTER
	uint8_t longCounter() const { return _longCounter; }
	// Set LONG_COUNTER
	void longCounter(uint8_t val) { _longCounter = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _wheelSpeedFl;
	uint8_t _wheelSpeedFr;
	uint8_t _wheelSpeedRl;
	uint8_t _wheelSpeedRr;
	uint8_t _setToX55;
	uint8_t _setToX552;
	uint8_t _longCounter;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[1];
};

// SCM_BUTTONS, can id: 662, length: 4
class ScmButtonsMessage
{
public:
	ScmButtonsMessage()
		: _cruiseSetting(0)
		, _cruiseButtons(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 662; }
	static constexpr uint32_t Length() { return 4; }
	static constexpr const char* MessageName() { return "SCM_BUTTONS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get CRUISE_SETTING
	Uknown cruiseSetting() const { return _cruiseSetting; }
	// Set CRUISE_SETTING
	void cruiseSetting(Uknown val) { _cruiseSetting = val; }

	// Get CRUISE_BUTTONS
	Uknown cruiseButtons() const { return _cruiseButtons; }
	// Set CRUISE_BUTTONS
	void cruiseButtons(Uknown val) { _cruiseButtons = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _cruiseSetting;
	Uknown _cruiseButtons;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[3];
};

// SCM_FEEDBACK, can id: 806, length: 8
class ScmFeedbackMessage
{
public:
	ScmFeedbackMessage()
		: _driversDoorOpen(0)
		, _cmbsStates(0)
		, _leftBlinker(0)
		, _rightBlinker(0)
		, _mainOn(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 806; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "SCM_FEEDBACK"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get DRIVERS_DOOR_OPEN
	Uknown driversDoorOpen() const { return _driversDoorOpen; }
	// Set DRIVERS_DOOR_OPEN
	void driversDoorOpen(Uknown val) { _driversDoorOpen = val; }

	// Get CMBS_STATES
	Uknown cmbsStates() const { return _cmbsStates; }
	// Set CMBS_STATES
	void cmbsStates(Uknown val) { _cmbsStates = val; }

	// Get LEFT_BLINKER
	Uknown leftBlinker() const { return _leftBlinker; }
	// Set LEFT_BLINKER
	void leftBlinker(Uknown val) { _leftBlinker = val; }

	// Get RIGHT_BLINKER
	Uknown rightBlinker() const { return _rightBlinker; }
	// Set RIGHT_BLINKER
	void rightBlinker(Uknown val) { _rightBlinker = val; }

	// Get MAIN_ON
	Uknown mainOn() const { return _mainOn; }
	// Set MAIN_ON
	void mainOn(Uknown val) { _mainOn = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _driversDoorOpen;
	Uknown _cmbsStates;
	Uknown _leftBlinker;
	Uknown _rightBlinker;
	Uknown _mainOn;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[7];
};

// CAMERA_MESSAGES, can id: 862, length: 8
class CameraMessagesMessage
{
public:
	CameraMessagesMessage()
		: _zerosBoh(0)
		, _zerosBoh2(0)
		, _highbeamsOn(0)
		, _autoHighbeamsActive(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 862; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "CAMERA_MESSAGES"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get ZEROS_BOH
	int32_t zerosBoh() const { return _zerosBoh; }
	// Set ZEROS_BOH
	void zerosBoh(int32_t val) { _zerosBoh = val; }

	// Get ZEROS_BOH_2
	Uknown zerosBoh2() const { return _zerosBoh2; }
	// Set ZEROS_BOH_2
	void zerosBoh2(Uknown val) { _zerosBoh2 = val; }

	// Get HIGHBEAMS_ON
	Uknown highbeamsOn() const { return _highbeamsOn; }
	// Set HIGHBEAMS_ON
	void highbeamsOn(Uknown val) { _highbeamsOn = val; }

	// Get AUTO_HIGHBEAMS_ACTIVE
	Uknown autoHighbeamsActive() const { return _autoHighbeamsActive; }
	// Set AUTO_HIGHBEAMS_ACTIVE
	void autoHighbeamsActive(Uknown val) { _autoHighbeamsActive = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int32_t _zerosBoh;
	Uknown _zerosBoh2;
	Uknown _highbeamsOn;
	Uknown _autoHighbeamsActive;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[1];
};

// RADAR_HUD, can id: 927, length: 8
class RadarHudMessage
{
public:
	RadarHudMessage()
		: _zerosBoh(0)
		, _zerosBoh2(0)
		, _cmbsOff(0)
		, _setTo1(0)
		, _accAlerts(0)
		, _resumeInstruction(0)
		, _setTo0(0)
		, _applyBrakesForCanc(0)
		, _setTo64(0)
		, _leadDistance(0)
		, _hudLead(0)
		, _zerosBoh3(0)
		, _zerosBoh4(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 927; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "RADAR_HUD"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get ZEROS_BOH
	uint8_t zerosBoh() const { return _zerosBoh; }
	// Set ZEROS_BOH
	void zerosBoh(uint8_t val) { _zerosBoh = val; }

	// Get ZEROS_BOH2
	Uknown zerosBoh2() const { return _zerosBoh2; }
	// Set ZEROS_BOH2
	void zerosBoh2(Uknown val) { _zerosBoh2 = val; }

	// Get CMBS_OFF
	Uknown cmbsOff() const { return _cmbsOff; }
	// Set CMBS_OFF
	void cmbsOff(Uknown val) { _cmbsOff = val; }

	// Get SET_TO_1
	Uknown setTo1() const { return _setTo1; }
	// Set SET_TO_1
	void setTo1(Uknown val) { _setTo1 = val; }

	// Get ACC_ALERTS
	Uknown accAlerts() const { return _accAlerts; }
	// Set ACC_ALERTS
	void accAlerts(Uknown val) { _accAlerts = val; }

	// Get RESUME_INSTRUCTION
	Uknown resumeInstruction() const { return _resumeInstruction; }
	// Set RESUME_INSTRUCTION
	void resumeInstruction(Uknown val) { _resumeInstruction = val; }

	// Get SET_TO_0
	Uknown setTo0() const { return _setTo0; }
	// Set SET_TO_0
	void setTo0(Uknown val) { _setTo0 = val; }

	// Get APPLY_BRAKES_FOR_CANC
	Uknown applyBrakesForCanc() const { return _applyBrakesForCanc; }
	// Set APPLY_BRAKES_FOR_CANC
	void applyBrakesForCanc(Uknown val) { _applyBrakesForCanc = val; }

	// Get SET_TO_64
	uint8_t setTo64() const { return _setTo64; }
	// Set SET_TO_64
	void setTo64(uint8_t val) { _setTo64 = val; }

	// Get LEAD_DISTANCE
	uint8_t leadDistance() const { return _leadDistance; }
	// Set LEAD_DISTANCE
	void leadDistance(uint8_t val) { _leadDistance = val; }

	// Get HUD_LEAD
	Uknown hudLead() const { return _hudLead; }
	// Set HUD_LEAD
	void hudLead(Uknown val) { _hudLead = val; }

	// Get ZEROS_BOH3
	uint8_t zerosBoh3() const { return _zerosBoh3; }
	// Set ZEROS_BOH3
	void zerosBoh3(uint8_t val) { _zerosBoh3 = val; }

	// Get ZEROS_BOH4
	uint8_t zerosBoh4() const { return _zerosBoh4; }
	// Set ZEROS_BOH4
	void zerosBoh4(uint8_t val) { _zerosBoh4 = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _zerosBoh;
	Uknown _zerosBoh2;
	Uknown _cmbsOff;
	Uknown _setTo1;
	Uknown _accAlerts;
	Uknown _resumeInstruction;
	Uknown _setTo0;
	Uknown _applyBrakesForCanc;
	uint8_t _setTo64;
	uint8_t _leadDistance;
	Uknown _hudLead;
	uint8_t _zerosBoh3;
	uint8_t _zerosBoh4;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[1];
};

// LKAS_HUD_A, can id: 13274, length: 5
class LkasHudAMessage
{
public:
	LkasHudAMessage()
		: _setMeX41(0)
		, _boh(0)
		, _camTempHigh(0)
		, _steeringRequired(0)
		, _ldwRight(0)
		, _solidLanes(0)
		, _lkasOff(0)
		, _lkasProblem(0)
		, _dtc(0)
		, _dashedLanes(0)
		, _beep(0)
		, _setMeX01(0)
		, _ldwProblem(0)
		, _boh(0)
		, _cleanWindshield(0)
		, _ldwOff(0)
		, _ldwOn(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 13274; }
	static constexpr uint32_t Length() { return 5; }
	static constexpr const char* MessageName() { return "LKAS_HUD_A"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get SET_ME_X41
	uint8_t setMeX41() const { return _setMeX41; }
	// Set SET_ME_X41
	void setMeX41(uint8_t val) { _setMeX41 = val; }

	// Get BOH
	uint8_t boh() const { return _boh; }
	// Set BOH
	void boh(uint8_t val) { _boh = val; }

	// Get CAM_TEMP_HIGH
	Uknown camTempHigh() const { return _camTempHigh; }
	// Set CAM_TEMP_HIGH
	void camTempHigh(Uknown val) { _camTempHigh = val; }

	// Get STEERING_REQUIRED
	Uknown steeringRequired() const { return _steeringRequired; }
	// Set STEERING_REQUIRED
	void steeringRequired(Uknown val) { _steeringRequired = val; }

	// Get LDW_RIGHT
	Uknown ldwRight() const { return _ldwRight; }
	// Set LDW_RIGHT
	void ldwRight(Uknown val) { _ldwRight = val; }

	// Get SOLID_LANES
	Uknown solidLanes() const { return _solidLanes; }
	// Set SOLID_LANES
	void solidLanes(Uknown val) { _solidLanes = val; }

	// Get LKAS_OFF
	Uknown lkasOff() const { return _lkasOff; }
	// Set LKAS_OFF
	void lkasOff(Uknown val) { _lkasOff = val; }

	// Get LKAS_PROBLEM
	Uknown lkasProblem() const { return _lkasProblem; }
	// Set LKAS_PROBLEM
	void lkasProblem(Uknown val) { _lkasProblem = val; }

	// Get DTC
	Uknown dtc() const { return _dtc; }
	// Set DTC
	void dtc(Uknown val) { _dtc = val; }

	// Get DASHED_LANES
	Uknown dashedLanes() const { return _dashedLanes; }
	// Set DASHED_LANES
	void dashedLanes(Uknown val) { _dashedLanes = val; }

	// Get BEEP
	Uknown beep() const { return _beep; }
	// Set BEEP
	void beep(Uknown val) { _beep = val; }

	// Get SET_ME_X01
	Uknown setMeX01() const { return _setMeX01; }
	// Set SET_ME_X01
	void setMeX01(Uknown val) { _setMeX01 = val; }

	// Get LDW_PROBLEM
	Uknown ldwProblem() const { return _ldwProblem; }
	// Set LDW_PROBLEM
	void ldwProblem(Uknown val) { _ldwProblem = val; }

	// Get BOH
	Uknown boh() const { return _boh; }
	// Set BOH
	void boh(Uknown val) { _boh = val; }

	// Get CLEAN_WINDSHIELD
	Uknown cleanWindshield() const { return _cleanWindshield; }
	// Set CLEAN_WINDSHIELD
	void cleanWindshield(Uknown val) { _cleanWindshield = val; }

	// Get LDW_OFF
	Uknown ldwOff() const { return _ldwOff; }
	// Set LDW_OFF
	void ldwOff(Uknown val) { _ldwOff = val; }

	// Get LDW_ON
	Uknown ldwOn() const { return _ldwOn; }
	// Set LDW_ON
	void ldwOn(Uknown val) { _ldwOn = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _setMeX41;
	uint8_t _boh;
	Uknown _camTempHigh;
	Uknown _steeringRequired;
	Uknown _ldwRight;
	Uknown _solidLanes;
	Uknown _lkasOff;
	Uknown _lkasProblem;
	Uknown _dtc;
	Uknown _dashedLanes;
	Uknown _beep;
	Uknown _setMeX01;
	Uknown _ldwProblem;
	Uknown _boh;
	Uknown _cleanWindshield;
	Uknown _ldwOff;
	Uknown _ldwOn;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[1];
};

// LKAS_HUD_B, can id: 13275, length: 8
class LkasHudBMessage
{
public:
	LkasHudBMessage()
		: _setMeX41(0)
		, _boh(0)
		, _camTempHigh(0)
		, _steeringRequired(0)
		, _ldwRight(0)
		, _solidLanes(0)
		, _lkasOff(0)
		, _lkasProblem(0)
		, _dtc(0)
		, _dashedLanes(0)
		, _beep(0)
		, _setMeX01(0)
		, _ldwProblem(0)
		, _boh(0)
		, _cleanWindshield(0)
		, _ldwOff(0)
		, _ldwOn(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 13275; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "LKAS_HUD_B"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get SET_ME_X41
	uint8_t setMeX41() const { return _setMeX41; }
	// Set SET_ME_X41
	void setMeX41(uint8_t val) { _setMeX41 = val; }

	// Get BOH
	uint8_t boh() const { return _boh; }
	// Set BOH
	void boh(uint8_t val) { _boh = val; }

	// Get CAM_TEMP_HIGH
	Uknown camTempHigh() const { return _camTempHigh; }
	// Set CAM_TEMP_HIGH
	void camTempHigh(Uknown val) { _camTempHigh = val; }

	// Get STEERING_REQUIRED
	Uknown steeringRequired() const { return _steeringRequired; }
	// Set STEERING_REQUIRED
	void steeringRequired(Uknown val) { _steeringRequired = val; }

	// Get LDW_RIGHT
	Uknown ldwRight() const { return _ldwRight; }
	// Set LDW_RIGHT
	void ldwRight(Uknown val) { _ldwRight = val; }

	// Get SOLID_LANES
	Uknown solidLanes() const { return _solidLanes; }
	// Set SOLID_LANES
	void solidLanes(Uknown val) { _solidLanes = val; }

	// Get LKAS_OFF
	Uknown lkasOff() const { return _lkasOff; }
	// Set LKAS_OFF
	void lkasOff(Uknown val) { _lkasOff = val; }

	// Get LKAS_PROBLEM
	Uknown lkasProblem() const { return _lkasProblem; }
	// Set LKAS_PROBLEM
	void lkasProblem(Uknown val) { _lkasProblem = val; }

	// Get DTC
	Uknown dtc() const { return _dtc; }
	// Set DTC
	void dtc(Uknown val) { _dtc = val; }

	// Get DASHED_LANES
	Uknown dashedLanes() const { return _dashedLanes; }
	// Set DASHED_LANES
	void dashedLanes(Uknown val) { _dashedLanes = val; }

	// Get BEEP
	Uknown beep() const { return _beep; }
	// Set BEEP
	void beep(Uknown val) { _beep = val; }

	// Get SET_ME_X01
	Uknown setMeX01() const { return _setMeX01; }
	// Set SET_ME_X01
	void setMeX01(Uknown val) { _setMeX01 = val; }

	// Get LDW_PROBLEM
	Uknown ldwProblem() const { return _ldwProblem; }
	// Set LDW_PROBLEM
	void ldwProblem(Uknown val) { _ldwProblem = val; }

	// Get BOH
	Uknown boh() const { return _boh; }
	// Set BOH
	void boh(Uknown val) { _boh = val; }

	// Get CLEAN_WINDSHIELD
	Uknown cleanWindshield() const { return _cleanWindshield; }
	// Set CLEAN_WINDSHIELD
	void cleanWindshield(Uknown val) { _cleanWindshield = val; }

	// Get LDW_OFF
	Uknown ldwOff() const { return _ldwOff; }
	// Set LDW_OFF
	void ldwOff(Uknown val) { _ldwOff = val; }

	// Get LDW_ON
	Uknown ldwOn() const { return _ldwOn; }
	// Set LDW_ON
	void ldwOn(Uknown val) { _ldwOn = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _setMeX41;
	uint8_t _boh;
	Uknown _camTempHigh;
	Uknown _steeringRequired;
	Uknown _ldwRight;
	Uknown _solidLanes;
	Uknown _lkasOff;
	Uknown _lkasProblem;
	Uknown _dtc;
	Uknown _dashedLanes;
	Uknown _beep;
	Uknown _setMeX01;
	Uknown _ldwProblem;
	Uknown _boh;
	Uknown _cleanWindshield;
	Uknown _ldwOff;
	Uknown _ldwOn;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[4];
};

// ACC_CONTROL, can id: 479, length: 8
class AccControlMessage
{
public:
	AccControlMessage()
		: _gasCommand(0)
		, _setTo0(0)
		, _controlOn(0)
		, _accelCommand(0)
		, _aebStatus(0)
		, _brakeRequest(0)
		, _standstill(0)
		, _standstillRelease(0)
		, _aebPrepare(0)
		, _aebBraking(0)
		, _checksum(0)
		, _counter(0)
		, _brakeLights(0)
		, _reserved{ 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 479; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ACC_CONTROL"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get GAS_COMMAND
	int16_t gasCommand() const { return _gasCommand; }
	// Set GAS_COMMAND
	void gasCommand(int16_t val) { _gasCommand = val; }

	// Get SET_TO_0
	Uknown setTo0() const { return _setTo0; }
	// Set SET_TO_0
	void setTo0(Uknown val) { _setTo0 = val; }

	// Get CONTROL_ON
	Uknown controlOn() const { return _controlOn; }
	// Set CONTROL_ON
	void controlOn(Uknown val) { _controlOn = val; }

	// Get ACCEL_COMMAND
	double accelCommand() const { return static_cast<double>(_accelCommand)*0.01; }
	// Set ACCEL_COMMAND
	void accelCommand(double val) { _accelCommand = static_cast<int8_t>(val/0.01); }

	// Get AEB_STATUS
	Uknown aebStatus() const { return _aebStatus; }
	// Set AEB_STATUS
	void aebStatus(Uknown val) { _aebStatus = val; }

	// Get BRAKE_REQUEST
	Uknown brakeRequest() const { return _brakeRequest; }
	// Set BRAKE_REQUEST
	void brakeRequest(Uknown val) { _brakeRequest = val; }

	// Get STANDSTILL
	Uknown standstill() const { return _standstill; }
	// Set STANDSTILL
	void standstill(Uknown val) { _standstill = val; }

	// Get STANDSTILL_RELEASE
	Uknown standstillRelease() const { return _standstillRelease; }
	// Set STANDSTILL_RELEASE
	void standstillRelease(Uknown val) { _standstillRelease = val; }

	// Get AEB_PREPARE
	Uknown aebPrepare() const { return _aebPrepare; }
	// Set AEB_PREPARE
	void aebPrepare(Uknown val) { _aebPrepare = val; }

	// Get AEB_BRAKING
	Uknown aebBraking() const { return _aebBraking; }
	// Set AEB_BRAKING
	void aebBraking(Uknown val) { _aebBraking = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

	// Get BRAKE_LIGHTS
	Uknown brakeLights() const { return _brakeLights; }
	// Set BRAKE_LIGHTS
	void brakeLights(Uknown val) { _brakeLights = val; }

private:
	int16_t _gasCommand;
	Uknown _setTo0;
	Uknown _controlOn;
	int8_t _accelCommand;
	Uknown _aebStatus;
	Uknown _brakeRequest;
	Uknown _standstill;
	Uknown _standstillRelease;
	Uknown _aebPrepare;
	Uknown _aebBraking;
	Uknown _checksum;
	Uknown _counter;
	Uknown _brakeLights;
	[[maybe_unused]] const char _reserved[2];
};

// ACC_CONTROL_ON, can id: 495, length: 8
class AccControlOnMessage
{
public:
	AccControlOnMessage()
		: _setTo3(0)
		, _controlOn(0)
		, _setToFf(0)
		, _zerosBoh(0)
		, _setTo75(0)
		, _setTo30(0)
		, _zerosBoh2(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 495; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ACC_CONTROL_ON"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get SET_TO_3
	uint8_t setTo3() const { return _setTo3; }
	// Set SET_TO_3
	void setTo3(uint8_t val) { _setTo3 = val; }

	// Get CONTROL_ON
	Uknown controlOn() const { return _controlOn; }
	// Set CONTROL_ON
	void controlOn(Uknown val) { _controlOn = val; }

	// Get SET_TO_FF
	uint8_t setToFf() const { return _setToFf; }
	// Set SET_TO_FF
	void setToFf(uint8_t val) { _setToFf = val; }

	// Get ZEROS_BOH
	uint8_t zerosBoh() const { return _zerosBoh; }
	// Set ZEROS_BOH
	void zerosBoh(uint8_t val) { _zerosBoh = val; }

	// Get SET_TO_75
	uint8_t setTo75() const { return _setTo75; }
	// Set SET_TO_75
	void setTo75(uint8_t val) { _setTo75 = val; }

	// Get SET_TO_30
	uint8_t setTo30() const { return _setTo30; }
	// Set SET_TO_30
	void setTo30(uint8_t val) { _setTo30 = val; }

	// Get ZEROS_BOH2
	uint16_t zerosBoh2() const { return _zerosBoh2; }
	// Set ZEROS_BOH2
	void zerosBoh2(uint16_t val) { _zerosBoh2 = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _setTo3;
	Uknown _controlOn;
	uint8_t _setToFf;
	uint8_t _zerosBoh;
	uint8_t _setTo75;
	uint8_t _setTo30;
	uint16_t _zerosBoh2;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[1];
};

// LKAS_HUD, can id: 829, length: 5
class LkasHudMessage
{
public:
	LkasHudMessage()
		: _setMeX41(0)
		, _boh(0)
		, _camTempHigh(0)
		, _steeringRequired(0)
		, _ldwRight(0)
		, _solidLanes(0)
		, _lkasOff(0)
		, _lkasProblem(0)
		, _dtc(0)
		, _dashedLanes(0)
		, _beep(0)
		, _ldwProblem(0)
		, _boh(0)
		, _cleanWindshield(0)
		, _ldwOff(0)
		, _ldwOn(0)
		, _setMeX48(0)
		, _checksum(0)
		, _counter(0)
	{}

	static constexpr uint32_t CanId() { return 829; }
	static constexpr uint32_t Length() { return 5; }
	static constexpr const char* MessageName() { return "LKAS_HUD"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get SET_ME_X41
	uint8_t setMeX41() const { return _setMeX41; }
	// Set SET_ME_X41
	void setMeX41(uint8_t val) { _setMeX41 = val; }

	// Get BOH
	uint8_t boh() const { return _boh; }
	// Set BOH
	void boh(uint8_t val) { _boh = val; }

	// Get CAM_TEMP_HIGH
	Uknown camTempHigh() const { return _camTempHigh; }
	// Set CAM_TEMP_HIGH
	void camTempHigh(Uknown val) { _camTempHigh = val; }

	// Get STEERING_REQUIRED
	Uknown steeringRequired() const { return _steeringRequired; }
	// Set STEERING_REQUIRED
	void steeringRequired(Uknown val) { _steeringRequired = val; }

	// Get LDW_RIGHT
	Uknown ldwRight() const { return _ldwRight; }
	// Set LDW_RIGHT
	void ldwRight(Uknown val) { _ldwRight = val; }

	// Get SOLID_LANES
	Uknown solidLanes() const { return _solidLanes; }
	// Set SOLID_LANES
	void solidLanes(Uknown val) { _solidLanes = val; }

	// Get LKAS_OFF
	Uknown lkasOff() const { return _lkasOff; }
	// Set LKAS_OFF
	void lkasOff(Uknown val) { _lkasOff = val; }

	// Get LKAS_PROBLEM
	Uknown lkasProblem() const { return _lkasProblem; }
	// Set LKAS_PROBLEM
	void lkasProblem(Uknown val) { _lkasProblem = val; }

	// Get DTC
	Uknown dtc() const { return _dtc; }
	// Set DTC
	void dtc(Uknown val) { _dtc = val; }

	// Get DASHED_LANES
	Uknown dashedLanes() const { return _dashedLanes; }
	// Set DASHED_LANES
	void dashedLanes(Uknown val) { _dashedLanes = val; }

	// Get BEEP
	Uknown beep() const { return _beep; }
	// Set BEEP
	void beep(Uknown val) { _beep = val; }

	// Get LDW_PROBLEM
	Uknown ldwProblem() const { return _ldwProblem; }
	// Set LDW_PROBLEM
	void ldwProblem(Uknown val) { _ldwProblem = val; }

	// Get BOH
	Uknown boh() const { return _boh; }
	// Set BOH
	void boh(Uknown val) { _boh = val; }

	// Get CLEAN_WINDSHIELD
	Uknown cleanWindshield() const { return _cleanWindshield; }
	// Set CLEAN_WINDSHIELD
	void cleanWindshield(Uknown val) { _cleanWindshield = val; }

	// Get LDW_OFF
	Uknown ldwOff() const { return _ldwOff; }
	// Set LDW_OFF
	void ldwOff(Uknown val) { _ldwOff = val; }

	// Get LDW_ON
	Uknown ldwOn() const { return _ldwOn; }
	// Set LDW_ON
	void ldwOn(Uknown val) { _ldwOn = val; }

	// Get SET_ME_X48
	uint8_t setMeX48() const { return _setMeX48; }
	// Set SET_ME_X48
	void setMeX48(uint8_t val) { _setMeX48 = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _setMeX41;
	uint8_t _boh;
	Uknown _camTempHigh;
	Uknown _steeringRequired;
	Uknown _ldwRight;
	Uknown _solidLanes;
	Uknown _lkasOff;
	Uknown _lkasProblem;
	Uknown _dtc;
	Uknown _dashedLanes;
	Uknown _beep;
	Uknown _ldwProblem;
	Uknown _boh;
	Uknown _cleanWindshield;
	Uknown _ldwOff;
	Uknown _ldwOn;
	uint8_t _setMeX48;
	Uknown _checksum;
	Uknown _counter;
};

// STEERING_SENSORS, can id: 330, length: 8
class SteeringSensorsMessage
{
public:
	SteeringSensorsMessage()
		: _steerAngle(0)
		, _steerAngleRate(0)
		, _steerSensorStatus3(0)
		, _steerSensorStatus2(0)
		, _steerSensorStatus1(0)
		, _steerWheelAngle(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0 }
	{}

	static constexpr uint32_t CanId() { return 330; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "STEERING_SENSORS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get STEER_ANGLE
	double steerAngle() const { return static_cast<double>(_steerAngle)*-0.1; }
	// Set STEER_ANGLE
	void steerAngle(double val) { _steerAngle = static_cast<int16_t>(val/-0.1); }

	// Get STEER_ANGLE_RATE
	double steerAngleRate() const { return static_cast<double>(_steerAngleRate)*-1; }
	// Set STEER_ANGLE_RATE
	void steerAngleRate(double val) { _steerAngleRate = static_cast<int16_t>(val/-1); }

	// Get STEER_SENSOR_STATUS_3
	Uknown steerSensorStatus3() const { return _steerSensorStatus3; }
	// Set STEER_SENSOR_STATUS_3
	void steerSensorStatus3(Uknown val) { _steerSensorStatus3 = val; }

	// Get STEER_SENSOR_STATUS_2
	Uknown steerSensorStatus2() const { return _steerSensorStatus2; }
	// Set STEER_SENSOR_STATUS_2
	void steerSensorStatus2(Uknown val) { _steerSensorStatus2 = val; }

	// Get STEER_SENSOR_STATUS_1
	Uknown steerSensorStatus1() const { return _steerSensorStatus1; }
	// Set STEER_SENSOR_STATUS_1
	void steerSensorStatus1(Uknown val) { _steerSensorStatus1 = val; }

	// Get STEER_WHEEL_ANGLE
	double steerWheelAngle() const { return static_cast<double>(_steerWheelAngle)*-0.1; }
	// Set STEER_WHEEL_ANGLE
	void steerWheelAngle(double val) { _steerWheelAngle = static_cast<int16_t>(val/-0.1); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int16_t _steerAngle;
	int16_t _steerAngleRate;
	Uknown _steerSensorStatus3;
	Uknown _steerSensorStatus2;
	Uknown _steerSensorStatus1;
	int16_t _steerWheelAngle;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[1];
};

// GEARBOX_15T, can id: 401, length: 8
class Gearbox15tMessage
{
public:
	Gearbox15tMessage()
		: _gearShifter(0)
		, _gear2(0)
		, _gear(0)
		, _boh(0)
		, _zerosBoh(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 401; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "GEARBOX_15T"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get GEAR_SHIFTER
	uint8_t gearShifter() const { return _gearShifter; }
	// Set GEAR_SHIFTER
	void gearShifter(uint8_t val) { _gearShifter = val; }

	// Get GEAR2
	uint8_t gear2() const { return _gear2; }
	// Set GEAR2
	void gear2(uint8_t val) { _gear2 = val; }

	// Get GEAR
	uint8_t gear() const { return _gear; }
	// Set GEAR
	void gear(uint8_t val) { _gear = val; }

	// Get BOH
	uint8_t boh() const { return _boh; }
	// Set BOH
	void boh(uint8_t val) { _boh = val; }

	// Get ZEROS_BOH
	Uknown zerosBoh() const { return _zerosBoh; }
	// Set ZEROS_BOH
	void zerosBoh(Uknown val) { _zerosBoh = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _gearShifter;
	uint8_t _gear2;
	uint8_t _gear;
	uint8_t _boh;
	Uknown _zerosBoh;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[4];
};

// GEARBOX, can id: 419, length: 8
class GearboxMessage
{
public:
	GearboxMessage()
		: _gearShifter(0)
		, _gear(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 419; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "GEARBOX"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get GEAR_SHIFTER
	uint8_t gearShifter() const { return _gearShifter; }
	// Set GEAR_SHIFTER
	void gearShifter(uint8_t val) { _gearShifter = val; }

	// Get GEAR
	uint8_t gear() const { return _gear; }
	// Set GEAR
	void gear(uint8_t val) { _gear = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _gearShifter;
	uint8_t _gear;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[6];
};

// STANDSTILL, can id: 432, length: 7
class StandstillMessage
{
public:
	StandstillMessage()
		: _brakeError2(0)
		, _brakeError1(0)
		, _wheelsMoving(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 432; }
	static constexpr uint32_t Length() { return 7; }
	static constexpr const char* MessageName() { return "STANDSTILL"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get BRAKE_ERROR_2
	Uknown brakeError2() const { return _brakeError2; }
	// Set BRAKE_ERROR_2
	void brakeError2(Uknown val) { _brakeError2 = val; }

	// Get BRAKE_ERROR_1
	Uknown brakeError1() const { return _brakeError1; }
	// Set BRAKE_ERROR_1
	void brakeError1(Uknown val) { _brakeError1 = val; }

	// Get WHEELS_MOVING
	Uknown wheelsMoving() const { return _wheelsMoving; }
	// Set WHEELS_MOVING
	void wheelsMoving(Uknown val) { _wheelsMoving = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _brakeError2;
	Uknown _brakeError1;
	Uknown _wheelsMoving;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[6];
};

// BRAKE_MODULE, can id: 446, length: 3
class BrakeModuleMessage
{
public:
	BrakeModuleMessage()
		: _brakePressed(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 446; }
	static constexpr uint32_t Length() { return 3; }
	static constexpr const char* MessageName() { return "BRAKE_MODULE"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get BRAKE_PRESSED
	Uknown brakePressed() const { return _brakePressed; }
	// Set BRAKE_PRESSED
	void brakePressed(Uknown val) { _brakePressed = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	Uknown _brakePressed;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[3];
};

// LEGACY_BRAKE_COMMAND, can id: 506, length: 8
class LegacyBrakeCommandMessage
{
public:
	LegacyBrakeCommandMessage()
		: _chime(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 506; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "LEGACY_BRAKE_COMMAND"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get CHIME
	uint8_t chime() const { return _chime; }
	// Set CHIME
	void chime(uint8_t val) { _chime = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	uint8_t _chime;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[7];
};

// CRUISE_PARAMS, can id: 892, length: 8
class CruiseParamsMessage
{
public:
	CruiseParamsMessage()
		: _cruiseSpeedOffset(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 892; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "CRUISE_PARAMS"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get CRUISE_SPEED_OFFSET
	double cruiseSpeedOffset() const { return static_cast<double>(_cruiseSpeedOffset)*0.1; }
	// Set CRUISE_SPEED_OFFSET
	void cruiseSpeedOffset(double val) { _cruiseSpeedOffset = static_cast<int8_t>(val/0.1); }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int8_t _cruiseSpeedOffset;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[7];
};

// ODOMETER, can id: 1302, length: 8
class OdometerMessage
{
public:
	OdometerMessage()
		: _odometer(0)
		, _checksum(0)
		, _counter(0)
		, _reserved{ 0, 0, 0, 0, 0 }
	{}

	static constexpr uint32_t CanId() { return 1302; }
	static constexpr uint32_t Length() { return 8; }
	static constexpr const char* MessageName() { return "ODOMETER"; }

	char* data() { return reinterpret_cast<char*>(this); }
	const char* data() const { return reinterpret_cast<const char*>(this); }

	// Get ODOMETER
	int16_t odometer() const { return _odometer; }
	// Set ODOMETER
	void odometer(int16_t val) { _odometer = val; }

	// Get CHECKSUM
	Uknown checksum() const { return _checksum; }
	// Set CHECKSUM
	void checksum(Uknown val) { _checksum = val; }

	// Get COUNTER
	Uknown counter() const { return _counter; }
	// Set COUNTER
	void counter(Uknown val) { _counter = val; }

private:
	int16_t _odometer;
	Uknown _checksum;
	Uknown _counter;
	[[maybe_unused]] const char _reserved[5];
};

enum class ETestclass : uint32_t
{
	EGasPedal2 = 304,
	EGasPedal = 316,
	EEngineData = 344,
	EPowertrainData = 380,
	EVsaStatus = 420,
	ESteerMotorTorque = 427,
	EWheelSpeeds = 464,
	EVehicleDynamics = 490,
	ESeatbeltStatus = 773,
	ECarSpeed = 777,
	EAccHud = 780,
	ECruise = 804,
	EStalkStatus = 884,
	EStalkStatus2 = 891,
	EDoorsStatus = 1029,
	EKinematics = 148,
	ESteeringControl = 228,
	EBoschSupplemental1 = 229,
	EBrakeHold = 232,
	ESteerStatus = 399,
	EEpbStatus = 450,
	EXxx16 = 545,
	ELeftLaneLine1 = 576,
	ELeftLaneLine2 = 577,
	ERightLaneLine1 = 579,
	ERightLaneLine2 = 580,
	EAdjacentLeftLaneLine1 = 582,
	EAdjacentLeftLaneLine2 = 583,
	EAdjacentRightLaneLine1 = 585,
	EAdjacentRightLaneLine2 = 586,
	ERoughWheelSpeed = 597,
	EScmButtons = 662,
	EScmFeedback = 806,
	ECameraMessages = 862,
	ERadarHud = 927,
	ELkasHudA = 13274,
	ELkasHudB = 13275,
	EAccControl = 479,
	EAccControlOn = 495,
	ELkasHud = 829,
	ESteeringSensors = 330,
	EGearbox15t = 401,
	EGearbox = 419,
	EStandstill = 432,
	EBrakeModule = 446,
	ELegacyBrakeCommand = 506,
	ECruiseParams = 892,
	EOdometer = 1302,
};

#endif // TESTCLASS_H
