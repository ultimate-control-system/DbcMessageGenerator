#include "backend.h"

#include <QDebug>

Backend::Backend(QObject *parent)
    : QObject{parent}
    , _validFile(false)
    , _validName(false)
    , _parsedSuccess(false)
    , _saveFileSelected(false)
    , _lineNo(0)
    , _errorLine(-1)
{
    // ToDo: remove this
    setClassName("TestClass");
}

QRegularExpression Backend::Regex = QRegularExpression();

void Backend::ParseName(const QStringList &in, int &lineNo, QString &out)
{
    out.clear();
    out = in[lineNo];

    if (in[lineNo + 1] == QStringLiteral(":"))
        ++lineNo;
    else
        out.remove(':');

}

void Backend::loadFile(const QUrl &url)
{
//    QUrl newUrl(url);
//    QString newUrl(url);
//    newUrl.remove("file:///");

    qDebug() << "Loading file: " << url.toLocalFile(); //.path();// .fileName();

    QFile dbcFile;
    dbcFile.setFileName(url.toLocalFile());

//    QString text;

    dbcFile.open(QIODevice::ReadOnly);
    if (dbcFile.isOpen())
    {
        _validFile = true;
        QString out = dbcFile.readAll();
        dbcFile.close();
        emit fileLoaded(out);
    }
    else
    {
        _validFile = false;
        QString errMessage
                = QString("File \"") + dbcFile.fileName()
                  + "\" open failed\nError code "
                  + QString::number(dbcFile.error()) + ": \""
                  + dbcFile.errorString() + "\"";
        qDebug() << errMessage;
        emit fileLoaded(errMessage);
    }

    emit validFileChanged(_validFile);
}

void Backend::selectSaveFile(const QUrl &url)
{
    _saveFile.setFileName(url.toLocalFile());
    emit saveFileSelectedChanged(_saveFileSelected = true);
}

void Backend::saveFile(const QString& text)
{
    if (!_saveFile.open(QIODeviceBase::WriteOnly | QIODeviceBase::Truncate | QIODeviceBase::Text))
    {
        qDebug() << "Open file failed";
        return;
    }

    auto bytesWritten = _saveFile.write(text.toUtf8());

    qDebug() << bytesWritten;

    _saveFile.close();
}

void Backend::parse(const QString& source)
{
    // Reset object list
    _objectList.clear();
    _lineNo = -1;
    emit errorLineChanged(_errorLine = -1);

    if (!_validFile)
    {
        emit parsedSuccessChanged(_parsedSuccess = false);
        emit fileParsed(QStringLiteral("Invalid file"));
        return;
    }
    if (!_validName)
    {
        emit parsedSuccessChanged(_parsedSuccess = false);
        emit fileParsed(QStringLiteral("Invalid name"));
        return;
    }

    Regex.setPattern("(?=[A-Z])+");
    QStringList sl = _className.split(Regex, Qt::SkipEmptyParts);

    QString upperName;
    for (int i = 0; i < sl.count(); ++i)
    {
        if (i != 0)
            upperName += QStringLiteral("_");
        upperName += sl[i].toUpper();
    }

//    _outputText.clear();
    QString out;
    QTextStream textStream(&out);

    textStream
        << QStringLiteral("#ifndef ") << upperName
        << QStringLiteral("_H\n#define ") << upperName
        << QStringLiteral("_H\n\n#include \"dbcgeneratorutils.h\"")
        << "\n\n";

//    dbcFile.open(QIODevice::ReadOnly);
//    if (!dbcFile.isOpen())
//    {
//        emit parsedSuccessChanged(_parsedSuccess = false);
//        emit fileParsed(QStringLiteral("Could not open file"));
//        return;
//    }
    QString istStr = source;
    QTextStream ist(&istStr, QIODeviceBase::ReadOnly);

    // Parsing starts here
    if (_findValType(ist))
    {
        emit parsedSuccessChanged(_parsedSuccess = false);
        emit errorLineChanged(_errorLine = _lineNo - 1);
        emit fileParsed(QStringLiteral("Error on line ") + QString::number(_lineNo));
        return;
    }

    // Generate classes
    for (auto& obj : _objectList)
    {
        textStream << obj;
    }

    // Generate enum
    textStream << "enum class E" << _className << " : uint32_t\n{";
    for (auto& obj : _objectList)
    {
        textStream << "\n\tE" << obj.normalizedName << " = " << obj.canId << ',';
    }
    textStream << "\n};\n\n";

    // Footer
    textStream << QStringLiteral("#endif // ") << upperName << "_H\n";

//    emit errorLineChanged(_errorLine = -1);
    emit parsedSuccessChanged(_parsedSuccess = true);
    emit fileParsed(out);
}

void Backend::setClassName(QString name)
{
    if (name == _className)
        return;

    // ToDo: More checks on the name
    _validName = !name.isEmpty();
    if (_validName)
    {
        NormalizeName(name, _className, true);
        _lowerName = _className.toLower();
    }
    else
    {
        _className.clear();
        _lowerName.clear();
    }

    emit classNameChanged();
}

void Backend::NormalizeName(const QString &in, QString& out, bool capFirst)
{
    Regex.setPattern("([_\\s]+)");
    QStringList sl = in.split(Regex, Qt::SkipEmptyParts);
    out.clear();
    for (auto& s : sl)
    {
        if (!capFirst)
        {
            s[0] = s[0].toLower();
            capFirst = true;
        }
        else
            s[0] = s[0].toUpper();

        out += s;
    }
}

bool Backend::_findObject(QTextStream& ist)
{

    if (_currentLineSplit.isEmpty())
        return false;

    // Look for BO_
    if (_currentLineSplit[0] != QStringLiteral("BO_"))
        return false;

    auto& obj = _objectList.emplaceBack();

    if (obj.parse(_currentLineSplit))
        return true;

    if (_findSignal(ist, obj))
        return true;

    // Reorder signals
    std::sort(obj.canSignals.begin(), obj.canSignals.end(), [](SignalObject& a, SignalObject& b) {
            return (a.bitOffset < b.bitOffset);
        }
    );

    // Check if the sum of signal sizes match the length
    int sum = 0;
    for (auto& sig : obj.canSignals)
    {
        sum += sig.size;
    }
    // If the sum is different we need to add padding bytes
    obj.paddingBytes = (obj.length - sum/8);

    return false;
}

bool Backend::_findSignal(QTextStream& ist, CanObject& obj)
{
    if (ist.atEnd())
        return false;

    _currentLine = ist.readLine();
    ++_lineNo;

    Regex.setPattern("([:;\\s]+)");
    _currentLineSplit = _currentLine.split(Regex, Qt::SkipEmptyParts);

    if (_currentLineSplit.isEmpty())
        return false;
    if (_currentLineSplit[0] != QStringLiteral("SG_"))
        return false;

    auto& signal = obj.canSignals.emplaceBack();
    if (signal.parse(_currentLineSplit))
        return true;

    // Run recursively
    return _findSignal(ist, obj);
}

bool Backend::_findValType(QTextStream& ist)
{

    // Check if we are at the end
    if (ist.atEnd())
        return false;

    // read a line of text and split it with spaces
    _currentLine = ist.readLine();
    ++_lineNo;

    Regex.setPattern("([:;\\s]+)");
    _currentLineSplit = _currentLine.split(Regex, Qt::SkipEmptyParts);

    if (!_currentLineSplit.isEmpty())
    {
        // Look for SIG_VALTYPE_
        if (_currentLineSplit[0] == QStringLiteral("SIG_VALTYPE_"))
        {
            // If we find one, parse it
            if (_parseValType())
                return true; // error
        }
        else
        {
            // if not, look for BO_
            if (_findObject(ist))
                return true; // error
        }
    }

    // Run recursively
    return _findValType(ist);
}

bool Backend::_parseValType()
{
    if (_currentLineSplit.count() < 4)
        return false;

    // SIG_VALTYPE_ 9 Pos_Estimate : 1;
    bool parseOk;
    uint32_t canId = _currentLineSplit[1].toInt(&parseOk);
    if (!parseOk)
        return true;

    auto canObjEnd = _objectList.end();
    auto canObjIt = std::find_if(_objectList.begin(), canObjEnd, [canId](const CanObject& obj){
        return (uint32_t(obj.canId) == canId);
    });

    if (canObjIt == canObjEnd)
        return true;

    auto& canSignals = canObjIt->canSignals;
    auto sigObjEnd = canSignals.end();
    auto sigObjIt = std::find_if(canSignals.begin(), sigObjEnd, [&](const SignalObject& sig){
        return (sig.name == _currentLineSplit[2]);
    });

    if (sigObjIt == sigObjEnd)
        return true;

    int floatType = _currentLineSplit[3].toInt(&parseOk);
    if (!parseOk)
        return true;

    switch (floatType) {
    case 1:
        sigObjIt->dataType = EDataType::Float;
        break;
    case 2:
        sigObjIt->dataType = EDataType::Double;
        break;
    default:
        return true;
    }

    return false;
}

bool CanObject::parse(const QStringList& line)
{
    if (line.count() < 4)
        return true;

    bool parseOk;
//    int lineNo = 0;

    canId = line[1].toUInt(&parseOk);
    if (!parseOk)
        return true;

//    Backend::ParseName(line, ++lineNo, name);
    name = line[2];
    Backend::NormalizeName(name, normalizedName, true);

    length = line[3].toUInt(&parseOk);
    if (!parseOk)
        return true;

    return false;
}

bool SignalObject::parse(const QStringList& line)
{

    if (line.count() < 4)
        return true;

//    int lineNo = 1;

//    Backend::ParseName(line, lineNo, name);
    name = line[1];
    Backend::NormalizeName(name, normalizedName);

    // 0|32@1+
    // BitOffset|Size@EndiannessSignedness

    auto sl = line[2].split('|', Qt::SkipEmptyParts);
    if (sl.count() != 2)
        return true;

    bool parseOk;
    bitOffset = sl[0].toInt(&parseOk);
    if (!parseOk)
        return true;

    auto sl2 = sl[1].split('@', Qt::SkipEmptyParts);
    if (sl2.count() != 2)
        return true;

    size = sl2[0].toInt(&parseOk);
    if (!parseOk)
        return true;

    if (sl2[1].length() != 2)
        return true;

    endianness = EEndianness(QString(sl2[1][0]).toInt(&parseOk));
    if (!parseOk)
        return true;

    char ch = sl2[1][1].toLatin1();
    switch (ch) {
    case '+':
        signedness = ESignedness::Unsigned;
        break;
    case '-':
        signedness = ESignedness::Signed;
        break;
    default:
        return true;
    }

    // (0.001,2)
    // (scale,offset)
    auto tmp = line[3];

    Backend::Regex.setPattern("([()])+");
    tmp.remove(Backend::Regex);

    sl = tmp.split(',');

    if (sl.count() != 2)
        return true;

    scaling = sl[0].toFloat(&parseOk);
    if (!parseOk)
        return true;

    offset = sl[1].toFloat(&parseOk);
    if (!parseOk)
        return true;

    return checkIntegerType();
}

bool SignalObject::checkIntegerType()
{
    if ((size > 64) || (size < 0))
        return true;



    dataType = EDataType(std::max((int(std::ceil(std::log2(size))) - 3)*2 + int(signedness), 1));
    return false;
}
