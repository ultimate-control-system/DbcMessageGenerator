import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

Rectangle {
    property color borderColor: "black"
    property double borderWidth: 1
    property int numberMargin: 40
    property string text: qsTr("")

    readonly property alias lineCount: textArea.lineCount
    readonly property alias textDocument: textArea.textDocument

    property alias cursorPosition: textArea.cursorPosition
    property alias editFocus: textArea.focus
    property alias length: textArea.length

    function select(start, end) { textArea.select(start, end) }
    function moveCursorSelection(position, mode = TextEdit.SelectCharacters)
    { textArea.moveCursorSelection(position, mode) }

    function setMarginColor(lineIndex, color) {
        listView.model.set(lineIndex, {marginColor: color})
    }
    function setNumberColor(lineIndex, color) {
        listView.model.set(lineIndex, {numberColor: color})
    }

    id: root

    implicitHeight: 250
    implicitWidth: 250

    border.color: borderColor
    border.width: borderWidth

    ScrollView {
        width:  parent.width - borderWidth*2
        height: parent.height - borderWidth*2
        anchors.centerIn: parent

        RowLayout {
            anchors.fill: parent

            ColumnLayout {
                spacing: 0
                Layout.fillHeight: true

                Item {
                    Layout.preferredHeight: textArea.space
                }

                ListView {
                    id: listView
                    Layout.fillHeight: true
                    Layout.preferredWidth: numberMargin
                    model: ListModel { ListElement { marginColor: ""; numberColor: "" } }
                    delegate: Item {
                        height: textArea.lineHeight
                        width: listView.width

                        function colorIntensity(color)
                        {
                            return Math.sqrt(Math.pow(color.r, 2) + Math.pow(color.g, 2) + Math.pow(color.b, 2))/Math.sqrt(3)
                        }

                        Rectangle {
                            anchors.fill: parent
                            color: (model.marginColor === "") ? "transparent" : model.marginColor
                        }

                        RowLayout {
                            anchors.fill: parent

                            Label {
                                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                                text: model.index + 1
                                color: (model.numberColor !== "") ?
                                    model.numberColor :
                                        (model.marginColor === "") ?
                                            borderColor :
                                               (colorIntensity(Qt.color(model.marginColor)) > 0.75) ?
                                                    "black" :
                                                        "white"
                            }

                            Rectangle {
                                Layout.preferredHeight: textArea.lineHeight
                                Layout.alignment: Qt.AlignRight
                                Layout.preferredWidth: borderWidth
                                color: borderColor
                            }
                        }
                    }
                }

                Item {
                    Layout.preferredHeight: textArea.space
                }
            }

            TextArea {
                property double lineHeight: (lineCount === 0) ? contentHeight : contentHeight/lineCount
                property double space

                Component.onCompleted: space = (height - contentHeight)/2

                id: textArea

                Layout.fillHeight: true
                Layout.fillWidth: true
                text: root.text
                onLineCountChanged: {
                    while (listView.model.count < lineCount)
                    {
                        listView.model.append({marginColor: ""})
                    }
                    while (listView.model.count > lineCount)
                    {
                        listView.model.remove(listView.model.count - 1)
                    }
                }
            }
        }
    }
}
