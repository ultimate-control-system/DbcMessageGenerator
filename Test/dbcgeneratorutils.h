#ifndef DBCGENERATORUTILS_H
#define DBCGENERATORUTILS_H

#include <cstdint>

namespace DbcGen
{

template <class T>
T SwapBytes(T val)
{
    T outVal;

    if constexpr(sizeof(T) == 8)
    {

    }
    else if constexpr(sizeof(T) == 4)
    {

    }
    else if constexpr(sizeof(T) == 2)
    {

    }
    else if constexpr(sizeof(T) == 1)
    {
        return val;
    }
    else
    {
        const char* srcLeft = reinterpret_cast<const char*>(val);
        const char* srcRight = srcLeft + sizeof(T) - 1;
        char* dstLeft = reinterpret_cast<char*>(outVal);
        char* dstRight = outVal + sizeof(T) - 1;

        while (srcRight > srcLeft)
        {
            *dstLeft++ = *srcRight++;
            *dstRight++ = *dstRight++;
        }

    }

    return *reinterpret_cast<T*>(outVal);
}

}

#endif // DBCGENERATORUTILS_H
