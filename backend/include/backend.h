#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QFile>
#include <QRegularExpression>
#include <QQuickTextDocument>
#include <QTextBlock>

enum class EDataType
{
    Uint8 = 0,
    Int8,
    Uint16,
    Int16,
    Uint32,
    Int32,
    Uint64,
    Int64,
    Float,
    Double,
    Undefined
};

template <class OStream>
OStream& operator <<(OStream& ost, EDataType obj)
{
    switch (obj)
    {
    case EDataType::Uint8:
        return (ost << "uint8_t");
    case EDataType::Int8:
        return (ost << "int8_t");
    case EDataType::Uint16:
        return (ost << "uint16_t");
    case EDataType::Int16:
        return (ost << "int16_t");
    case EDataType::Uint32:
        return (ost << "uint32_t");
    case EDataType::Int32:
        return (ost << "int32_t");
    case EDataType::Uint64:
        return (ost << "uint64_t");
    case EDataType::Int64:
        return (ost << "int64_t");
    case EDataType::Float:
        return (ost << "float");
    case EDataType::Double:
        return (ost << "double");
    case EDataType::Undefined:
        return (ost << "Undefined");
    default:
        return (ost << "Unknown");
    }
}

enum class EEndianness
{
    BigEndian = 0,
    LittleEndian = 1,
    Undefined
};

template <class OStream>
OStream& operator <<(OStream& ost, EEndianness obj)
{
    switch (obj)
    {
    case EEndianness::BigEndian:
        return (ost << "BigEndian");
    case EEndianness::LittleEndian:
        return (ost << "LittleEndian");
    case EEndianness::Undefined:
        return (ost << "Undefined");
    default:
        return (ost << "Unknown");
    }

}

enum class ESignedness
{
    Unsigned = 0,
    Signed = 1,
    Undefined
};

template <class OStream>
OStream& operator <<(OStream& ost, ESignedness obj)
{
    switch (obj) {
    case ESignedness::Unsigned:
        return (ost << "Unsigned");
    case ESignedness::Signed:
        return (ost << "Signed");
    case ESignedness::Undefined:
        return (ost << "Undefined");
    default:
        return (ost << "Unknown");
    }

}

struct SignalObject
{
    SignalObject()
        : bitOffset(0)
        , size(0)
        , scaling(0.0f)
        , offset(0.0f)
        , endianness(EEndianness::Undefined)
        , signedness(ESignedness::Undefined)
        , dataType(EDataType::Undefined)
    {}

    bool parse(const QStringList& line);
    bool checkIntegerType();
    template <class OStream>
    OStream& initStr(OStream& ost) const;
    template <class OStream>
    OStream& getSetStr(OStream& ost) const;

//    QStringList line;
    QString name;
    QString normalizedName;
    int bitOffset;
    int size;
    float scaling;
    float offset;
    EEndianness endianness;
    ESignedness signedness;
    EDataType dataType;

    template <class OStream>
    friend OStream& operator <<(OStream& ost, const SignalObject& obj);
};

template <class OStream>
inline OStream& operator <<(OStream& ost, const SignalObject& obj)
{
    ost << "\t" << obj.dataType << " _" << obj.normalizedName << " : " << obj.size;

    return ost;
}

template <class OStream>
inline OStream& SignalObject::initStr(OStream& ost) const
{
    ost << normalizedName;
    if (dataType == EDataType::Float)
        ost << "(0.0f)";
    else if (dataType == EDataType::Double)
        ost << "(0.0)";
    else
        ost << "(0)";

    return ost;
}

template <class OStream>
inline OStream& SignalObject::getSetStr(OStream& ost) const
{
    ost << "\t// Get " << name << "\n";

    if ((scaling != 1.0f) && (scaling != 0.0f))
    {
        if (offset != 0.0f)
        {
            ost << "\tdouble " << normalizedName << "() const { return static_cast<double>(_"
                << normalizedName << ")*" << QString::number(scaling) << (offset > 0 ? " + " : " - ")
                << QString::number(std::abs(offset)) << "; }\n";
        }
        else
        {
            ost << "\tdouble " << normalizedName << "() const { return static_cast<double>(_"
                << normalizedName << ")*" << QString::number(scaling) << "; }\n";
        }
    }
    else
    {
        if (offset != 0.0f)
        {
            ost << '\t' << dataType << ' ' << normalizedName << "() const { return _" << normalizedName
                << (offset > 0 ? " + " : " - ") << QString::number(std::abs(offset)) << "; }\n";
        }
        else
        {
            ost << '\t' << dataType << ' ' << normalizedName << "() const { return _" << normalizedName << "; }\n";
        }
    }

    ost << "\t// Set " << name << "\n";

    if ((scaling != 1.0f) && (scaling != 0.0f))
    {
        if (offset != 0.0f)
        {
            ost << "\tvoid " << normalizedName << "(double val) { _" << normalizedName << " = static_cast<"
                << dataType << ">((val" << (offset > 0 ? " - " : " + ")
                << QString::number(std::abs(offset)) << ")/" << QString::number(scaling) << "); }\n\n";
        }
        else
        {
            ost << "\tvoid " << normalizedName << "(double val) { _"
                << normalizedName << " = static_cast<" << dataType << ">(val/" << QString::number(scaling) << "); }\n\n";
        }
    }
    else
    {
        if (offset != 0.0f)
        {
            ost << "\tvoid " << normalizedName << '(' << dataType << " val) { _" << normalizedName
                << " = val" <<  (offset > 0 ? " - " : " + ") << QString::number(std::abs(offset)) << "; }\n\n";
        }
        else
        {
            ost << "\tvoid " << normalizedName << '(' << dataType << " val) { _" << normalizedName << " = val; }\n\n";
        }
    }

    return ost;
}

struct CanId
{
    explicit CanId(uint32_t id)
    {
        *this = id;
    }

    CanId& operator =(const uint32_t& other) { *reinterpret_cast<uint32_t*>(this) = other; return *this; }
    operator uint32_t() const {return *reinterpret_cast<const uint32_t*>(this);}

    uint32_t commandId: 5;
    uint32_t axisId: 24;
    uint32_t errorFlag: 1;
    uint32_t rtrFlag: 1;
    uint32_t effFlag: 1;
};

struct CanObject
{
    CanObject()
        : canId(0)
        , length(0)
        , paddingBytes(0)
    {}

    bool parse(const QStringList& line);

    QList<SignalObject> canSignals;
    QString name;
    QString normalizedName;
    uint32_t canId;
    int length;
    int paddingBytes;

    template <class OStream>
    friend OStream& operator <<(OStream& ost, const CanObject& obj);
};

template <class OStream>
inline OStream& operator <<(OStream& ost, const CanObject& obj)
{
    ost << "// " << obj.name
        << ", can id: " << obj.canId
        << ", length: " << obj.length
        << '\n';

    ost << "class " << obj.normalizedName << "Message\n{\npublic:\n\t"
        << obj.normalizedName << "Message()\n";

    bool firstSig = true;
    for (auto& sig : obj.canSignals)
    {
        ost << (firstSig ? "\t\t: _" : "\t\t, _");
        sig.initStr(ost) << "\n";
        firstSig = false;
    }

    if (obj.paddingBytes != 0)
    {
        ost << "\t\t, _reserved{ ";
        for (int padd = 0; padd < obj.paddingBytes; ++padd)
        {
            ost << '0' << ((padd == (obj.paddingBytes - 1)) ? "" : ", ");
        }
        ost << " }\n";
    }

    ost << "\t{}\n\n\tstatic constexpr uint32_t CanId() { return " << obj.canId
        << "; }\n\tstatic constexpr uint32_t Length() { return " << obj.length
        << "; }\n\tstatic constexpr const char* MessageName() { return \"" << obj.name << "\"; }\n\n";


    if (obj.canSignals.count() > 0)
    {
        ost << "\tchar* data() { return reinterpret_cast<char*>(this); }\n"
            << "\tconst char* data() const { return reinterpret_cast<const char*>(this); }\n\n";
    }

    for (auto& sig : obj.canSignals)
        sig.getSetStr(ost);

    ost << "private:\n";
    for (auto& sig : obj.canSignals)
        ost << sig << ";\n";

    if (obj.paddingBytes != 0)
        ost << "\t[[maybe_unused]] const char _reserved[" << obj.paddingBytes << "];\n";

    return (ost << "};\n\n");
}


class Backend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool validFile MEMBER _validFile NOTIFY validFileChanged)
    Q_PROPERTY(bool validName MEMBER _validName NOTIFY classNameChanged)
    Q_PROPERTY(bool parsedSucess MEMBER _parsedSuccess NOTIFY parsedSuccessChanged)
    Q_PROPERTY(bool saveFileSelected MEMBER _saveFileSelected NOTIFY saveFileSelectedChanged)
    Q_PROPERTY(QString className READ className WRITE setClassName NOTIFY classNameChanged)
    Q_PROPERTY(QString lowerName MEMBER _lowerName NOTIFY classNameChanged)
    Q_PROPERTY(int errorLine MEMBER _errorLine NOTIFY errorLineChanged)

public:
    explicit Backend(QObject *parent = nullptr);

    static QRegularExpression Regex;

    static void ParseName(const QStringList& in, int& lineNo, QString& out);
    static void NormalizeName(const QString& in, QString& out, bool capFirst = false);

    Q_INVOKABLE
    void loadFile(const QUrl& url);
    Q_INVOKABLE
    void selectSaveFile(const QUrl& url);
    Q_INVOKABLE
    void saveFile(const QString& text);
    Q_INVOKABLE
    void parse(const QString& source);

    void setClassName(QString name);
    QString className() const { return _className; }

    Q_INVOKABLE int currentLineNumber(QQuickTextDocument *textDocument, int cursorPosition){
        if(QTextDocument * td = textDocument->textDocument()){
            QTextBlock tb = td->findBlock(cursorPosition);
            return tb.blockNumber();
        }
        return -1;
    }

signals:
    void fileLoaded(const QString& text);
    void fileParsed(const QString& text);
    void validFileChanged(bool val);
    void saveFileSelectedChanged(bool val);
    void classNameChanged();
    void parsedSuccessChanged(bool val);
    void errorLineChanged(int);

private:
    QList<CanObject> _objectList;

    bool _findObject(QTextStream& ist);
    bool _findSignal(QTextStream& ist, CanObject& obj);
    bool _findValType(QTextStream& ist);
    bool _parseValType();

//    QFile _dbcFile;
    QFile _saveFile;
    bool _validFile;
    bool _validName;
    bool _parsedSuccess;
    bool _saveFileSelected;
    QString _className;
    QString _lowerName;
    QString _currentLine;
//    QString _outputText;
    QStringList _currentLineSplit;
    int _lineNo;
    int _errorLine;

};

#endif // BACKEND_H
